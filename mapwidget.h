//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef MAPWIDGET_H
#define MAPWIDGET_H

#include <QWidget>
#include <QSettings>
#include <MarbleGlobal.h>
#include <GeoDataLookAt.h>
#include <GeoDataLatLonAltBox.h>
#include <RenderState.h>

#include <TileCoordsPyramid.h>
#include <GeoDataCoordinates.h>



namespace Marble {
class AbstractDataPluginItem;
class MarbleMap;
class MarbleModel;
class ViewportParams;
class GeoDataCoordinates;
class GeoPainter;
class GeoDataPlacemark;
class RenderPlugin;
class MapWidgetInputHandler;
}

class MapWidgetPopupMenu;

using namespace Marble;

class MapWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MapWidget(QWidget *parent = 0);
    MarbleModel *model();
    ViewportParams *viewport();
    const ViewportParams *viewport() const;

    const MarbleModel *model() const;

    MarbleMap *map();
    const MarbleMap *map() const;

    QList<RenderPlugin *> renderPlugins() const;
    int tileZoomLevel() const;

    qreal centerLongitude() const;
    qreal centerLatitude() const;

    int zoom() const;
    qreal zoom(qreal radius) const;
    qreal radius(qreal zoom) const;

    void readPluginSettings( QSettings& settings );
    void writePluginSettings( QSettings& settings ) const;

    MapWidgetPopupMenu *popupMenu();

    QVector<const GeoDataPlacemark*> whichFeatureAt( const QPoint& ) const;
    QList<AbstractDataPluginItem *> whichItemAt( const QPoint& curpos ) const;
    bool geoCoordinates( int x, int y,
                         qreal& lon, qreal& lat,
                         GeoDataCoordinates::Unit = GeoDataCoordinates::Degree ) const;
    bool screenCoordinates( qreal lon, qreal lat,
                            qreal& x, qreal& y ) const;

    void setInputHandler();
    void setInputHandler(MapWidgetInputHandler *handler );
    MapWidgetInputHandler *inputHandler();
    const MapWidgetInputHandler *inputHandler() const;

    qreal distanceFromZoom(qreal zoom) const;
    qreal zoomFromDistance(qreal distance) const;

    qreal distance() const;
    qreal distanceFromRadius(qreal radius) const;
    QString distanceString() const;
    int radius() const;
    qreal radiusFromDistance(qreal distance) const;
    void zoomAt(const QPoint &pos, qreal newDistance);
    GeoDataLookAt lookAt() const;

    int minimumZoom() const;
    int maximumZoom() const;
signals:
    void zoomChanged(int zoom);
    void distanceChanged(const QString& distanceString);
    void updateRequired();

    void mouseMoveGeoPosition( const QString& );

    // signals forwarded from map
    void visibleLatLonAltBoxChanged( const GeoDataLatLonAltBox& visibleLatLonAltBox );
    void projectionChanged( Projection );
    void tileLevelChanged( int level );
    void framesPerSecond( qreal fps );
    void pluginSettingsChanged();
    void renderPluginInitialized( RenderPlugin *renderPlugin );
    void renderStatusChanged( RenderStatus status );
    void renderStateChanged( const RenderState &state );


public slots:
    void centerOn( const GeoDataCoordinates &point, bool animated = false );
    void setZoom( int zoom);
    void downloadRegion( QVector<TileCoordsPyramid> const & );
    void flyTo(const GeoDataLookAt &newLookAt, FlyToMode mode = Automatic);
    void clearVolatileTileCache();


protected:
    void paintEvent(QPaintEvent * event);
    void resizeEvent(QResizeEvent * event);

private:
    MarbleMap * m_map;
    MapWidgetPopupMenu *m_popupmenu;
    MapWidgetInputHandler *m_inputhandler;
    const qreal m_viewAngle;
    int m_logzoom;

};

#endif // MAPWIDGET_H

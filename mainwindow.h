//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 47 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:02:47 +0000 (Wed, 07 Nov 2018) $
//


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QPointer>
#include <QMainWindow>
#include <QMultiHash>
#include <QRect>

#ifdef WITH_MARBLEWIDGET
#include <MarbleWidget.h>
#include "mymarblewidget.h"
#else
#include "mapwidget.h"
#endif
#include <GeoDataDocument.h>
#include <MarbleLocale.h>

#include "RouteToolPluginInterface.h"
#include "AirspacesPluginInterface.h"
#include "AptNavaidsPluginInterface.h"
//#include "EadBrowserPluginInterface.h"


using namespace Marble;

class QLabel;
class QProgressBar;
class QAction;
class QWebView;
class DatabaseHandler;
class DbMaintWidget;
class FindWidget;
class WebviewPopup;
class ConfigDialog;
class AircraftList;

#ifndef WITH_MARBLEWIDGET
class MapWidget;
class MapWidgetPopupMenu;
#endif

namespace Marble {
class DownloadRegionDialog;
}

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool initOk();
    QString getErrorMsg() { return sDBerr; }

protected:
    void closeEvent (QCloseEvent * event);

private:
    Ui::MainWindow *ui;
#ifdef WITH_MARBLEWIDGET
    MyMarbleWidget *m_pMapWidget;
    MarbleWidgetPopupMenu *m_pPopupMenu;
#else
    MapWidget *m_pMapWidget;
    MapWidgetPopupMenu *m_pPopupMenu;
#endif
    QPointer<DbMaintWidget> m_pDbMaintWidget;
    FindWidget *m_pFindWidget;
    DownloadRegionDialog *m_pDownloadRegionDialog;

    qreal m_homeLon;
    qreal m_homeLat;
    int m_homeZoom;
    // WebPopup
    QList<QAction *> m_actionList;
    WebviewPopup *m_popupBrowser;
    QRect m_popupBrowserGeom;

    DatabaseHandler *m_pDatabase;
    RouteToolPluginInterface *m_pRouteInterface;
    QPointer<ConfigDialog> m_pConfigDialog;
//    OpenAipUpdater *m_pUpdater;

    // Status Bar
    QString     m_position;
    QString     m_zoom;
    QLabel      *m_pPositionLabel;
    QLabel      *m_pZoomLabel;
    QProgressBar *m_pDownloadProgressBar;
    QStringList m_activeContinents;
    MarbleLocale::MeasurementSystem m_measurementSystem;
    AircraftList *m_pAicraftList;
    bool m_initDbOk;
    QString sDBerr;

#ifdef LOAD_FROM_CSV
    void addAirports(GeoDataDocument &document);
    void loadRunways(QMultiHash<QString, QStringList> &table);
#endif

    void updateStatusBar();
    void createStatusBar();
    void readSettings();
    void writeSettings();
    void setupWebview();


private slots:
    void showAirportInfo();
    void showAirportInfo(int index, QString code);
    void showNotam();
    void showNotam(QStringList icaoCodes);
    void showAIPList();

    void showDbMaint();
    void findWpt();
    void gotoWaypoint(qreal lon, qreal lat);
    void addWaypoint(qreal lon, qreal lat, QString code, QString type);
    void showDownloadRegionDialog();
    void showAbout();
    void showHelp();
    void setHome();
    void showConfig();

    void  setupStatusBar();
    void  setupDownloadProgressBar();


    // Download region dialog
    void  downloadRegion();

    void handleProgress( int, int );
    void removeProgressItem();


    void  showPosition( const QString& position);
    void  showZoom( int );

    // temporär
    void printDebugInt(int x);

};

#endif // MAINWINDOW_H

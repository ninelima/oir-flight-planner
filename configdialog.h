//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QDialog>
#include <RenderPluginModel.h>

using namespace Marble;

class QSettings;
class MapWidget;

namespace Ui {
class ConfigDialog;
}

namespace Marble {
class MarblePluginSettingsWidget;
}

class ConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConfigDialog(MapWidget *mapWidget, QWidget *parent = 0);
    ~ConfigDialog();

public slots:
    bool close();

private:
    Ui::ConfigDialog *ui;
    MarblePluginSettingsWidget *m_pluginConfig;
    RenderPluginModel m_pluginModel;
    MapWidget *m_pMapWidget;
    QSettings *m_pSettings;

private:  // functions
    void setupTabWebview();
    void setupTabContinents();
    void setupTabAircraft();
    void setupTabGeneral();
private slots:
    void saveWebviewSettings();
    void saveContinentsSettings();
    void saveAircraftSettings();
    void saveGeneralSettings();

    void addWebViewItem();
    void removeWebViewItem();
    void addAircraftItem();
    void removeAircraftItem();
    void writeSettings();

};

#endif // DIALOG_H

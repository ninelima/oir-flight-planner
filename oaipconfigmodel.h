//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef OAIPCONFIGMODEL_H
#define OAIPCONFIGMODEL_H

#include <stdint.h>
#include <QAbstractTableModel>
#include <QSqlQuery>


class OaipConfigModel : public QAbstractTableModel
{
    Q_OBJECT

struct dataSet {
    QString country;
    QString name_en;
    QString continent;
    int8_t wpt; // 0 = not installed, 1 = installed, 2 = not available
    int8_t nav;
    int8_t asp;
};

public:
    explicit OaipConfigModel(QObject *parent = 0);
    int rowCount (const QModelIndex & parent = QModelIndex()) const;
    int columnCount (const QModelIndex & parent = QModelIndex()) const;
    QVariant data (const QModelIndex & index, int role = Qt::DisplayRole) const;
    QVariant headerData (int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags (const QModelIndex & index) const;
    bool setData (const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);
    bool setCheckState (const int col, const int row, const enum Qt::CheckState checkState);
    void toggleTransaction();
    bool insertRows (int row, int count, const QModelIndex & parent = QModelIndex());
    bool setupData();

signals:

public slots:

private:
   QList<struct dataSet*> datalist;
   QSqlQuery query;

   QVariant getCheckstate(const int8_t state) const;
   bool m_transactionActive;


};

#endif // OAIPCONFIGMODEL_H

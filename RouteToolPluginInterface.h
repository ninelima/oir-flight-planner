//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef ROUTETOOLPLUGININTERFACE_H
#define ROUTETOOLPLUGININTERFACE_H

#include <QString>
#include <QPoint>
#include <QObject>
#include <MarbleMap.h>
#include <QAction>
#include "aircraft.h"


class RouteToolPluginWorker: public QObject
{
    Q_OBJECT
public:
    explicit RouteToolPluginWorker(QObject *parent = 0)
    {
        Q_UNUSED(parent)
    }

Q_SIGNALS:
    void showAptInfo(int index, QString code);  // must be defined in the implementation's header file as well!
    void gotoWpt(qreal lon, qreal lat);
    void mouseMove(QPoint pos);
    void posClicked(int x, int y);
    void actionListChanged(QList<QAction *>);
    void routeNotamsRequested(QStringList icaoCodes);

};


class RouteToolPluginInterface
{
public:
    virtual ~RouteToolPluginInterface() {}
    virtual void addWaypoint(qreal lon, qreal lat, QString code, QString type) = 0;
    virtual void closePopup() = 0;
    virtual void setMap(Marble::MarbleMap *map) = 0;
    virtual void setACList(AircraftList *aircraftList) = 0;
    virtual QList<QAction *> getActions() = 0;
    virtual RouteToolPluginWorker *newModule() = 0;

//private:
    RouteToolPluginWorker *w;
};





Q_DECLARE_INTERFACE(RouteToolPluginInterface, "FlightRoute.RouteToolPluginInterface")


#endif // ROUTETOOLPLUGININTERFACE_H

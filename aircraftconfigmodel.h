//
// Copyright 2016 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef AIRCRAFTCONFIGMODEL_H
#define AIRCRAFTCONFIGMODEL_H

#include <QAbstractTableModel>
#include <QTime>
#include <GeoDataLineString.h>
#include <MarbleLocale.h>

class Aircraft;

class AircraftConfigModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit AircraftConfigModel(QObject *parent = 0);
    ~AircraftConfigModel();

    int rowCount (const QModelIndex & = QModelIndex()) const;
    int columnCount (const QModelIndex & = QModelIndex()) const;
    QModelIndex index ( int row, int column, const QModelIndex & parent = QModelIndex() ) const;
    void clear();
    bool isEmpty();
    bool removeRow(int row, const QModelIndex &parent = QModelIndex() );
    bool insertRow (int row, const QModelIndex & parent = QModelIndex() );

    QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;
    QVariant headerData (int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    Qt::ItemFlags flags (const QModelIndex & index) const;

signals:

public slots:
    void moveUp(int row);
    void moveDown(int row);
    void saveConfig();


private:
    QList <Aircraft*> *m_pAircraftList;

private slots:


};

#endif // AIRCRAFTCONFIGMODEL_H

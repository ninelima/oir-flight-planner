//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef DATABASEHANDLER_H
#define DATABASEHANDLER_H

#include <QObject>
#include <QSqlDatabase>
#include <QMultiHash>
#include <GeoDataDocument.h>


class DatabaseHandler : public QObject
{
    Q_OBJECT
public:
    explicit DatabaseHandler(QObject *parent = 0);
    int connectDatabase();

//    void getAirports(Marble::GeoDataDocument &document);

//    void getNavaids(Marble::GeoDataDocument &document);
//    QStringList getWaypointInfo(QString code, QString country, int type);
    QList<QStringList> findWaypoint(QString wpt, bool icaoonly=true);

    qint32 getAirspaceCount() { return m_airspacesCount; }
    qint32 getAirportCount() { return m_airportsCount; }
    qint32 getRunwayCount() { return m_runwaysCount; }
    qint32 getNavaidCount() { return m_navaidsCount; }

    // getter/setter to implelemt
    int upperLimit;
    int lowerLimit;

signals:

public slots:
    bool cleanDatabase();
    bool resetUpdateStatus();

private:
    QSqlDatabase m_db;   //! Verbindung zur Datenbank
    qint32 m_airspacesCount;
    qint32 m_airportsCount;
    qint32 m_runwaysCount;
    qint32 m_navaidsCount;



//    void loadRunways(QMultiHash<qlonglong, QStringList> &table);
    bool checkDatabase();
};

#endif // DATABASEHANDLER_H

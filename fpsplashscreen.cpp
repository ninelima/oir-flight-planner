//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "fpsplashscreen.h"

#include <QPainter>
#include <QApplication>

FpSplashScreen::FpSplashScreen(const QPixmap &pixmap, Qt::WindowFlags f) :
    QSplashScreen(pixmap, f)
{
}

void FpSplashScreen::drawContents(QPainter *painter)
{
    painter->setPen(Qt::darkGreen);
    painter->setFont(QFont("Arial", 30));
    painter->drawText(20, 50, "OIR Flight Planner");

    painter->setPen(QColor(125,124,125));
    painter->setFont(QFont("Arial", 15));
    painter->drawText(20, 80, "Benefit from Open and Free Data!");

    painter->setPen(Qt::white);
    painter->setFont(QFont("Arial", 10));

    painter->drawText(0, height()-20, width(), 20, Qt::AlignCenter, QString("Version: %1").arg(QApplication::applicationVersion()));


}

//
// Copyright 2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 52 $
//  $Author: brougham73 $
//  $Date: 2019-06-04 16:46:04 +0000 (Tue, 04 Jun 2019) $
//

#ifndef FIVELNCUPDATER_H
#define FIVELNCUPDATER_H

#include <QObject>
#include <QUrl>
#include <QSqlQuery>

class QNetworkAccessManager;

class FiveLNCupdater : public QObject
{
    Q_OBJECT
public:
    explicit FiveLNCupdater(QObject *parent = 0);

private:
    QUrl m_url;
    QNetworkAccessManager *m_pManager;
    QSqlQuery m_query;
    bool m_isWorking;


signals:

public slots:
    void getJSON();

private slots:
    void parseJSON();
};

#endif // FIVELNCUPDATER_H

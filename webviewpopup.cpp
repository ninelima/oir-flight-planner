//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "webviewpopup.h"
#include "ui_webviewpopup.h"


WebviewPopup::WebviewPopup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WebviewPopup)
{
    ui->setupUi(this);
    connect(ui->webviewWidget, SIGNAL(urlChanged(QUrl)), this, SLOT(urlChanged(QUrl)));
    connect(ui->pbReload, SIGNAL(clicked()), ui->webviewWidget, SLOT(reload()));
}

WebviewPopup::~WebviewPopup()
{
    delete ui;
}

WebviewWidget *WebviewPopup::webviewWidget()
{
    return ui->webviewWidget;
}

void WebviewPopup::urlChanged(QUrl url)
{
    ui->leURL->setText(url.toString());
}

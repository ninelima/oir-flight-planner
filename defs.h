//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 51 $
//  $Author: brougham73 $
//  $Date: 2019-05-22 18:59:33 +0000 (Wed, 22 May 2019) $
//

#ifndef DEFS_H
#define DEFS_H

#include <QString>
#include <QStringList>

const QString appName = "OIR Flight Planner";
const QString orgName = "Whitepeak Software";
const QString appVersion = "0.00.05";
const QString dbFilename = "data.sqlite";


#endif // DEFS_H

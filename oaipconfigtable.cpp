//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "oaipconfigtable.h"
//#include <QKeyEvent>
#include <QMenu>
#include <QContextMenuEvent>
#include <QHeaderView>
#include <QAction>

#include <QSortFilterProxyModel>
#include "oaipconfigmodel.h"

OaipConfigTable::OaipConfigTable(QWidget *parent) :
    QTableView(parent),
    m_pModel(new OaipConfigModel),
    m_pProxyModel(new QSortFilterProxyModel),
    m_contextMenuActions(QList<QAction*>()),
    m_colClicked(-1),
    m_rowClicked(-1)
{
    this->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this->horizontalHeader(), SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequested(QPoint)));

    m_contextMenuActions.append(new QAction("Check all in column", this));
    connect(m_contextMenuActions.last(), SIGNAL(triggered()), this, SLOT(checkCol()));
    m_contextMenuActions.append(new QAction("Uncheck all in column", this));
    connect(m_contextMenuActions.last(), SIGNAL(triggered()), this, SLOT(uncheckCol()));
    m_contextMenuActions.append(new QAction("Check all in row", this));
    connect(m_contextMenuActions.last(), SIGNAL(triggered()), this, SLOT(checkRow()));
    m_contextMenuActions.append(new QAction("Uncheck all in row", this));
    connect(m_contextMenuActions.last(), SIGNAL(triggered()), this, SLOT(uncheckRow()));

    m_pModel->setupData();
    m_pProxyModel->setSourceModel(m_pModel);
    this->setModel(m_pProxyModel);

}

OaipConfigTable::~OaipConfigTable()
{
    delete m_pModel;
    delete m_pProxyModel;
}

void OaipConfigTable::setFilter(int keyColumn, QString regExp)
{
    m_pProxyModel->setFilterRegExp(QRegExp(regExp, Qt::CaseInsensitive,
                                        QRegExp::FixedString));
    m_pProxyModel->setFilterKeyColumn(keyColumn);
}

void OaipConfigTable::contextMenuEvent(QContextMenuEvent *event)
{
   contextMenuRequested(event->pos());
}

void OaipConfigTable::contextMenuRequested(QPoint point)
{
    QString sMenuCheckCol = tr("Check displayed in column '%1'");
    QString sMenuUncheckCol = tr("Uncheck displayed in column '%1'");
    QString sMenuCheckRow = tr("Check all in row '%1'");
    QString sMenuUncheckRow = tr("Uncheck all in row '%1'");
    m_colClicked = this->columnAt(point.x());
    m_rowClicked = this->rowAt(point.y());
    switch (m_colClicked)
    {
    case 3:
        sMenuCheckCol = sMenuCheckCol.arg(tr("Wpt"));
        sMenuUncheckCol = sMenuUncheckCol.arg(tr("Wpt"));
        break;
    case 4:
        sMenuCheckCol = sMenuCheckCol.arg(tr("Nav"));
        sMenuUncheckCol = sMenuUncheckCol.arg(tr("Nav"));
        break;
    case 5:
        sMenuCheckCol = sMenuCheckCol.arg(tr("Asp"));
        sMenuUncheckCol = sMenuUncheckCol.arg(tr("Asp"));
        break;

    }

    QString countryCode = m_pModel->data(m_pProxyModel->mapToSource(m_pProxyModel->index(m_rowClicked, 0))).toString();
    sMenuCheckRow = sMenuCheckRow.arg(countryCode);
    sMenuUncheckRow = sMenuUncheckRow.arg(countryCode);

    QMenu contextMenu(this);

    if (m_colClicked > 2)
    {
        m_contextMenuActions.at(0)->setText(sMenuCheckCol);
        contextMenu.addAction(m_contextMenuActions.at(0));
        m_contextMenuActions.at(1)->setText(sMenuUncheckCol);
        contextMenu.addAction(m_contextMenuActions.at(1));

    }
    m_contextMenuActions.at(2)->setText(sMenuCheckRow);
    contextMenu.addAction(m_contextMenuActions.at(2));
    m_contextMenuActions.at(3)->setText(sMenuUncheckRow);
    contextMenu.addAction(m_contextMenuActions.at(3));
    QPoint menuPos = mapToGlobal(point);
    menuPos.setX(menuPos.x()+5);
    menuPos.setY(menuPos.y()+5);
    contextMenu.exec(menuPos);
}

void OaipConfigTable::uncheckCol()
{
    m_pModel->toggleTransaction();
    for (int i = 0; i < m_pProxyModel->rowCount(); i++)
    {
        QModelIndex index = m_pProxyModel->index(i, m_colClicked);
        m_pModel->setCheckState(m_pProxyModel->mapToSource(index).column(), m_pProxyModel->mapToSource(index).row(), Qt::Unchecked);
    }
    m_pModel->toggleTransaction();
}

void OaipConfigTable::checkCol()
{
    m_pModel->toggleTransaction();
    for (int i = 0; i < m_pProxyModel->rowCount(); i++)
    {
        QModelIndex index = m_pProxyModel->index(i, m_colClicked);
        m_pModel->setCheckState(m_pProxyModel->mapToSource(index).column(), m_pProxyModel->mapToSource(index).row(), Qt::Checked);
    }
    m_pModel->toggleTransaction();
}

void OaipConfigTable::uncheckRow()
{
    for (int i = 3; i < 6; i++)  // 4th to 6th column have to be checked/unchecked
    {
        QModelIndex index = m_pProxyModel->index(m_rowClicked, i);
        m_pModel->setCheckState(m_pProxyModel->mapToSource(index).column(), m_pProxyModel->mapToSource(index).row(), Qt::Unchecked);
    }
}

void OaipConfigTable::checkRow()
{
    for (int i = 3; i < 6; i++)  // 4th to 6th column have to be checked/unchecked
    {
        QModelIndex index = m_pProxyModel->index(m_rowClicked, i);
        m_pModel->setCheckState(m_pProxyModel->mapToSource(index).column(), m_pProxyModel->mapToSource(index).row(), Qt::Checked);
    }
}

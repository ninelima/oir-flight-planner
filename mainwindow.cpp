//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//


#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QLabel>
#include <QProgressBar>
#include <QFileInfo>
#include <QApplication>
#include <QMultiHash>
#include <QAction>
#ifdef WITH_WEBKIT
//#include <QWebView>
#else
//#include <QtWebEngine>
#endif
#include <QFileDialog>
#include <QEvent>
#include <QKeyEvent>
#include <QSettings>
#include <QMessageBox>

#ifndef WITH_MARBLEWIDGET
#include "mapwidgetpopupmenu.h"
#include "mapwidget.h"
#include "mapwidgetinputhandler.h"
#endif
#include <MarbleModel.h>

#include <DownloadRegionDialog.h>
#include <DownloadRegion.h>
#include <ViewportParams.h>
#include <GeoDataCoordinates.h>
#include <GeoDataPlacemark.h>
#include <GeoDataTreeModel.h>
#include <GeoDataStyle.h>
#include <GeoDataIconStyle.h>
#include <Planet.h>
#include <MarbleLocale.h>
#include <MarbleDirs.h>
#include <MarbleDebug.h>
#include <RenderPlugin.h>
#include <PopupLayer.h>
#include <HttpDownloadManager.h>

#include "defs.h"
#include "databasehandler.h"
#include "dbmaintwidget.h"
#include "findwidget.h"
#include "aboutwindow.h"
#include "simplehelpwindow.h"
#include "webviewpopup.h"
#include "configdialog.h"
#include "aircraft.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_pDbMaintWidget(NULL),
    m_pDownloadRegionDialog(NULL),
    m_homeLon(8.2582014),
    m_homeLat(46.9022335),
    m_homeZoom(2000),
    m_actionList(QList<QAction *>()),
    m_popupBrowser(0),
    m_pRouteInterface(0),
    m_pConfigDialog(0),
    // Status Bar
    m_position(QString()),
    m_zoom(QString()),
    m_pPositionLabel( 0 ),
    m_pZoomLabel( 0 ),
    m_pDownloadProgressBar( 0 ),
    m_measurementSystem(MarbleLocale::NauticalSystem),
    m_pAicraftList(0),
    m_initDbOk(false),
    sDBerr(QString())
{
    ui->setupUi(this);
    this->setWindowTitle(appName);

    connect(ui->actionConfigPlugins, SIGNAL(triggered()), this, SLOT(showConfig()));
    connect(ui->actionFind, SIGNAL(triggered()), this, SLOT(findWpt()));
    connect(ui->actionDownload, SIGNAL(triggered()), this, SLOT(showDownloadRegionDialog()));
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(showAbout()));
    connect(ui->actionHelp, SIGNAL(triggered()), this, SLOT(showHelp()));
    connect(ui->actionClose, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->actionSetHome, SIGNAL(triggered()), this, SLOT(setHome()));
    connect(ui->actionListAIPs, SIGNAL(triggered()), this, SLOT(showAIPList()));
    connect(ui->actionDbMaint, SIGNAL(triggered()), this, SLOT(showDbMaint()));


    m_pDatabase = new DatabaseHandler();
    if (m_pDatabase != NULL)
    {
        int iConn;
        if ((iConn = m_pDatabase->connectDatabase()) != 0)
        {
            switch (iConn)
            {
            case -1:
                sDBerr = tr("No valid database driver (QSqlDatabase)");
                break;
            case -2:
                sDBerr = tr("No database file found (data.sqlite)");
                break;
            case -3:
                sDBerr = tr("Database open failedr (QSqlDatabase::open())");
                break;
            case -4:
                sDBerr = tr("Database connection isn't open (QSqlDatabase::isOpen())");
                break;
            default:
                sDBerr = tr("Unknown database error");

            }
        }
        else
        {
            m_initDbOk = true;
        }
    }

    // check for SSL support

    qDebug() << "SSL support available:" << QSslSocket::supportsSsl();

    MarbleDirs::setMarbleDataPath(QCoreApplication::applicationDirPath()+"/data");
    MarbleDirs::setMarblePluginPath(QCoreApplication::applicationDirPath()+"/plugins");
//    MarbleDebug::setEnabled(true);
//    MarbleDirs::debug();
//    qDebug() << "MarbleDirs::localPath:" << MarbleDirs::localPath();

    m_pMapWidget = new MapWidget;
    connect(m_pMapWidget, SIGNAL(zoomChanged(int)), this, SLOT(printDebugInt(int)));

    readSettings(); // load setting as soon as the MarbleWidget is created

    MarbleLocale *locale = MarbleGlobal::getInstance()->locale();
    locale->setMeasurementSystem(m_measurementSystem);


    // Center the map onto a given position
    GeoDataCoordinates home(m_homeLon, m_homeLat, 0.0, GeoDataCoordinates::Degree);
    m_pMapWidget->centerOn(home);
    m_pMapWidget->setZoom( m_homeZoom );
    m_pMapWidget->model()->setHome(home, m_homeZoom);
    setCentralWidget(m_pMapWidget);

    //m_pMapWidget->model()->clearPersistentTileCache();
    //m_pMapWidget->clearVolatileTileCache();

    m_pAicraftList = AircraftList::instance();


    setupWebview();

    m_pFindWidget = new FindWidget(m_pDatabase);
    connect(m_pFindWidget, SIGNAL(gotoClicked(qreal,qreal)), this, SLOT(gotoWaypoint(qreal,qreal)));

    QList<Marble::RenderPlugin*> pluginList = m_pMapWidget->renderPlugins();
    for (int i= 0; i < pluginList.size(); i++)
    {
        if (pluginList.at(i)->nameId() == "route-tool")
        {
            m_pRouteInterface = qobject_cast<RouteToolPluginInterface *>(pluginList.at(i));
            m_pRouteInterface->setMap(m_pMapWidget->map());
            m_pRouteInterface->setACList(m_pAicraftList);
            connect(m_pFindWidget, SIGNAL(addClicked(qreal,qreal,QString, QString)), this, SLOT(addWaypoint(qreal,qreal,QString, QString)));
            connect(m_pFindWidget, SIGNAL(gotoClicked(qreal,qreal)), this, SLOT(gotoWaypoint(qreal,qreal)));
            connect(m_pRouteInterface->w, SIGNAL(showAptInfo(int,QString)), this, SLOT(showAirportInfo(int,QString)));
            connect(m_pRouteInterface->w, SIGNAL(gotoWpt(qreal,qreal)), this, SLOT(gotoWaypoint(qreal,qreal)));
            connect(m_pRouteInterface->w, SIGNAL(routeNotamsRequested(QStringList)), this, SLOT(showNotam(QStringList)));
            connect(m_pMapWidget->inputHandler(), SIGNAL(rmbRequest(int,int)), m_pRouteInterface->w, SIGNAL(posClicked(int,int)));
            m_pMapWidget->popupMenu()->addRmbActionsGeneral(m_pRouteInterface->getActions());
        }
        else if (pluginList.at(i)->nameId() == "airspaces-display")
        {
            AirspacesPluginInterface *aspPlugin = qobject_cast<AirspacesPluginInterface *>(pluginList.at(i));
            aspPlugin->setMap(m_pMapWidget->map(), &m_activeContinents);
        }
        else if (pluginList.at(i)->nameId() == "aptnavaids-display")
        {
            AptNavaidsPluginInterface *aptPlugin = qobject_cast<AptNavaidsPluginInterface *>(pluginList.at(i));
            aptPlugin->setMap(m_pMapWidget->map(), &m_activeContinents);
        }
/*        else if (pluginList.at(i)->nameId() == "ead-browser")
        {
            EadBrowserPluginInterface *eadPlugin = qobject_cast<EadBrowserPluginInterface *>(pluginList.at(i));
            eadPlugin->setMap(m_pMapWidget->map(), &m_activeContinents);
        }
*/
    }

    createStatusBar();
    m_position = tr( NOT_AVAILABLE );
    m_zoom = QString::number( m_pMapWidget->tileZoomLevel() );
    setupStatusBar();

}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_pDbMaintWidget;
    delete m_pDatabase;
}

bool MainWindow::initOk()
{
    return m_initDbOk;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (!m_pDbMaintWidget.isNull())
    {
        if (!m_pDbMaintWidget->isWorkerFinished())
        {
            event->ignore();
            QMessageBox msgBox;
            msgBox.setText(tr("An update task is still running. Please wait until it has finished before closing the application!"));
            msgBox.setWindowIcon(QIcon("://oirfp32x32.png"));
            msgBox.exec();
            return;
        }
        m_pDbMaintWidget->close();
    }
    m_pFindWidget->close();
    m_popupBrowser->close();
    if (m_pRouteInterface != NULL)
    {
        m_pRouteInterface->closePopup();
    }
    writeSettings();
    event->accept();
}


void MainWindow::showAirportInfo()
{
    QPoint mousePos = m_pPopupMenu->mousePosition();
    QVector<const Marble::GeoDataPlacemark *> placeMark = m_pMapWidget->whichFeatureAt(mousePos);
    if (placeMark.empty())
    {
        return;  // no placemark at this position
    }
    QString airportId = placeMark.first()->name();
    m_popupBrowser->webviewWidget()->setUrlParam(airportId);

    QObject *senderObj = this->sender();
    for (int i = 0; i < m_actionList.size(); i++)
    {
        if (m_actionList.at(i) == senderObj)
        {
            m_popupBrowser->webviewWidget()->loadIndex(i);
            break;
        }
    }
    m_popupBrowser->show();
    m_popupBrowser->raise();
    m_popupBrowser->setWindowState(Qt::WindowActive);

}

void MainWindow::showAirportInfo(int index, QString code)
{
    m_popupBrowser->webviewWidget()->setUrlParam(code);
    m_popupBrowser->webviewWidget()->loadIndex(index);
    qDebug() << "Show " << index << code;

    m_popupBrowser->show();
    m_popupBrowser->raise();
    m_popupBrowser->setWindowState(Qt::WindowActive);
}

void MainWindow::showNotam()
{
    QPoint mousePos = m_pPopupMenu->mousePosition();
    QVector<const Marble::GeoDataPlacemark *> placeMark = m_pMapWidget->whichFeatureAt(mousePos);
    if (placeMark.empty())
    {
        return;  // no placemark at this position
    }
    QString airportId = placeMark.first()->name();
    this->showNotam(QStringList(airportId));
}

void MainWindow::showNotam(QStringList icaoCodes)
{
    m_popupBrowser->webviewWidget()->showNotam(icaoCodes);
    m_popupBrowser->show();
    m_popupBrowser->raise();
    m_popupBrowser->setWindowState(Qt::WindowActive);
}

void MainWindow::showAIPList()
{
    m_popupBrowser->webviewWidget()->showUrl(QUrl("http://gis.icao.int/gallery/ONLINE_AIPs.html"));
    m_popupBrowser->show();
    m_popupBrowser->raise();
    m_popupBrowser->setWindowState(Qt::WindowActive);
}



void MainWindow::showDownloadRegionDialog()
{
#ifdef WITH_MARBLEWIDGET
    if ( !m_downloadRegionDialog ) {
        m_downloadRegionDialog = new DownloadRegionDialog(m_pMapWidget);
        // it might be tempting to move the connects to DownloadRegionDialog's "accepted" and
        // "applied" signals, be aware that the "hidden" signal might be come before the "accepted"
        // signal, leading to a too early disconnect.
        connect( m_downloadRegionDialog, SIGNAL(accepted()), this, SLOT(downloadRegion()));
        connect( m_downloadRegionDialog, SIGNAL(applied()), this, SLOT(downloadRegion()));
    }
    // FIXME: get allowed range from current map theme
    m_downloadRegionDialog->setAllowedTileLevelRange( 0, 16 );
    m_downloadRegionDialog->setSelectionMethod( DownloadRegionDialog::VisibleRegionMethod );
    ViewportParams const * const viewport =
        m_pMapWidget->viewport();
    m_downloadRegionDialog->setSpecifiedLatLonAltBox( viewport->viewLatLonAltBox() );
    m_downloadRegionDialog->setVisibleLatLonAltBox( viewport->viewLatLonAltBox() );

    m_downloadRegionDialog->show();
    m_downloadRegionDialog->raise();
    m_downloadRegionDialog->activateWindow();
#endif
}

void MainWindow::showAbout()
{
    AboutWindow about(m_pDatabase, this);
    about.show();
    about.exec();


}

void MainWindow::showHelp()
{
    SimpleHelpWindow help(this);
    help.show();
    help.exec();
}

void MainWindow::setHome()
{
    QSettings settings;
    settings.beginGroup("home");
        settings.setValue("HomeLongitude", m_pMapWidget->centerLongitude());
        settings.setValue("HomeLatitude", m_pMapWidget->centerLatitude());
        settings.setValue("HomeZoom", m_pMapWidget->zoom());
    settings.endGroup();

    m_pMapWidget->model()->setHome(m_pMapWidget->centerLongitude(), m_pMapWidget->centerLatitude(), m_pMapWidget->zoom());

}

void MainWindow::showConfig()
{
    if (!m_pConfigDialog)
    {
        m_pConfigDialog = new ConfigDialog(m_pMapWidget, this);
    }
    m_pConfigDialog->show();
    m_pConfigDialog->exec();
}

void MainWindow::downloadRegion()
{
    Q_ASSERT( m_pDownloadRegionDialog );
    QVector<TileCoordsPyramid> const pyramid = m_pDownloadRegionDialog->region();
    if ( !pyramid.isEmpty() ) {
        m_pMapWidget->downloadRegion( pyramid );
    }
}

void MainWindow::showDbMaint()
{
    if (m_pDbMaintWidget.isNull())
    {
        m_pDbMaintWidget = new DbMaintWidget(m_pDatabase, this);
    }
    m_pDbMaintWidget->show();

}

void MainWindow::findWpt()
{
    m_pFindWidget->show();
}

void MainWindow::gotoWaypoint(qreal lon, qreal lat)
{
    GeoDataCoordinates coord(lon, lat, 0, GeoDataCoordinates::Degree);
    m_pMapWidget->centerOn(coord);
}

void MainWindow::addWaypoint(qreal lon, qreal lat, QString code, QString type)
{
    gotoWaypoint(lon,lat);
    if (m_pRouteInterface)
    {
        m_pRouteInterface->addWaypoint(lon, lat, code, type);
    }
}

void MainWindow::showPosition( const QString& position )
{
    m_position = position;
    updateStatusBar();
}

void MainWindow::showZoom( int zoom )
{
    m_zoom = QString::number( zoom );
    updateStatusBar();
}

void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
    //statusBar()->hide();
}

void MainWindow::readSettings()
{
    QSettings settings;
    m_pMapWidget->readPluginSettings(settings);

    settings.beginGroup("geometry");
        m_popupBrowserGeom.setX(settings.value("PopupBrowserX", 10).toInt());
        m_popupBrowserGeom.setY(settings.value("PopupBrowserY", 20).toInt());
        m_popupBrowserGeom.setWidth(settings.value("PopupBrowserWidth", 800).toInt());
        m_popupBrowserGeom.setHeight(settings.value("PopupBrowserHeight", 600).toInt());

        QRect mainwinGeom;
        mainwinGeom.setX(settings.value("MainWindowX", 10).toInt());
        mainwinGeom.setY(settings.value("MainWindowY", 10).toInt());
        mainwinGeom.setWidth(settings.value("MainWindowWidth", 800).toInt());
        mainwinGeom.setHeight(settings.value("MainWindowHeight", 600).toInt());
        this->setGeometry(mainwinGeom);


    settings.endGroup();
    settings.beginGroup("home");
        m_homeLon = settings.value("HomeLongitude", 8.2582014).toFloat();
        m_homeLat = settings.value("HomeLatitude", 46.9022335).toFloat();
        m_homeZoom = settings.value("HomeZoom", 2000).toInt();
    settings.endGroup();

    m_activeContinents = settings.value("view/activeContinents", QStringList("EU")).toStringList();
    m_measurementSystem = static_cast<MarbleLocale::MeasurementSystem> (settings.value("view/measurementSystem", MarbleLocale::NauticalSystem).toInt());

}

void MainWindow::writeSettings()
{
    QSettings settings;
    m_pMapWidget->writePluginSettings(settings);
    settings.beginGroup("geometry");
        settings.setValue("PopupBrowserX", m_popupBrowser->geometry().x());
        settings.setValue("PopupBrowserY", m_popupBrowser->geometry().y());
        settings.setValue("PopupBrowserWidth", m_popupBrowser->geometry().width());
        settings.setValue("PopupBrowserHeight", m_popupBrowser->geometry().height());

        settings.setValue("MainWindowX", this->geometry().x());
        settings.setValue("MainWindowY", this->geometry().y());
        settings.setValue("MainWindowWidth", this->geometry().width());
        settings.setValue("MainWindowHeight", this->geometry().height());


    settings.endGroup();
    settings.sync();

}

void MainWindow::setupWebview()
{
    QSettings settings;
    m_pPopupMenu = m_pMapWidget->popupMenu();
    m_popupBrowser = new WebviewPopup();
    m_popupBrowser->setWindowFlags(Qt::Window);
    m_popupBrowser->setWindowTitle("Webview");
    m_popupBrowser->setGeometry(m_popupBrowserGeom);

    settings.beginGroup("Webview");
    int size = settings.beginReadArray("Urls");
    if (size <= 0)
    {
       // write initial set of URL's if not yet done
       settings.endArray();
       settings.beginWriteArray("Urls", 4);
       settings.setArrayIndex(0);
       settings.setValue("description", "Skyvector");
       settings.setValue("url", "http://skyvector.com/airport/%1");
       settings.setArrayIndex(1);
       settings.setValue("description", "OurAiports");
       settings.setValue("url", "http://ourairports.com/airports/%1/pilot-info.html");
       settings.setArrayIndex(2);
       settings.setValue("description", "EDDH Pirep");
       settings.setValue("url", "http://www.eddh.de/info/lande_pireps.php?pi_icao=%1");
       settings.setArrayIndex(3);
       settings.setValue("description", "EDDH Info");
       settings.setValue("url", "http://www.eddh.de/info/landeinfo-ergebnis.php?eicao=%1");
       settings.endArray();
       settings.sync();
       size = settings.beginReadArray("Urls");
    }
    for (int i = 0; i < size; ++i) {
         settings.setArrayIndex(i);
         QString desc = QString("Show '%1'").arg(settings.value("description").toString());
         QString url = settings.value("url").toString();
         m_actionList.append(new QAction(desc, m_pPopupMenu));
         m_popupBrowser->webviewWidget()->appendUrlTemplate(desc, url);
    }
    settings.endArray();
    //
    for (int i = 0; i < m_actionList.size(); i++)
    {
        m_pPopupMenu->addActionForApt(Qt::RightButton, m_actionList.at(i));
        connect(m_actionList.at(i), SIGNAL(triggered()), this, SLOT(showAirportInfo()));
    }
    m_actionList.append(new QAction("Notam", m_pPopupMenu));
    m_pPopupMenu->addActionForApt(Qt::RightButton, m_actionList.last());
    connect(m_actionList.last(), SIGNAL(triggered()), this, SLOT(showNotam()));
}


void MainWindow::updateStatusBar()
{
    if ( m_pPositionLabel )
        m_pPositionLabel->setText( QString( "%1 %2" ).
        arg( tr( "Position: " ) ).arg( m_position ) );

    if ( m_pZoomLabel )
        m_pZoomLabel->setText( QString( "%1 %2" )
        .arg( tr( "Zoom: " ) ).arg( m_zoom ) );

}

void MainWindow::setupStatusBar()
{
    statusBar()->setSizeGripEnabled( true );
    statusBar()->setContextMenuPolicy( Qt::ActionsContextMenu );

    setupDownloadProgressBar();

    m_pPositionLabel = new QLabel( );
    m_pPositionLabel->setIndent( 5 );
    QString templatePositionString =
        QString( "%1 000\xb0 00\' 00\"_, 000\xb0 00\' 00\"_" ).arg("Position: ");
    int maxPositionWidth = fontMetrics().boundingRect(templatePositionString).width()
                            + 2 * m_pPositionLabel->margin() + 2 * m_pPositionLabel->indent();
    m_pPositionLabel->setFixedWidth( maxPositionWidth );
    statusBar()->addPermanentWidget ( m_pPositionLabel );

    m_pZoomLabel = new QLabel( );
    m_pZoomLabel->setIndent( 5 );
    QString templateZoomString =
        QString( "%1 00" ).arg("Zoom: ");
    int maxZoomWidth = fontMetrics().boundingRect(templateZoomString).width()
                            + 2 * m_pZoomLabel->margin() + 2 * m_pZoomLabel->indent();
    m_pZoomLabel->setFixedWidth( maxZoomWidth );
    statusBar()->addPermanentWidget ( m_pZoomLabel );


    connect( m_pMapWidget, SIGNAL(mouseMoveGeoPosition(QString)),
              this, SLOT(showPosition(QString)) );
    connect( m_pMapWidget, SIGNAL(tileLevelChanged(int)),
            this, SLOT(showZoom(int)) );

    updateStatusBar();
}

void MainWindow::setupDownloadProgressBar()
{
    m_pDownloadProgressBar = new QProgressBar;
    m_pDownloadProgressBar->setVisible( true );
    statusBar()->addPermanentWidget( m_pDownloadProgressBar );

    HttpDownloadManager * const downloadManager =
        m_pMapWidget->model()->downloadManager();
    Q_ASSERT( downloadManager );
    connect( downloadManager, SIGNAL(progressChanged( int, int )), SLOT(handleProgress( int, int )) );
    connect( downloadManager, SIGNAL(jobRemoved()), SLOT(removeProgressItem()) );
}

void MainWindow::handleProgress( int active, int queued ){
    m_pDownloadProgressBar->setUpdatesEnabled( false );
    if ( m_pDownloadProgressBar->value() < 0 ) {
        m_pDownloadProgressBar->setMaximum( 1 );
        m_pDownloadProgressBar->setValue( 0 );
        m_pDownloadProgressBar->setVisible( true );
    } else {
        m_pDownloadProgressBar->setMaximum( qMax<int>( m_pDownloadProgressBar->maximum(), active + queued ) );
    }

    m_pDownloadProgressBar->setUpdatesEnabled( true );
}

void MainWindow::removeProgressItem(){
    m_pDownloadProgressBar->setUpdatesEnabled( false );
    m_pDownloadProgressBar->setValue( m_pDownloadProgressBar->value() + 1 );
    if ( m_pDownloadProgressBar->value() == m_pDownloadProgressBar->maximum() ) {
        m_pDownloadProgressBar->reset();
        m_pDownloadProgressBar->setVisible( false );
    }

    m_pDownloadProgressBar->setUpdatesEnabled( true );
}

void MainWindow::printDebugInt(int x)
{
    return;
    qDebug() << "logZoom:" << x;
    qDebug() << "distanceFromZoom:" << m_pMapWidget->distanceFromZoom(x);
    int radius = m_pMapWidget->radius();
    qDebug() << "radius:" << radius;
    qDebug() << "distanceFromRadius:" << m_pMapWidget->distanceFromRadius(radius);

}

#ifdef LOAD_FROM_CSV
// runways.csv und airports.csv mit Semikolon statt Komma neu gespeichert!!

void MainWindow::loadRunways(QMultiHash<QString, QStringList> &table)
{
    QFile file("runways.csv");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    const QRegExp regExp("^\"|\"$");
    QTextStream in(&file);
    while (!in.atEnd()) {
        QString line = in.readLine();
        QStringList runway = line.split(",");
        for (int i = 0; i < runway.size(); i++)
        {
            runway[i].replace(regExp, "");
        }

        if (runway.at(0).compare("id") == 0)
        {
            continue;
        }
        QString key = runway.at(2);
        QStringList data;
        // Data: runway id 1 / id 2, length, width, surface, closed
        QString length = QString::number((int)(runway.at(3).toFloat() * 0.3048));
        QString width = QString::number((int)(runway.at(4).toFloat() * 0.3048));
        data << runway.at(8) << runway.at(14) << length << width << runway.at(5) << runway.at(7);
        table.insert(key, data);
    }
    file.close();
}

void MainWindow::addAirports(GeoDataDocument &document)
{

    QMultiHash<QString, QStringList> runways;

    loadRunways(runways);

    QFile file("airports_europe.csv");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);
    while (!in.atEnd()) {
        QString line = in.readLine();
        QStringList airport = line.split(";");
        if (airport.at(0).compare("id") == 0)
        {
            continue;
        }

        //Create the placemark representing the airport
        GeoDataPlacemark *placemarkTemp = new GeoDataPlacemark(airport.at(1));
        float fLatitude = airport.at(4).toFloat();
        float fLongitude = airport.at(5).toFloat();
        float fAltitude = airport.at(6).toFloat() * 0.3048; // ft to meters
        placemarkTemp->setCoordinate( fLongitude, fLatitude, fAltitude, GeoDataCoordinates::Degree );
        placemarkTemp->setCountryCode(airport.at(8));

        QString airportId = airport.at(1);
        QString sDescription = airport.at(3);
        sDescription.append("<br>");
        sDescription.append(airport.at(10));
        sDescription.append(" ");
        sDescription.append(airport.at(8));
        sDescription.append("<br>");
        sDescription.append(airport.at(15));
        sDescription.append("<br>");

        QList<QStringList> runwayData = runways.values(airportId);
        for (int i = 0; i < runwayData.size(); i++)
        {
            QStringList data = runwayData.at(i);
            QString t = QString("Runway %1/%2 %3x%4 %5").arg(data.at(0), data.at(1), data.at(2), data.at(3), data.at(4));
            if (data.at(5).compare("0") != 0)
            {
                t.append(" closed");
            }
            sDescription.append(t);
            sDescription.append("<br>");
        }

        placemarkTemp->setDescription(sDescription);
        //placemarkTemp->set
        //placemarkTemp->setAbstractView();
        //placemarkTemp->setStyle();


        //Add styles (icons, colors, etc.) to the placemarks
// icon wird nich angezeigt , vorerst default lassen
//        GeoDataStyle *styleTemp = new GeoDataStyle;
//        GeoDataIconStyle iconStyle;
//        iconStyle.setIconPath("picnic.jpg");
//        styleTemp->setIconStyle(iconStyle);
//        placemarkTemp->setStyle(styleTemp);


        document.append(placemarkTemp );
    }
    file.close();

}

#endif


CONFIG -= debug_and_release
CONFIG( debug, debug|release )  {
  CONFIG -= release
}
else {
  CONFIG -= debug
  CONFIG += release
}

TEMPLATE = app
TARGET = flightplanner

#   #define WITH_MARBLEWIDGET
#DEFINES += WITH_MARBLEWIDGET

include(common.pri)

win32 {
#Windows
    DEFINES += WITH_WEBKIT
    QT += core gui widgets webkitwidgets sql xml
} else {
#Linux
    QT += core gui widgets webenginewidgets sql xml
}

DESTDIR = $$DIST_DIR


INCLUDEPATH += . ./libs/include \
                $$MARBLE_INC \
                $$MARBLE_SRC \
                $$MARBLE_SRC/graphicsview \
                $$MARBLE_SRC/layers \
                $$MARBLE_SRC/geodata \
                $$MARBLE_SRC/geodata/data \
                $$MARBLE_SRC/geodata/parser \
                $$MARBLE_SRC/geodata/scene


# Input
SOURCES += main.cpp \
    mainwindow.cpp \
    databasehandler.cpp \
    openaipreader.cpp \
    dataimportwidget.cpp \
    ourairportsreader.cpp \
#    mapeventfilter.cpp \
#    waypointlayer.cpp \
#    waypoint.cpp
    findwidget.cpp \
    aboutwindow.cpp \
    simplehelpwindow.cpp \
    webviewwidget.cpp \
    configdialog.cpp \
    webviewpopup.cpp \
    fpsplashscreen.cpp \
    openaipupdater.cpp \
    oaipconfigmodel.cpp \
    oaipupdatewidget.cpp \
    dbmaintwidget.cpp \
    waitingspinnerwidget.cpp \
    oaipconfigtable.cpp \
    oaipconfigwidget.cpp \
    aircraftconfigmodel.cpp \
    aircraftconfigtable.cpp \
    speedunitdelegate.cpp \
    aircraft.cpp \
    rateunitdelegate.cpp \
    fivelncupdater.cpp
#    mymarblewidgetinputhandler.cpp

!contains( DEFINES, WITH_MARBLEWIDGET ) {
    SOURCES += mapwidget.cpp \
    mapwidgetinputhandler.cpp \
    mapwidgetpopupmenu.cpp
}


FORMS += \
    mainwindow.ui \
    dataimportwidget.ui \
    findwidget.ui \
    aboutwindow.ui \
    simplehelpwindow.ui \
    configdialog.ui \
    webviewpopup.ui \
    oaipupdatewidget.ui \
    dbmaintwidget.ui \
    oaipconfigwidget.ui

HEADERS += \
    mainwindow.h \
    databasehandler.h \
    openaipreader.h \
    dataimportwidget.h \
    ourairportsreader.h \
#    mapeventfilter.h \
#    waypointlayer.h \
#    waypoint.h
    findwidget.h \
    aboutwindow.h \
    simplehelpwindow.h \
    webviewwidget.h \
    configdialog.h \
    defs.h \
    webviewpopup.h \
    fpsplashscreen.h \
    openaipupdater.h \
    oaipconfigmodel.h \
    oaipupdatewidget.h \
    dbmaintwidget.h \
    waitingspinnerwidget.h \
    sql_queries.h \
    oaipconfigtable.h \
    oaipconfigwidget.h \
    aircraftconfigmodel.h \
    aircraftconfigtable.h \
    speedunitdelegate.h \
    aircraft.h \
    rateunitdelegate.h \
    fivelncupdater.h
#    mymarblewidgetinputhandler.h

!contains( DEFINES, WITH_MARBLEWIDGET ) {
   HEADERS += mapwidget.h \
    mapwidgetinputhandler.h \
    mapwidgetpopupmenu.h
}


RESOURCES += \
    data/resources.qrc

DISTFILES += \
    common.pri

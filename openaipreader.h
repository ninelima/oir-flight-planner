//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef OPENAIPREADER_H
#define OPENAIPREADER_H

#include <QObject>
#include <QXmlStreamReader>
#include <QHash>
#include <QSqlQuery>
#include <QStringList>
#include <QIODevice>


class OpenAIPReader: public QObject
{
typedef struct  {
        QString name;
        QString sfc;
        QString status;
        QString length;
        QString width;
        QString lighted;
    } runway;

    Q_OBJECT
public:
    explicit OpenAIPReader(QObject *parent = 0);

signals:
    void recordProceeded(QString);
    void fileFinished();

public slots:
    void readAirspaceFiles(QStringList fileNames);
    void readAirportFiles(QStringList fileNames);
    void readNavaidsFiles(QStringList fileNames);

    QString readAirspaceData(QIODevice *ioDev);
    QString readAirportData(QIODevice *ioDev);
    QString readNavaidsData(QIODevice *ioDev);

    void readAirspaceData(QIODevice *ioDev, QString fileDate, QString country);
    void readAirportData(QIODevice *ioDev, QString fileDate, QString country);
    void readNavaidsData(QIODevice *ioDev, QString fileDate, QString country);


private:
    QSqlQuery query;
    QHash<QString, QString> cntry_continent;


    void parseAirspace(QXmlStreamReader& xml);
    void parseAirport(QXmlStreamReader& xml);
    void parseNavaid(QXmlStreamReader& xml);


};

#endif // OPENAIPREADER_H

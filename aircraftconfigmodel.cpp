//
// Copyright 2016 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "aircraftconfigmodel.h"

#include <QBrush>
#include <QColor>
#include <QSettings>

#include "aircraft.h"


AircraftConfigModel::AircraftConfigModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    /*
    QSettings settings;
    settings.beginGroup("Aircraft");
    int size = settings.beginReadArray("Data");
    for (int i = 0; i < size; ++i) {
         settings.setArrayIndex(i);
         Aircraft *newAircraft = new Aircraft;
         newAircraft->immatriculation = settings.value("imma", "-").toString();
         newAircraft->type = settings.value("type", "").toString();
         newAircraft->cruiseSpeed = settings.value("cruiseSpeed", 120).toInt();
         newAircraft->climbSpeed = settings.value("climbSpeed", 100).toInt();
         newAircraft->climbRate = settings.value("climbRate", 500).toInt();
         newAircraft->descentSpeed = settings.value("descentSpeed", 130).toInt();
         newAircraft->descentRate = settings.value("descentRate", 500).toInt();
         newAircraft->speedUnit = settings.value("speedUnit", 0).toInt();
         newAircraft->rateUnit = settings.value("rateUnit", 0).toInt();
         newAircraft->fuelFlow = settings.value("fuelflow", 32).toReal();
         newAircraft->fuelUsable = settings.value("fuelusable", 150).toInt();
         m_pAircraftList.append(newAircraft);
    }
    settings.endArray();
    settings.endGroup();
    */
    m_pAircraftList = AircraftList::instance();
}

AircraftConfigModel::~AircraftConfigModel()
{
    clear();
}

int AircraftConfigModel::rowCount(const QModelIndex & /* parent */) const
{
    return m_pAircraftList->size();
}

int AircraftConfigModel::columnCount(const QModelIndex & /* parent */) const
{
    return 11;
}

QModelIndex AircraftConfigModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return createIndex(row, column);
}


void AircraftConfigModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, m_pAircraftList->size()-1);
    for (int i = 0; i < m_pAircraftList->size(); i++)
    {
        delete m_pAircraftList->at(i);
    }
    m_pAircraftList->clear();
    endRemoveRows();
}

bool AircraftConfigModel::isEmpty()
{
    return m_pAircraftList->isEmpty();
}

bool AircraftConfigModel::removeRow(int row, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginRemoveRows(QModelIndex(), row, row);
    delete m_pAircraftList->at(row);
    m_pAircraftList->removeAt(row);
    endRemoveRows();
    return true;
}

bool AircraftConfigModel::insertRow(int row, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginInsertRows(QModelIndex(), row, row);
    Aircraft *newAircraft = new Aircraft;
    m_pAircraftList->insert(row, newAircraft);
    endInsertRows();
    return true;
}


QVariant AircraftConfigModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        switch (index.column())
        {
        case 0:
            return m_pAircraftList->at(index.row())->getImmatriculation();
            break;
        case 1:
            return m_pAircraftList->at(index.row())->getType();
            break;
        case 2:
            return m_pAircraftList->at(index.row())->getCruiseSpeed();
            break;
        case 3:
            return m_pAircraftList->at(index.row())->getClimbSpeed();
            break;
        case 4:
            return m_pAircraftList->at(index.row())->getClimbRate();
            break;
        case 5:
            return m_pAircraftList->at(index.row())->getDescentSpeed();
            break;
        case 6:
            return m_pAircraftList->at(index.row())->getDescentRate();
            break;
        case 7:
            if (role == Qt::EditRole)
            {
                return m_pAircraftList->at(index.row())->getSpeedUnit();
            }
            else
            {
                return Aircraft::speedUnits().at(m_pAircraftList->at(index.row())->getSpeedUnit());
            }
            break;
        case 8:
            if (role == Qt::EditRole)
            {
                return m_pAircraftList->at(index.row())->getRateUnit();
            }
            else
            {
                return Aircraft::rateUnits().at(m_pAircraftList->at(index.row())->getRateUnit());
            }
            break;
        case 9:
            return m_pAircraftList->at(index.row())->getFuelFlow();
            break;
        case 10:
            return m_pAircraftList->at(index.row())->getFuelUsable();
            break;
        default:
            return QVariant();
        }

    }

    return QVariant();
}

QVariant AircraftConfigModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal)
        {
            switch (section)
            {
            case 0:
                return tr("Immatr.");
                break;
            case 1:
                return tr("Type");
                break;
            case 2:
                return tr("Cruise");
                break;
            case 3:
                return tr("Climb");
                break;
            case 4:
                return tr("Climb rate");
                break;
            case 5:
                return tr("Descent");
                break;
            case 6:
                return tr("Descent rate");
                break;
            case 7:
                return tr("Speed unit");
                break;
            case 8:
                return tr("Rate unit");
                break;
            case 9:
                return tr("Fuelflow");
                break;
            case 10:
                return tr("Usable");
                break;
            default:
                return QVariant();
            }
        }
        else if (orientation == Qt::Vertical)
        {
            return QString::number(section+1);
        }
    }

    return QVariant();
}

bool AircraftConfigModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(role)
    switch (index.column())
    {
    case 0:
        m_pAircraftList->at(index.row())->setImmatriculation(value.toString());
        break;
    case 1:
        m_pAircraftList->at(index.row())->setType(value.toString());
        break;
    case 2:
        m_pAircraftList->at(index.row())->setCruiseSpeed(value.toInt());
        break;
    case 3:
        m_pAircraftList->at(index.row())->setClimbSpeed(value.toInt());
        break;
    case 4:
        m_pAircraftList->at(index.row())->setClimbRate(value.toUInt());
        break;
    case 5:
        m_pAircraftList->at(index.row())->setDescentSpeed(value.toInt());
        break;
    case 6:
        m_pAircraftList->at(index.row())->setDescentRate(value.toInt());
        break;
    case 7:
        m_pAircraftList->at(index.row())->setSpeedUnit(value.toInt());
        break;
    case 8:
        m_pAircraftList->at(index.row())->setRateUnit(value.toInt());
        break;
    case 9:
        m_pAircraftList->at(index.row())->setFuelFlow(value.toReal());
        break;
    case 10:
        m_pAircraftList->at(index.row())->setFuelUsable(value.toInt());
        break;
    }
    return true;
}

Qt::ItemFlags AircraftConfigModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index)
    return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable ;
}


void AircraftConfigModel::moveUp(int row)
{

    if (row > 0)
    {
        beginMoveRows(QModelIndex(), row, row, QModelIndex(), row - 1);
        m_pAircraftList->move(row, row-1);
        endMoveRows();
    }
}

void AircraftConfigModel::moveDown(int row)
{
    if (row < m_pAircraftList->size()-1)
    {
        beginMoveRows(QModelIndex(), row, row, QModelIndex(), row + 2);
        m_pAircraftList->move(row, row+1);
        endMoveRows();
    }
}

void AircraftConfigModel::saveConfig()
{
    QSettings settings;

    settings.beginGroup("Aircraft");

    int size = rowCount();
    settings.beginWriteArray("Data", size);
    for (int i = 0; i < size; i++)
    {
       settings.setArrayIndex(i);
       settings.setValue("imma", m_pAircraftList->at(i)->getImmatriculation());
       settings.setValue("type", m_pAircraftList->at(i)->getType());
       settings.setValue("cruiseSpeed", m_pAircraftList->at(i)->getCruiseSpeed());
       settings.setValue("climbSpeed", m_pAircraftList->at(i)->getClimbSpeed());
       settings.setValue("climbRate", m_pAircraftList->at(i)->getClimbRate());
       settings.setValue("descentSpeed", m_pAircraftList->at(i)->getDescentSpeed());
       settings.setValue("descentRate", m_pAircraftList->at(i)->getDescentRate());
       settings.setValue("speedUnit", m_pAircraftList->at(i)->getSpeedUnit());
       settings.setValue("rateUnit", m_pAircraftList->at(i)->getRateUnit());
       settings.setValue("fuelflow", m_pAircraftList->at(i)->getFuelFlow());
       settings.setValue("fuelusable", m_pAircraftList->at(i)->getFuelUsable());

    }
    settings.endArray();
    settings.endGroup();
    settings.sync();

}






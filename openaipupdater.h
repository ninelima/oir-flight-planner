//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef OPENAIPUPDATER_H
#define OPENAIPUPDATER_H

#include <QObject>
#include <QSqlQuery>
#include <QDate>
#include <QPointer>
#include <QUrl>

class QNetworkAccessManager;
class QNetworkReply;

class NetworkReply : public QObject
{
    Q_OBJECT
public:
    explicit NetworkReply(QObject *parent = 0);
    void setReply(QNetworkReply *reply);
    QNetworkReply *getNetworkReply();
    void setDate(QDate date);
    QDate getDate();
    void setFilename(QString fileName);
    QString getFilename();

private:
    QNetworkReply *m_pReply;
    QDate m_fileDate;
    QString m_fileName;
signals:
    void readyRead();
    void finished();
};

class OpenAipUpdater : public QObject
{
    Q_OBJECT
public:
    explicit OpenAipUpdater(QObject *parent = 0);
    ~OpenAipUpdater();
    bool isWorking() { return m_isWorking; }

signals:

public slots:
    void getIndexFromWeb();
    void getFiles();

private slots:
    void parseIndex();
    void replyFinished();
    void getNextFile();

private:
    QUrl m_url;
    QNetworkAccessManager *m_pManager;
    QList<NetworkReply *> m_replyList;
    QSqlQuery m_query;
    bool m_isWorking;

signals:
    void textlineToAdd(QString);
    void repliesFinished();
    void getIndexFinished();

};

#endif // OPENAIPUPDATER_H

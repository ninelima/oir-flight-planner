//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 47 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:02:47 +0000 (Wed, 07 Nov 2018) $
//

#include "openaipreader.h"

#include <QFile>
#include <QFileInfo>
#include <QStringList>
#include <QSqlQuery>
#include <QVariant>
#include <QDateTime>


OpenAIPReader::OpenAIPReader(QObject *parent) :
    QObject(parent)
{

    // fill hash table with country/continent codes
    QString sQuery;
    // use http://dev.maxmind.com/geoip/legacy/codes/country_continent/ to fill the database table
    sQuery = "SELECT iso_country, continent FROM cntry_continent;";
    query.setForwardOnly(true);
    query.exec(sQuery);
    while (query.next())
    {
        cntry_continent.insert(query.value(0).toString(), query.value(1).toString());
    }

}

void OpenAIPReader::readAirspaceFiles(QStringList fileNames)
{
    for (int i = 0; i < fileNames.size(); i++)
    {
        QString fileVersion;
        QString fileDate;
        query.exec("BEGIN TRANSACTION");
        QFile file(fileNames.at(i));
        if (!file.open(QIODevice::ReadOnly)) {
    //     out << "  Couldn't open the file." << endl << endl << endl;
         continue;
        }

        fileVersion = readAirspaceData(&file);
        QFileInfo fileInfo(file);
        fileDate = fileInfo.created().toString("yyyy-MM-dd");
        QString sQuery = "UPDATE oaipfiles SET version='%1', dateInst='%2', installed='1' WHERE country='%3' AND type='asp'";
        sQuery = sQuery.arg(fileVersion).arg(fileDate).arg(fileInfo.baseName().left(2));
        query.exec(sQuery);
        //query.exec(sQuery);
        if (query.numRowsAffected() == 0)  // try to update
        {
            sQuery = "INSERT INTO oaipfiles (country, type, version, dateInst, installed) VALUES ('%1', 'asp', '%2', '%3', '1')";
            sQuery = sQuery.arg(fileInfo.baseName().left(2)).arg(fileVersion).arg(fileDate);
            query.exec(sQuery);
        }
        query.exec("END TRANSACTION");
    }

    emit fileFinished();

}

void OpenAIPReader::readAirportFiles(QStringList fileNames)
{

    for (int i = 0; i < fileNames.size(); i++)
    {
        QString fileVersion;
        QString fileDate;
        query.exec("BEGIN TRANSACTION");
        QFile file(fileNames.at(i));
        if (!file.open(QIODevice::ReadOnly)) {
    //     out << "  Couldn't open the file." << endl << endl << endl;
         continue;
        }

        fileVersion = readAirportData(&file);

        QFileInfo fileInfo(file);
        fileDate = fileInfo.created().toString("yyyy-MM-dd");
        QString sQuery = "UPDATE oaipfiles SET version='%1', dateInst='%2', installed='1' WHERE country='%3' AND type='wpt'";
        sQuery = sQuery.arg(fileVersion).arg(fileDate).arg(fileInfo.baseName().left(2));
        query.exec(sQuery);
        //query.exec(sQuery);
        if (query.numRowsAffected() == 0)  // try to update
        {
            sQuery = "INSERT INTO oaipfiles (country, type, version, dateInst, installed) VALUES ('%1', 'wpt', '%2', '%3', '1')";
            sQuery = sQuery.arg(fileInfo.baseName().left(2)).arg(fileVersion).arg(fileDate);
            query.exec(sQuery);
        }
        query.exec("END TRANSACTION");
    }

    emit fileFinished();
}

void OpenAIPReader::readNavaidsFiles(QStringList fileNames)
{
    for (int i = 0; i < fileNames.size(); i++)
    {
        QString fileDate;
        QString fileVersion;
        query.exec("BEGIN TRANSACTION");
        QFile file(fileNames.at(i));
        if (!file.open(QIODevice::ReadOnly)) {
    //     out << "  Couldn't open the file." << endl << endl << endl;
         continue;
        }

        fileVersion = readNavaidsData(&file);
        QFileInfo fileInfo(file);
        fileDate = fileInfo.created().toString("yyyy-MM-dd");
        QString sQuery = "UPDATE oaipfiles SET version='%1', dateInst='%2', installed='1' WHERE country='%3' AND type='nav'";
        sQuery = sQuery.arg(fileVersion).arg(fileDate).arg(fileInfo.baseName().left(2));
        query.exec(sQuery);
        //query.exec(sQuery);
        if (query.numRowsAffected() == 0)  // try to update
        {
            sQuery = "INSERT INTO oaipfiles (country, type, version, dateInst, installed) VALUES ('%1', 'nav', '%2', '%3', '1')";
            sQuery = sQuery.arg(fileInfo.baseName().left(2)).arg(fileVersion).arg(fileDate);
            query.exec(sQuery);
        }
        query.exec("END TRANSACTION");
    }

    emit fileFinished();
}

QString OpenAIPReader::readAirspaceData(QIODevice *ioDev)
{
    QString dataVersion;
    QXmlStreamReader xmlreader(ioDev);

    while(!xmlreader.atEnd() && !xmlreader.hasError()) {
        /* Read next element.*/
        QXmlStreamReader::TokenType token = xmlreader.readNext();
        /* If token is just StartDocument, we'll go to next.*/
        if(token == QXmlStreamReader::StartDocument) {
            continue;
        }
        /* If token is StartElement, we'll see if we can read it.*/
        if(token == QXmlStreamReader::StartElement) {
            /* If it's named OPENAIP or AIRSPACES, we'll go to the next.*/
            if(xmlreader.name() == "OPENAIP") {
                dataVersion = xmlreader.attributes().value("VERSION").toString();
                continue;
            }
            if(xmlreader.name() == "AIRSPACES") {
                continue;
            }            /* If it's named ASP, we'll dig the information from there.*/
            if(xmlreader.name() == "ASP") {
                this->parseAirspace(xmlreader);
            }
        }
    }
    return dataVersion;
}

QString OpenAIPReader::readAirportData(QIODevice *ioDev)
{
    QString dataVersion;
    QXmlStreamReader xmlreader(ioDev);

    while(!xmlreader.atEnd() && !xmlreader.hasError()) {
        /* Read next element.*/
        QXmlStreamReader::TokenType token = xmlreader.readNext();
        /* If token is just StartDocument, we'll go to next.*/
        if(token == QXmlStreamReader::StartDocument) {
            continue;
        }
        /* If token is StartElement, we'll see if we can read it.*/
        if(token == QXmlStreamReader::StartElement) {
            /* If it's named OPENAIP or WAYPOINTS, we'll go to the next.*/
            if(xmlreader.name() == "OPENAIP") {
                QXmlStreamAttributes attributes = xmlreader.attributes();
                dataVersion = xmlreader.attributes().value("VERSION").toString();
                continue;
            }
            if(xmlreader.name() == "WAYPOINTS") {
                continue;
            }            /* If it's named AIRPORT, we'll dig the information from there.*/
            if(xmlreader.name() == "AIRPORT") {
                this->parseAirport(xmlreader);
            }
        }
    }
    return dataVersion;
}

QString OpenAIPReader::readNavaidsData(QIODevice *ioDev)
{
    QString dataVersion;
    QXmlStreamReader xmlreader(ioDev);

    while(!xmlreader.atEnd() && !xmlreader.hasError()) {

        /* Read next element.*/
        QXmlStreamReader::TokenType token = xmlreader.readNext();
        /* If token is just StartDocument, we'll go to next.*/
        if(token == QXmlStreamReader::StartDocument) {
            continue;
        }
        /* If token is StartElement, we'll see if we can read it.*/
        if(token == QXmlStreamReader::StartElement) {
            /* If it's named OPENAIP or NAVAIDS, we'll go to the next.*/
            if(xmlreader.name() == "OPENAIP") {
                dataVersion = xmlreader.attributes().value("VERSION").toString();
                continue;
            }
            if(xmlreader.name() == "NAVAIDS") {
                continue;
            }            /* If it's named NAVAID, we'll dig the information from there.*/
            if(xmlreader.name() == "NAVAID") {
                this->parseNavaid(xmlreader);
            }
        }

    }
    return dataVersion;

}

void OpenAIPReader::readAirspaceData(QIODevice *ioDev, QString fileDate, QString country)
{
    query.exec("BEGIN TRANSACTION");
    QString fileVersion = readAirspaceData(ioDev);
    QString sQuery = "UPDATE oaipfiles SET version='%1', dateInst='%2', installed='1' WHERE country='%3' AND type='asp'";
    sQuery = sQuery.arg(fileVersion).arg(fileDate).arg(country);
    query.exec(sQuery);
    if (query.numRowsAffected() == 0)  // try to update
    {
        sQuery = "INSERT INTO oaipfiles (country, type, version, dateInst, installed) VALUES ('%1', 'asp', '%2', '%3', '1')";
        sQuery = sQuery.arg(country).arg(fileVersion).arg(fileDate);
        query.exec(sQuery);
    }
    query.exec("END TRANSACTION");
}

void OpenAIPReader::readNavaidsData(QIODevice *ioDev, QString fileDate, QString country)
{
    query.exec("BEGIN TRANSACTION");
    QString fileVersion = readNavaidsData(ioDev);
    QString sQuery = "UPDATE oaipfiles SET version='%1', dateInst='%2', installed='1' WHERE country='%3' AND type='nav'";
    sQuery = sQuery.arg(fileVersion).arg(fileDate).arg(country);
    query.exec(sQuery);
    if (query.numRowsAffected() == 0)  // try to update
    {
        sQuery = "INSERT INTO oaipfiles (country, type, version, dateInst, installed) VALUES ('%1', 'nav', '%2', '%3', '1')";
        sQuery = sQuery.arg(country).arg(fileVersion).arg(fileDate);
        query.exec(sQuery);
    }
    query.exec("END TRANSACTION");
}

void OpenAIPReader::readAirportData(QIODevice *ioDev, QString fileDate, QString country)
{
    query.exec("BEGIN TRANSACTION");
    QString fileVersion = readAirportData(ioDev);
    QString sQuery = "UPDATE oaipfiles SET version='%1', dateInst='%2', installed='1' WHERE country='%3' AND type='wpt'";
    sQuery = sQuery.arg(fileVersion).arg(fileDate).arg(country);
    query.exec(sQuery);
    if (query.numRowsAffected() == 0)  // try to update
    {
        sQuery = "INSERT INTO oaipfiles (country, type, version, dateInst, installed) VALUES ('%1', 'wpt', '%2', '%3', '1')";
        sQuery = sQuery.arg(country).arg(fileVersion).arg(fileDate);
        query.exec(sQuery);
    }
    query.exec("END TRANSACTION");
}

void OpenAIPReader::parseAirspace(QXmlStreamReader &xml)
{
    QString cat;
    QString id;
    QString country;
    QString name;
    QString top;
    QString topRef;
    QString topUnit;
    QString bottom;
    QString bottomRef;
    QString bottomUnit;
    QString polygon;
    /* Let's check that we're really getting a ASP. */
    if(xml.tokenType() != QXmlStreamReader::StartElement &&
            xml.name() == "ASP") {
        return;
    }
    /* Let's get the attributes for ASP (the airspace type) */
    QXmlStreamAttributes attributes = xml.attributes();
    /* Let's check that ASP has CATEGORY attribute. */
    if(attributes.hasAttribute("CATEGORY")) {
        /* We'll add it to the string. */
        cat = attributes.value("CATEGORY").toString();
    }
    /* Next element... */
    xml.readNext();
    /*
     * We're going to loop over the things because the order might change.
     * We'll continue the loop until we hit an EndElement named ASP.
     */
    while(!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "ASP"))
    {
        if(xml.tokenType() == QXmlStreamReader::StartElement)
        {
            if(xml.name() == "ID")
            {
                id = xml.readElementText();
            }
            else if(xml.name() == "COUNTRY")
            {
                country = xml.readElementText();
            }
            else if(xml.name() == "NAME")
            {
                name = xml.readElementText();
            }
            else if(xml.name() == "ALTLIMIT_TOP")
            {
                topRef = xml.attributes().value("REFERENCE").toString();

                xml.readNext();
                while(!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "ALTLIMIT_TOP"))
                {
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "ALT")
                        {
                            topUnit = xml.attributes().value("UNIT").toString();
                            top = xml.readElementText();
                        }
                   }
                    /* ...and next... */
                    xml.readNext();
                }
            }
            else if(xml.name() == "ALTLIMIT_BOTTOM")
            {
                bottomRef = xml.attributes().value("REFERENCE").toString();

                xml.readNext();
                while(!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "ALTLIMIT_BOTTOM"))
                {
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "ALT")
                        {
                            bottomUnit = xml.attributes().value("UNIT").toString();
                            bottom = xml.readElementText();
                        }
                   }
                    /* ...and next... */
                    xml.readNext();
                }
            }
            else if(xml.name() == "POLYGON")
            {
                polygon = xml.readElementText();
            }
        }
        /* ...and next... */
        xml.readNext();
    }

    QString sQuery = "UPDATE airspaces SET id='%1',country='%2', name='%3', cat='%4', top='%5', topref='%6', topunit='%7', bottom='%8', bottomref='%9', bottomunit='%10', polygon='%11', continent='%12' WHERE name='%3';";
    sQuery = sQuery.arg(id).arg(country).arg(name).arg(cat).arg(top).arg(topRef).arg(topUnit).arg(bottom).arg(bottomRef).arg(bottomUnit).arg(polygon).arg(cntry_continent.value(country));
    query.exec(sQuery);
    if (query.numRowsAffected() == 0)  // try to update
    {
        QString sQuery = "INSERT INTO airspaces (id,country,name,cat,top,topref,topunit,bottom,bottomref,bottomunit,polygon, continent) VALUES ('%1','%2','%3','%4','%5','%6','%7','%8','%9','%10','%11', '%12');";
        sQuery = sQuery.arg(id).arg(country).arg(name).arg(cat).arg(top).arg(topRef).arg(topUnit).arg(bottom).arg(bottomRef).arg(bottomUnit).arg(polygon).arg(cntry_continent.value(country));
        query.exec(sQuery);
    }
    emit recordProceeded(QString("Airspace %1 processed").arg(name));


}

void OpenAIPReader::parseAirport(QXmlStreamReader &xml)
{
    QString icao = "-";
    QString type;
    QString name;
    float lat;
    float lon;
    float elev;
    QString country;
    QString municipality;
    QString home_url;
    QString wikipedia_url;
    QStringList radioCat;
    QString elevUnit;

    QList<runway> runways;
    /* Let's check that we're really getting a AIRPORT. */
    if(xml.tokenType() != QXmlStreamReader::StartElement &&
            xml.name() == "AIRPORT") {
        return;
    }
    /* Let's get the attributes for AIRPORT (the airport type) */
    QXmlStreamAttributes attributes = xml.attributes();
    /* Let's check that AIRPORT has TYPE attribute. */
    if(attributes.hasAttribute("TYPE")) {
        /* We'll add it to the string. */
        type = attributes.value("TYPE").toString();
    }
    /* Next element... */
    xml.readNext();
    /*
     * We're going to loop over the things because the order might change.
     * We'll continue the loop until we hit an EndElement named AIRPORT.
     */
    while(!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "AIRPORT"))
    {
        if(xml.tokenType() == QXmlStreamReader::StartElement)
        {
            if(xml.name() == "COUNTRY")
            {
                country = xml.readElementText();
            }
            else if(xml.name() == "NAME")
            {
                name = xml.readElementText();
            }
            else if(xml.name() == "ICAO")
            {
                icao = xml.readElementText();
            }
            else if(xml.name() == "GEOLOCATION")
            {
                xml.readNext();
                while(!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "GEOLOCATION"))
                {
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "LAT")
                        {
                            lat = xml.readElementText().toFloat();
                        }
                    }
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "LON")
                        {
                            lon = xml.readElementText().toFloat();
                        }
                    }
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "ELEV")
                        {
                            elevUnit = xml.attributes().value("UNIT").toString();
                            elev = xml.readElementText().toFloat();
                            if (elevUnit.compare("M", Qt::CaseInsensitive) == 0)
                            {
                                elev = elev * 3.28084;  // m -> ft
                            }
                        }
                    }
                    /* ...and next... */
                    xml.readNext();
                }
            }
            else if(xml.name() == "RADIO")
            {
                radioCat << xml.attributes().value("CATEGORY").toString();

                xml.readNext();
                while(!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "RADIO"))
                {
                    // not yet used
                    /*
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "ALT")
                        {
                            topUnit = xml.attributes().value("UNIT").toString();
                            top = xml.readElementText();
                        }
                   }
                   */
                    /* ...and next... */
                    xml.readNext();
                }
            }
            else if(xml.name() == "RWY")
            {
                runway temp;
                temp.status = xml.attributes().value("OPERATIONS").toString();
                xml.readNext();
                while(!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "RWY"))
                {
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "NAME")
                        {
                            temp.name = xml.readElementText();
                        }
                        if(xml.name() == "SFC")
                        {
                            temp.sfc = xml.readElementText();
                        }
                        if(xml.name() == "LENGTH")
                        {
                            QString lengthUnit = xml.attributes().value("UNIT").toString();
                            if (lengthUnit.compare("M", Qt::CaseInsensitive) == 0)  // at the moment only dimensions in m are allowde
                            {
                                temp.length = xml.readElementText();
                            }
                        }
                        if(xml.name() == "WIDTH")
                        {
                            QString widthUnit = xml.attributes().value("UNIT").toString();
                            if (widthUnit.compare("M", Qt::CaseInsensitive) == 0)  // at the moment only dimensions in m are allowde
                            {
                                temp.width = xml.readElementText();
                            }
                        }
                   }
                    /* ...and next... */
                    xml.readNext();
                }
                runways << temp;
            }
        }
        /* ...and next... */
        xml.readNext();
    }

    qlonglong lastInsertId;
    QString sQuery = "UPDATE airports SET icao='%1' ,type='%2', name='%3', lat_deg='%4', lon_deg='%5', elev_ft='%6', country='%7', continent='%8', municipality='%9', home_url='%10', wikipedia_url='%11', source=source|'%12' WHERE name='%3' AND country='%7';";
    sQuery = sQuery.arg(icao).arg(type).arg(name).arg(lat).arg(lon).arg(qRound(elev)).arg(country).arg(cntry_continent.value(country)).arg(municipality).arg(home_url).arg(wikipedia_url).arg(1);
    query.exec(sQuery);
    if (query.numRowsAffected() == 0)  // update returne no rows affected, try to insert
    {
        sQuery = "INSERT INTO airports (icao,type, name, lat_deg, lon_deg, elev_ft, country, continent, municipality, home_url, wikipedia_url, source) VALUES ('%1','%2','%3','%4','%5','%6','%7', '%8', '%9','%10','%11','%12')";
        sQuery = sQuery.arg(icao).arg(type).arg(name).arg(lat).arg(lon).arg(qRound(elev)).arg(country).arg(cntry_continent.value(country)).arg(municipality).arg(home_url).arg(wikipedia_url).arg(1);
        query.exec(sQuery);
        lastInsertId = query.lastInsertId().toLongLong();  // sqlite 'rowid'
    }
    else
    {
        sQuery = "SELECT rowid FROM airports name='%1' AND country='%2';";
        sQuery = sQuery.arg(name).arg(country);
        query.exec(sQuery);
        if (query.next())
        {
            lastInsertId = query.value(0).toLongLong();
        }
    }

    // delete all runways attribued to the airport, dont update becaus runway may change their name which is a identification field
    sQuery = "DELETE FROM runways WHERE airports_rowid = '%1';";
    sQuery = sQuery.arg(lastInsertId);
    query.exec(sQuery);

    for (int i = 0; i < runways.size(); i++)
    {
        sQuery = "INSERT INTO runways (airports_rowid, icao, name, sfc, status, length_m, width_m, lighted) VALUES ('%1','%2','%3','%4','%5','%6','%7', '%8');";
        sQuery = sQuery.arg(lastInsertId).arg(name).arg(runways.at(i).name).arg(runways.at(i).sfc).arg(runways.at(i).status).arg(runways.at(i).length).arg(runways.at(i).width).arg(runways.at(i).lighted);
        query.exec(sQuery);
    }

    emit recordProceeded(QString("Airport %1 (%2) processed").arg(name, icao));
}

void OpenAIPReader::parseNavaid(QXmlStreamReader &xml)
{

    QString ident;
    QString type;
    QString name;
    float lat;
    float lon;
    float elev;
    QString country;
    float freq;
    QString power;

    QString elevUnit;

    /* Let's check that we're really getting a NAVAID. */
    if(xml.tokenType() != QXmlStreamReader::StartElement &&
            xml.name() == "NAVAID") {
        return;
    }
    /* Let's get the attributes for NAVAID (the navaid type) */
    QXmlStreamAttributes attributes = xml.attributes();
    /* Let's check that NAVAID has TYPE attribute. */
    if(attributes.hasAttribute("TYPE")) {
        /* We'll add it to the string. */
        type = attributes.value("TYPE").toString();
    }
    /* Next element... */
    xml.readNext();
    /*
     * We're going to loop over the things because the order might change.
     * We'll continue the loop until we hit an EndElement named NAVAID.
     */
    while(!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "NAVAID"))
    {
        if(xml.tokenType() == QXmlStreamReader::StartElement)
        {
            if(xml.name() == "ID")
            {
                ident = xml.readElementText();
            }
            else if(xml.name() == "COUNTRY")
            {
                country = xml.readElementText();
            }
            else if(xml.name() == "NAME")
            {
                name = xml.readElementText();
            }
            else if(xml.name() == "GEOLOCATION")
            {
                xml.readNext();
                while(!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "GEOLOCATION"))
                {
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "LAT")
                        {
                            lat = xml.readElementText().toFloat();
                        }
                    }
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "LON")
                        {
                            lon = xml.readElementText().toFloat();
                        }
                    }
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "ELEV")
                        {
                            elevUnit = xml.attributes().value("UNIT").toString();
                            elev = xml.readElementText().toFloat();
                        }
                    }
                    /* ...and next... */
                    xml.readNext();
                }
            }
            else if(xml.name() == "RADIO")
            {
                xml.readNext();
                while(!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "RADIO"))
                {
                    if(xml.tokenType() == QXmlStreamReader::StartElement)
                    {
                        if(xml.name() == "FREQUENCY")
                        {
                            freq = xml.readElementText().toFloat();  // xml defines frequency in MHz
                        }
                   }
                    /* ...and next... */
                    xml.readNext();
                }
            }

        }
        /* ...and next... */
        xml.readNext();
    }

    if (elevUnit.compare("M") == 0)
    {
        elev = elev * 3.28084; // m -> ft

    }

    QString sQuery = "UPDATE navaids SET ident='%1',type='%2', name='%3', lat_deg='%4', lon_deg='%5', elev_ft='%6', country='%7', continent='%8', freq_mhz='%9', power='%10', source=source|'%11' WHERE ident='%1' AND country='%7';";
    sQuery = sQuery.arg(ident).arg(type).arg(name).arg(lat).arg(lon).arg(qRound(elev)).arg(country).arg(cntry_continent.value(country)).arg(freq).arg(power).arg(1);
    query.exec(sQuery);
    if (query.numRowsAffected() == 0)  // try to update
    {
        sQuery = "INSERT INTO navaids (ident, type, name, lat_deg, lon_deg, elev_ft, country, continent, freq_mhz, power, source) VALUES ('%1','%2','%3','%4','%5','%6','%7', '%8', '%9', '%10', '%11');";
        sQuery = sQuery.arg(ident).arg(type).arg(name).arg(lat).arg(lon).arg(qRound(elev)).arg(country).arg(cntry_continent.value(country)).arg(freq).arg(power).arg(1);
        query.exec(sQuery);
    }
    emit recordProceeded(QString("Navaid %1 processed").arg(ident));

}



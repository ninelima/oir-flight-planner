//
// Copyright 2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>

#include <QDebug>
#include <QSettings>


#include "defs.h"
#include "fivelncupdater.h"


FiveLNCupdater::FiveLNCupdater(QObject *parent) : QObject(parent),
        m_pManager(new QNetworkAccessManager(this))
{
    QSettings settings;
    m_url.setUrl(settings.value("update/fivelnc_url", "http://oirfp.local/5lnc.php").toString());
}


void FiveLNCupdater::getJSON()
{
//    m_isWorking = true;
    QNetworkRequest request;
    request.setUrl(m_url);
    QString ua = QString("%1 %2").arg(appName).arg(appVersion);
    request.setRawHeader("User-Agent", ua.toLocal8Bit());
    QNetworkReply *reply = m_pManager->get(request);
    connect(reply, SIGNAL(finished()), this, SLOT(parseJSON()));
}

void FiveLNCupdater::parseJSON()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(QObject::sender());
    //qDebug() << reply->readAll();


    if (reply->canReadLine())
    {
        QString firstLine = QString(reply->readLine()).trimmed();
        if (firstLine == "##V1")
        {
            m_query.exec("DELETE FROM icao5lnc WHERE 1"); // clean the table
            m_query.exec("BEGIN TRANSACTION");
            while (reply->canReadLine())
            {
                QStringList temp = QString(reply->readLine()).trimmed().split(";");
                qDebug() << temp;
//                QString sQuery = "INSERT INTO icao5lnc (ident, lat_deg, lon_deg, country, continent) VALUES ('%1', '%2', '%3', '%4', (SELECT continent FROM cntry_continent WHERE iso_country='%4'))";
                QString sQuery = "INSERT INTO icao5lnc (ident, lat_deg, lon_deg, country, continent) VALUES ('%1', '%2', '%3', '%4', '%5')";
                sQuery = sQuery.arg(temp.at(0)).arg(temp.at(1)).arg(temp.at(2)).arg(temp.at(4)).arg(temp.at(5));
                m_query.exec(sQuery);
            }
            m_query.exec("END TRANSACTION");
        }
        else
        {
            qDebug() << "version incorrect";
        }
    }

}

//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 47 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:02:47 +0000 (Wed, 07 Nov 2018) $
//


#include "oaipupdatewidget.h"
#include "ui_oaipupdatewidget.h"

#include <QTableView>
#include <QAbstractItemModel>

#include "databasehandler.h"
#include "openaipupdater.h"
#include "fivelncupdater.h"
#include "waitingspinnerwidget.h"

OaipUpdateWidget::OaipUpdateWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OaipUpdateWidget),
    m_pDatabaseHandler(NULL),
    m_pUpdater(new OpenAipUpdater()),
    m_p5LNCupdater(new FiveLNCupdater())
{
    ui->setupUi(this);
    m_pUpdater->moveToThread(&updateWorker);
    m_p5LNCupdater->moveToThread(&updateWorker);
    connect(&updateWorker, SIGNAL(finished()), m_pUpdater, SLOT(deleteLater()));
    connect(&updateWorker, SIGNAL(finished()), m_p5LNCupdater, SLOT(deleteLater()));

    m_pWaitingSpinner = new WaitingSpinnerWidget(ui->teStatus, true, false);

    connect(ui->pbUpdateDb, SIGNAL(clicked()), m_pUpdater, SLOT(getFiles()));
    connect(ui->pbCheckWeb, SIGNAL(clicked()), m_pUpdater, SLOT(getIndexFromWeb()));
    connect(ui->pbUpdateDb, SIGNAL(clicked()), m_pWaitingSpinner, SLOT(start()));
    connect(ui->pbCheckWeb, SIGNAL(clicked()), m_pWaitingSpinner, SLOT(start()));
    connect(ui->pbUpdate5LNC, SIGNAL(clicked()), m_p5LNCupdater, SLOT(getJSON()));

    connect(m_pUpdater, SIGNAL(textlineToAdd(QString)), ui->teStatus, SLOT(appendPlainText(QString)));
    connect(m_pUpdater, SIGNAL(repliesFinished()), m_pWaitingSpinner, SLOT(stop()));
    connect(m_pUpdater, SIGNAL(repliesFinished()), this, SLOT(workerTaskFinished()));
    connect(m_pUpdater, SIGNAL(getIndexFinished()), m_pWaitingSpinner, SLOT(stop()));
    connect(m_pUpdater, SIGNAL(getIndexFinished()), this, SLOT(workerTaskFinished()));
}

OaipUpdateWidget::~OaipUpdateWidget()
{
    updateWorker.quit();
    updateWorker.wait();
    delete ui;
}

void OaipUpdateWidget::init(DatabaseHandler *databaseHandler)
{
    m_pDatabaseHandler = databaseHandler;
    updateWorker.start();
}

bool OaipUpdateWidget::isWorking()
{
    return m_pUpdater->isWorking();
}

void OaipUpdateWidget::workerTaskFinished()
{
    if (!m_pUpdater->isWorking())
    {
        emit workerFinished();
    }

}


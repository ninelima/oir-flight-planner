//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include <QMessageBox>

#include "databasehandler.h"
#include "oaipconfigwidget.h"
#include "ui_oaipconfigwidget.h"

OaipConfigWidget::OaipConfigWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OaipConfigWidget),
    m_pDatabaseHandler(NULL)
{
    ui->setupUi(this);
    connect(ui->cbContinentFilter, SIGNAL(currentIndexChanged(int)), this, SLOT(setContinentFilter(int)));
    ui->tv->sortByColumn(0, Qt::AscendingOrder);
    ui->tv->setSortingEnabled(true);
}

OaipConfigWidget::~OaipConfigWidget()
{
    delete ui;
}

void OaipConfigWidget::setContinentFilter(int index)
{
    QString sFilter;
    switch (index)
    {
    case 1:
        sFilter = "AF";
        break;
    case 2:
        sFilter = "AN";
        break;
    case 3:
        sFilter = "AS";
        break;
    case 4:
        sFilter = "EU";
        break;
    case 5:
        sFilter = "NA";
        break;
    case 6:
        sFilter = "OC";
        break;
    case 7:
        sFilter = "SA";
        break;
    default:
        sFilter = "";
    }

    ui->tv->setFilter(2, sFilter);
}

void OaipConfigWidget::cleanDatabase()
{
     QMessageBox msgBox;
     msgBox.setText(tr("The airport, navaid and airspace data will be removed from the database!"));
     msgBox.setInformativeText("Do you want to proceed?");
     msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
     msgBox.setDefaultButton(QMessageBox::No);
     int ret = msgBox.exec();
     if (ret == QMessageBox::Yes)
     {
         m_pDatabaseHandler->cleanDatabase();
     }
}

void OaipConfigWidget::init(DatabaseHandler *databaseHandler)
{
    m_pDatabaseHandler = databaseHandler;
    connect(ui->pbCleanDB, SIGNAL(clicked()), this, SLOT(cleanDatabase()));
    connect(ui->pbResetUpdStatus, SIGNAL(clicked()), m_pDatabaseHandler, SLOT(resetUpdateStatus()));
}


//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef MARBLE_MAPWIDGETINPUTHANDLER_H
#define MARBLE_MAPWIDGETINPUTHANDLER_H

#include <QObject>
#include <QPoint>

class MapWidget;
class QKeyEvent;
class QMouseEvent;
class QWheelEvent;
class QTimer;

namespace Marble {
class RenderPlugin;

class MapWidgetInputHandler : public QObject
{
    Q_OBJECT

public:
    explicit MapWidgetInputHandler(MapWidget *widget);
    //virtual ~MapWidgetInputHandler();

    void setPositionSignalConnected( bool connected );
    bool isPositionSignalConnected() const;
    void installPluginEventFilter();

signals:
    // Mouse button menus
    void lmbRequest( int, int );
    void rmbRequest( int, int );

    //Gps coordinates
    void mouseClickScreenPosition( int, int );
    void mouseMoveGeoPosition( QString );


protected:
    bool eventFilter( QObject *, QEvent * );
    bool handleMouseEvent(QMouseEvent *event);

private slots:
    void installPluginEventFilter(RenderPlugin *renderPlugin);
    void openItemToolTip();
    void setCursor(const QCursor &cursor);

private:
    MapWidget *m_widget;
    // Indicates if the left mouse button has been pressed already.
    bool m_leftPressed;
    // Indicates whether the drag was started by a click above or below the visible pole.
    int m_leftPressedDirection;
    // Indicates if the middle mouse button has been pressed already.
    bool m_midPressed;
    // The mouse pointer x position when the left mouse button has been pressed.
    int m_leftPressedX;
    // The mouse pointer y position when the left mouse button has been pressed.
    int m_leftPressedY;
    // The mouse pointer y position when the middle mouse button has been pressed.
    int m_midPressedY;
    int m_startingRadius;
    // The center longitude in radian when the left mouse button has been pressed.
    qreal m_leftPressedLon;
    // The center latitude in radian when the left mouse button has been pressed.
    qreal m_leftPressedLat;

    int m_dragThreshold;
    qreal m_wheelZoomTargetDistance;

    QTimer *m_mouseWheelTimer;
    bool m_positionSignalConnected;


private:
    bool handleKeyPress(QKeyEvent *e);
    bool handleWheel(QWheelEvent *e);

    virtual void handleMouseButtonPress(QMouseEvent *e);
    virtual void handleLeftMouseButtonPress(QMouseEvent *e);
    virtual void handleRightMouseButtonPress(QMouseEvent *e);
    virtual void handleMiddleMouseButtonPress(QMouseEvent *e);
    virtual void handleMouseButtonRelease(QMouseEvent *e);

    void notifyPosition(bool isAboveMap, qreal mouseLon, qreal mouseLat);

    Q_DISABLE_COPY(MapWidgetInputHandler)

};

}


#endif

# source from http://sourceforge.net/projects/sgml-for-qt/
# cvs revision 32

# build instructions:
# qmake
# make

QMAKE_CXXFLAGS += -std=gnu++11
QT -= gui
TARGET = html
TEMPLATE = lib
INCLUDEPATH += ../../include
DESTDIR = ../../bin
CONFIG += staticlib
SOURCES += QSgmlTag.cpp \
	   QSgml.cpp
HEADERS += ../../include/QSgmlTag.h \
	   ../../include/QSgml.h

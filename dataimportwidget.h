//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef DATAIMPORTWIDGET_H
#define DATAIMPORTWIDGET_H

#include <QWidget>
#include <QThread>
#include <QStringList>

namespace Ui {
class DataImportWidget;
}

class OpenAIPReader;
class OurairportsReader;

class DataImportWidget : public QWidget
{
    Q_OBJECT
    QThread aipreadWorker;
    QThread ourreadWorker;
public:
    explicit DataImportWidget(QWidget *parent = 0);
    ~DataImportWidget();
    bool isWorking() { return m_isWorking; }

protected:
    void endisControls(bool enable);
private slots:
    void readAirspaceFile();
    void readAirportFile();
    void readNavaidsFile();
    void processingFinished();


private:
    Ui::DataImportWidget *ui;
    OpenAIPReader *aipreader;
    OurairportsReader *oareader;
    bool m_isWorking;

signals:
    void rtrAirspaceFile(QStringList);
    void rtrAirportFileOur(QStringList);
    void rtrAirportFileAIP(QStringList);
    void rtrNavaidsFileOur(QStringList);
    void rtrNavaidsFileAIP(QStringList);
    void workerFinished();
};

#endif // DATAIMPORTWIDGET_H

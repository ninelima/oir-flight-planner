//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "mapwidgetinputhandler.h"

#include <QToolTip>
#include <QTimer>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QWheelEvent>

#include "MarbleGlobal.h"
#include "MarbleDebug.h"
#include "MarbleMap.h"
#include "mapwidget.h"
#include "AbstractDataPluginItem.h"
#include "AbstractFloatItem.h"
#include "mapwidgetpopupmenu.h"
#include "PopupLayer.h"
#include "RenderPlugin.h"

namespace Marble
{



void MapWidgetInputHandler::setCursor(const QCursor &cursor)
{
    m_widget->setCursor(cursor);
}

bool MapWidgetInputHandler::handleKeyPress(QKeyEvent *e)
{
    Q_UNUSED(e)
    return false;
}

MapWidgetInputHandler::MapWidgetInputHandler(MapWidget *widget) :
    QObject(widget),
     m_leftPressed(false),
      m_midPressed(false),
      m_dragThreshold(MarbleGlobal::getInstance()->profiles() & MarbleGlobal::SmallScreen ? 15 : 3),
      m_mouseWheelTimer(0),
      m_positionSignalConnected(false)
{
    m_widget = widget;
    //m_mouseWheelTimer = new QTimer( this );
    //connect(m_mouseWheelTimer, SIGNAL(timeout()), this, SLOT(restoreViewContext()));

}

void MapWidgetInputHandler::installPluginEventFilter(Marble::RenderPlugin *renderPlugin)
{
    m_widget->installEventFilter(renderPlugin);
}

void MapWidgetInputHandler::openItemToolTip()
{
    /*
    if (!lastToolTipItem().isNull())
    {
        QToolTip::showText(d->m_marbleWidget->mapToGlobal(toolTipPosition()),
                            lastToolTipItem()->toolTip(),
                            d->m_marbleWidget,
                            lastToolTipItem()->containsRect(toolTipPosition()).toRect());
    }
    */
}

bool MapWidgetInputHandler::eventFilter(QObject* o, QEvent* e)
{
    Q_UNUSED(o);
    switch (e->type())
    {
    case QEvent::KeyPress:
        return handleKeyPress(static_cast<QKeyEvent *>(e));
    case QEvent::Wheel:
        return handleWheel(static_cast<QWheelEvent*>(e));
    case QEvent::MouseButtonDblClick:
//        return handleDoubleClick(static_cast<QMouseEvent*>(e));
    case QEvent::MouseButtonPress:
    case QEvent::MouseButtonRelease:
    case QEvent::MouseMove:
        return handleMouseEvent(static_cast<QMouseEvent*>(e));
    default:
        return false;
    }
}

bool MapWidgetInputHandler::handleMouseEvent(QMouseEvent *event)
{
    QPoint direction;

    //checkReleasedMove(event);

    // Do not handle (and therefore eat) mouse press and release events
    // that occur above visible float items. Mouse motion events are still
    // handled, however.
    if (event->type() != QEvent::MouseMove)
    {

        foreach (AbstractFloatItem *floatItem, m_widget->map()->floatItems())
        {
            if ( floatItem->enabled() && floatItem->visible()
                 && floatItem->contains( event->pos() ) )
            {
                //d->m_lmbTimer.stop();
                return false;
            }
        }

    }

    qreal mouseLon;
    qreal mouseLat;
    const bool isMouseAboveMap = m_widget->map()->geoCoordinates(event->x(), event->y(),
                                             mouseLon, mouseLat, GeoDataCoordinates::Radian);
    notifyPosition(isMouseAboveMap, mouseLon, mouseLat);
    QPoint mousePosition(event->x(), event->y());
    if (isMouseAboveMap || m_widget->map()->whichFeatureAt( mousePosition ).size() != 0)
    {
        if (event->type() == QEvent::MouseButtonPress)
        {
            handleMouseButtonPress(event);
        }

        if (event->type() == QEvent::MouseButtonRelease)
        {
            handleMouseButtonRelease(event);
        }

        // Regarding all kinds of mouse moves:

        if (m_leftPressed)
        {
            qreal radius = (qreal)(m_widget->map()->radius());
            int deltax = event->x() - m_leftPressedX;
            int deltay = event->y() - m_leftPressedY;

            if (abs(deltax) > m_dragThreshold
                 || abs(deltay) > m_dragThreshold
                 /*|| !d->m_lmbTimer.isActive()*/)
            {
                //d->m_lmbTimer.stop();

                const qreal posLon = m_leftPressedLon - 90.0 * m_leftPressedDirection * deltax / radius;
                const qreal posLat = m_leftPressedLat + 90.0 * deltay / radius;
                m_widget->map()->centerOn(posLon, posLat);
                m_widget->update();
            }
        }
/*
        if (d->m_midPressed)
        {
            int eventy = event->y();
            int dy = d->m_midPressedY - eventy;
            MarbleInputHandler::d->m_marblePresenter->setRadius(d->m_startingRadius * pow(1.005, dy));
        }
*/

    }
    else
    {
        //direction = mouseMovedOutside(event);
    }

    return false;
}

bool MapWidgetInputHandler::handleWheel(QWheelEvent *wheelevt)
{
    int steps = wheelevt->delta() / 3;
    qreal zoom = m_widget->zoom();

    qreal newDistance = m_widget->distanceFromZoom(zoom + steps);
    m_widget->zoomAt(wheelevt->pos(), newDistance);

    return true;
}


void MapWidgetInputHandler::handleMouseButtonPress(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton )
    {
       handleLeftMouseButtonPress(event);
    }

    if ( event->button() == Qt::MidButton )
    {
       handleMiddleMouseButtonPress(event);
    }

    if ( event->button() == Qt::RightButton )
    {
       handleRightMouseButtonPress(event);
    }
}

void MapWidgetInputHandler::handleLeftMouseButtonPress(QMouseEvent *event)
{


    m_leftPressed = true;
    m_midPressed = false;

    // On the single event of a mouse button press these
    // values get stored, to enable us to e.g. calculate the
    // distance of a mouse drag while the mouse button is
    // still down.
    m_leftPressedX = event->x();
    m_leftPressedY = event->y();

    // Calculate translation of center point
    m_leftPressedLon = m_widget->map()->centerLongitude();
    m_leftPressedLat = m_widget->map()->centerLatitude();

    m_leftPressedDirection = 1;

    // Choose spin direction by taking into account whether we
    // drag above or below the visible pole.
    if (m_widget->map()->projection() == Spherical)
    {
        if (m_leftPressedLat >= 0)
        {   // The visible pole is the north pole
            qreal northPoleX, northPoleY;
            m_widget->map()->screenCoordinates(0.0, 90.0, northPoleX, northPoleY);
            if (event->y() < northPoleY)
            {
                m_leftPressedDirection = -1;
            }
        }
        else
        {   // The visible pole is the south pole
            qreal southPoleX, southPoleY;
            m_widget->map()->screenCoordinates(0.0, -90.0, southPoleX, southPoleY);
            if (event->y() > southPoleY)
            {
                m_leftPressedDirection = -1;
            }
        }
    }

    m_widget->map()->setViewContext(Animation);

}

void MapWidgetInputHandler::handleMiddleMouseButtonPress(QMouseEvent *event)
{
    m_midPressed = true;
    m_leftPressed = false;
    m_startingRadius = m_widget->map()->radius();
    m_midPressedY = event->y();


    m_widget->map()->setViewContext(Animation);
}

void MapWidgetInputHandler::handleRightMouseButtonPress(QMouseEvent *event)
{
    emit rmbRequest(event->x(), event->y());
}

void MapWidgetInputHandler::handleMouseButtonRelease(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        //emit current coordinates to be be interpreted
        //as requested
        emit mouseClickScreenPosition(m_leftPressedX, m_leftPressedY);

        m_leftPressed = false;
        m_widget->map()->setViewContext(Still);
    }

    if (event->button() == Qt::MidButton)
    {
        m_midPressed = false;

        m_widget->map()->setViewContext(Still);
    }

    if (event->type() == QEvent::MouseButtonRelease && event->button() == Qt::RightButton)
    {
    }

}


void MapWidgetInputHandler::notifyPosition(bool isMouseAboveMap, qreal mouseLon, qreal mouseLat)
{
    // emit the position string only if the signal got attached
    if (m_positionSignalConnected) {
        if (!isMouseAboveMap)
        {
            emit mouseMoveGeoPosition(tr(NOT_AVAILABLE));
        }
        else
        {
            QString position = GeoDataCoordinates(mouseLon, mouseLat).toString();
            emit mouseMoveGeoPosition(position);
        }
    }
}

void MapWidgetInputHandler::setPositionSignalConnected(bool connected)
{
    m_positionSignalConnected = connected;
}

bool MapWidgetInputHandler::isPositionSignalConnected() const
{
    return m_positionSignalConnected;
}

void MapWidgetInputHandler::installPluginEventFilter()
{
    foreach(Marble::RenderPlugin *renderPlugin, m_widget->renderPlugins())
    {
        if(renderPlugin->isInitialized())
        {
            qDebug() << renderPlugin->nameId();
            installPluginEventFilter(renderPlugin);
        }
    }
}



}

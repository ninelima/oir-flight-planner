//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "dbmaintwidget.h"
#include "ui_dbmaintwidget.h"

#include <QTableView>
#include <QAbstractItemModel>
#include <QMessageBox>
#include <QCloseEvent>

#include "databasehandler.h"


DbMaintWidget::DbMaintWidget(DatabaseHandler *databaseHandler, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DbMaintWidget),
    m_pDatabaseHandler(databaseHandler)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window);

    ui->onlineWidget->init(m_pDatabaseHandler);
    ui->configWidget->init(m_pDatabaseHandler);
    connect(ui->pbClose, SIGNAL(clicked()), this, SLOT(close()));

    connect(ui->onlineWidget, SIGNAL(workerFinished()), this, SLOT(workerFinished()));
    connect(ui->offlineWidget, SIGNAL(workerFinished()), this, SLOT(workerFinished()));
}

DbMaintWidget::~DbMaintWidget()
{
    delete ui;
}

bool DbMaintWidget::isWorkerFinished()
{
    if (ui->onlineWidget->isWorking() || ui->offlineWidget->isWorking()) //
    {
        return false;
    }
    return true;
}

void DbMaintWidget::closeEvent(QCloseEvent *event)
{
    if (isWorkerFinished())
    {
        if (m_databaseChanged)
        {
            QMessageBox msgBox;
            msgBox.setText(tr("Database content has changed!  You may need to restart the application for the changes to take effect!"));
            msgBox.setWindowIcon(QIcon("://oirfp32x32.png"));
            msgBox.exec();
            m_databaseChanged = false;
        }
    }
    event->accept();
}

void DbMaintWidget::workerFinished()
{
    m_databaseChanged = true;
    this->show();
}


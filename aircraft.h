//
// Copyright 2016 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef AIRCRAFT_H
#define AIRCRAFT_H

#include <QObject>
#include <QStringList>

class Aircraft : public QObject
{
    Q_OBJECT
public:
    explicit Aircraft(QObject *parent = 0);

public:
    static const QStringList speedUnits(){ return  (QStringList() << "kts" << "mph" << "km/h"); }
    static const QStringList rateUnits(){ return  (QStringList() << "ft/min" << "m/s" << "m/min"); }
public:

    inline int getCruiseSpeed() const { return cruiseSpeed; }
    inline void setCruiseSpeed(int value) { cruiseSpeed = value; }

    inline QString getImmatriculation() const { return immatriculation; }
    inline void setImmatriculation(const QString &value) { immatriculation = value; }

    inline QString getType() const { return type; }
    inline void setType(const QString &value) { type = value; }

    inline int getClimbSpeed() const { return climbSpeed; }
    inline void setClimbSpeed(int value) { climbSpeed = value; }

    inline int getClimbRate() const { return climbRate; }
    inline void setClimbRate(int value) { climbRate = value; }

    inline int getDescentSpeed() const { return descentSpeed; }
    inline void setDescentSpeed(int value) { descentSpeed = value; }

    inline int getDescentRate() const { return descentRate; }
    inline void setDescentRate(int value) { descentRate = value; }

    inline int getSpeedUnit() const { return speedUnit; }
    inline void setSpeedUnit(int value) { speedUnit = value; }

    inline int getRateUnit() const { return rateUnit; }
    inline void setRateUnit(int value) { rateUnit = value; }

    inline qreal getFuelFlow() const { return fuelFlow; }
    inline void setFuelFlow(const qreal &value) { fuelFlow = value; }

    inline int getFuelUsable() const { return fuelUsable; }
    inline void setFuelUsable(int value) { fuelUsable = value; }

private:
    QString immatriculation;
    QString type;
    int cruiseSpeed;
    int climbSpeed;
    int climbRate;
    int descentSpeed;
    int descentRate;
    int speedUnit;
    int rateUnit;
    qreal fuelFlow;
    int fuelUsable;

signals:

public slots:
};

class AircraftList : public QList<Aircraft*>
{

public:
    AircraftList();
    static AircraftList *instance();

};

#endif // AIRCRAFT_H

//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include <QApplication>
#include <QMessageBox>
#include <QDebug>

#include "fpsplashscreen.h"
#include "defs.h"
#include "mainwindow.h"

int main(int argc, char** argv)
{
    QApplication app(argc,argv);
    QApplication::setOrganizationName(orgName);
    QApplication::setApplicationName(appName);
    QApplication::setApplicationVersion(appVersion);
    QPixmap pixmap(":/splash.jpg");
    FpSplashScreen splash(pixmap, Qt::WindowStaysOnTopHint);
    splash.show();
    splash.repaint();
    app.processEvents();

    // MarbleMap writes its cached data to c:\Users\[user]\AppData\Local\.marble
    // The database must be writable and accessible by every user

    MainWindow mainwindow;

    if (!mainwindow.initOk())
    {
        splash.finish(&mainwindow);
        QMessageBox msgBox;
        msgBox.setText(mainwindow.getErrorMsg());
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
        return -1;
    }
    mainwindow.show();
    splash.finish(&mainwindow);
    return app.exec();
}



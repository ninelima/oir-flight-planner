//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef SQL_QUERIES_H
#define SQL_QUERIES_H

#define RESET_UPD_STATUS "UPDATE oaipfiles SET dateInst ='' WHERE 1;"

#define DROP_AIRPORTS "DROP TABLE airports;"
#define CREATE_AIRPORTS "CREATE TABLE `airports` ( \
                            `icao`	TEXT, \
                            `type`	TEXT, \
                            `name`	TEXT, \
                            `lat_deg`	REAL, \
                            `lon_deg`	REAL, \
                            `elev_ft`	REAL, \
                            `country`	TEXT, \
                            `continent`	TEXT, \
                            `municipality`	TEXT, \
                            `home_url`	TEXT, \
                            `wikipedia_url`	TEXT, \
                            `source`	INTEGER \
                            );"
#define UPD_FILES_AFT_DROP_APT "UPDATE oaipfiles SET dateInst ='' WHERE type='wpt';"

#define DROP_AIRSPACES "DROP TABLE airspaces;"
#define CREATE_AIRSPACES "CREATE TABLE `airspaces` ( \
                            `id`	INTEGER, \
                            `country`	TEXT, \
                            `name`	TEXT, \
                            `cat`	TEXT, \
                            `top`	INTEGER, \
                            `topref`	TEXT, \
                            `topunit`	TEXT, \
                            `bottom`	INTEGER, \
                            `bottomref`	TEXT, \
                            `bottomunit`	TEXT, \
                            `polygon`	TEXT, \
                            `continent`	TEXT \
                            );"
#define UPD_FILES_AFT_DROP_ASP "UPDATE oaipfiles SET dateInst ='' WHERE type='asp';"


#define DROP_NAVAIDS "DROP TABLE navaids;"
#define CREATE_NAVAIDS "CREATE TABLE `navaids` ( \
                        `ident`	TEXT, \
                        `type`	TEXT, \
                        `name`	TEXT, \
                        `lat_deg`	REAL, \
                        `lon_deg`	REAL, \
                        `elev_ft`	REAL, \
                        `country`	TEXT, \
                        `continent`	TEXT, \
                        `freq_mhz`	REAL, \
                        `power`	TEXT, \
                        `source`	INTEGER \
                        );"
#define UPD_FILES_AFT_DROP_NAV "UPDATE oaipfiles SET dateInst ='' WHERE type='nav';"


#define DROP_RUNWAYS "DROP TABLE runways;"
#define CREATE_RUNWAYS "CREATE TABLE `runways` ( \
                        `airports_rowid`	INTEGER, \
                        `icao`	TEXT, \
                        `name`	TEXT, \
                        `sfc`	TEXT, \
                        `status`	TEXT, \
                        `length_m`	INTEGER, \
                        `width_m`	INTEGER, \
                        `lighted`	TEXT \
                        );"

#define DROP_OAIPFILES "DROP TABLE oaipfiles;"
#define CREATE_OAIPFILES "CREATE TABLE `oaipfiles` ( \
                            `country`	TEXT, \
                            `type`	TEXT, \
                            `version`	INTEGER, \
                            `installed`	INTEGER, \
                            `dateInst`	TEXT, \
                            `dateAvail`	TEXT \
                            );"

#define SEL_CTRY_CONFIG "SELECT ct.country, country_name, continent, wpt, nav, asp FROM \
                            (SELECT oaipfiles.country, cntry_continent.country_name, cntry_continent.continent FROM oaipfiles, cntry_continent WHERE \
                                oaipfiles.country = lower(cntry_continent.iso_country) GROUP BY country ) as ct LEFT JOIN \
                            (SELECT country, type, installed as wpt FROM oaipfiles WHERE type='wpt') as wt ON ct.country = wt.country LEFT OUTER JOIN \
                            (SELECT country, type, installed as nav FROM oaipfiles WHERE type='nav') as nt ON ct.country = nt.country LEFT OUTER JOIN \
                            (SELECT country, type, installed as asp FROM oaipfiles WHERE type='asp') as at ON ct.country = at.country;"


#endif // SQL_QUERIES_H

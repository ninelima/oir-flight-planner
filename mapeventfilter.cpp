//
//
// This program is free software licensed under the GNU LGPL. You can
// find a copy of this license in LICENSE.txt in the top directory of
// the source code.
//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>

//

#include "mapeventfilter.h"

#include <MarbleWidget.h>
#include <MarbleWidgetInputHandler.h>


#include <QEvent>
#include <QKeyEvent>
#include <QDebug>


MapEventFilter::MapEventFilter(QObject *parent):
    QObject(parent)
{

}


bool MapEventFilter::eventFilter(QObject *o, QEvent *e)
{
/*
    if (e->type() == QEvent::MouseButtonPress)
    {
        qDebug() << "MapEventFilter: MouseButtonPress";
    }
*/
    Marble::MarbleWidget *m = dynamic_cast<Marble::MarbleWidget*>(o);
    if (m == NULL) return false;
    Marble::MarbleWidgetInputHandler *inputHandler = m->inputHandler();

    if (e->type() == QEvent::MouseMove)
    {
        QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(e);
        if (mouseEvent->modifiers() == Qt::ShiftModifier)
        {
            e->accept();
            return false;
        }
        else
        {
            emit mouseMove(mouseEvent->pos());
            return true;
        }
    }
    /*
    else if (e->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(e);
        if (mouseEvent->modifiers() == Qt::NoModifier)
        {
            if (mouseEvent->button() == Qt::LeftButton)
            {
                emit mouseLeftButtonPress(mouseEvent->pos());
                return true;
            }
            else
            {
                e->accept();
                return false;
            }
        }
    }
    */

    return false;



}



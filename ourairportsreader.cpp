//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "ourairportsreader.h"

#include <QFile>
#include <QFileInfo>
#include <QStringList>
#include <QSqlQuery>
#include <QVariant>
#include <QTextStream>

OurairportsReader::OurairportsReader(QObject *parent) :
    QObject(parent)
{

    // fill hash table with country/continent codes
    QString sQuery;
    // use http://dev.maxmind.com/geoip/legacy/codes/country_continent/ to fill the database table
    sQuery = "SELECT iso_country, continent FROM cntry_continent;";
    query.setForwardOnly(true);
    query.exec(sQuery);
    while (query.next())
    {
        cntry_continent.insert(query.value(0).toString(), query.value(1).toString());
    }
}


#if 0

defines:

    m -> ft : 3.28084
    1: OpenAIP
    2: ourairports

    enum AirportType



#endif

void OurairportsReader::readAirspaceFile(QStringList fileNames)
{
    Q_UNUSED(fileNames)
    // do nothing, ourairports does not provide airspaces
    emit fileFinished();
}

void OurairportsReader::readAirportFile(QStringList fileNames)
{
    QString id;
    QString icao;
    QString type;
    QString name;
    //float lat;
    //float lon;
    //int elev;
    QString country;
    QString municipality;
    QString home_url;
    QString wikipedia_url;


    for (int i = 0; i < fileNames.size(); i++)
    {
        query.exec("BEGIN TRANSACTION");

        QFile file(fileNames.at(i));
        if (!file.open(QIODevice::ReadOnly)) {
    //     out << "  Couldn't open the file." << endl << endl << endl;
         continue;
        }

        const QRegExp regExp("^\"|\"$");
        QTextStream in(&file);
        while (!in.atEnd()) {
            QString line = in.readLine();
            QStringList airport = line.split(";");
            for (int i = 0; i < airport.size(); i++)
            {
                airport[i].replace(regExp, "");
            }
            if (airport.at(0).compare("id") == 0)
            {
                continue;
            }

            //  id;ident;type;name;latitude_deg;longitude_deg;elevation_ft;continent;iso_country;iso_region;
            //  municipality;scheduled_service;gps_code;iata_code;local_code;home_link;wikipedia_link;keywords

            id = airport.at(0);
            icao = airport.at(1);
            type = airport.at(2);
            name = airport.at(3);
            //lat = airport.at(4).toFloat();
            //lon = airport.at(5).toFloat();
            //elev = airport.at(6).toInt();
            country = airport.at(8);
            municipality = airport.at(10);
            home_url = airport.at(15);
            wikipedia_url = airport.at(16);

//            qlonglong lastInsertId;

//            QString sQuery = "UPDATE airports SET icao='%1' ,type='%2', name='%3', lat_deg='%4', lon_deg='%5', elev_ft='%6', country='%7', continent='%8', municipality='%9', home_url='%10', wikipedia_url='%11', source=source+'%12' WHERE name='%3' AND country='%7';";
//            sQuery = sQuery.arg(icao).arg(type).arg(name).arg(lat).arg(lon).arg(elev).arg(country).arg(cntry_continent.value(country)).arg(municipality).arg(home_url).arg(wikipedia_url).arg(2);

            // update only urls etc.
            QString sQuery = "UPDATE airports SET municipality='%1', home_url='%2', wikipedia_url='%3', source=source|'%4' WHERE icao='%5'";
            sQuery = sQuery.arg(municipality).arg(home_url).arg(wikipedia_url).arg(2).arg(icao);
            query.exec(sQuery);
            /*
            if (query.numRowsAffected() == 0)  // update returne no rows affected, try to insert
            {
                sQuery = "INSERT INTO airports (icao,type, name, lat_deg, lon_deg, elev_ft, country, continent, municipality, home_url, wikipedia_url, source) VALUES ('%1','%2','%3','%4','%5','%6','%7', '%8', '%9','%10','%11','%12')";
                sQuery = sQuery.arg(icao).arg(type).arg(name).arg(lat).arg(lon).arg(elev).arg(country).arg(cntry_continent.value(country)).arg(municipality).arg(home_url).arg(wikipedia_url).arg(2);
                query.exec(sQuery);
                lastInsertId = query.lastInsertId().toLongLong();  // sqlite 'rowid'
            }
            else
            {
                sQuery = "SELECT rowid FROM airports name='%1' AND country='%2';";
                sQuery = sQuery.arg(name).arg(country);
                query.exec(sQuery);
                if (query.next())
                {
                    lastInsertId = query.value(0).toLongLong();
                }
            }

            // delete all runways attribued to the airport, dont update becaus runway may change their name which is a identification field
            sQuery = "DELETE FROM runways WHERE airports_rowid = '%1';";
            sQuery = sQuery.arg(lastInsertId);
            query.exec(sQuery);

            QList<QStringList> runwayData = runways.values(id);
            for (int i = 0; i < runwayData.size(); i++)
            {            {
                sQuery = "INSERT INTO runways (airports_rowid, icao, name, sfc, status, length_m, width_m, lighted) VALUES ('%1','%2','%3','%4','%5','%6','%7', '%8');";
                sQuery = sQuery.arg(lastInsertId).arg(name).arg(runways.at(i).name).arg(runways.at(i).sfc).arg(runways.at(i).status).arg(runways.at(i).length).arg(runways.at(i).width).arg(runways.at(i).lighted);
                query.exec(sQuery);
            }
            */

            emit recordProceeded(QString("Airport %1 (%2) processed").arg(name, icao));

        }
        query.exec("END TRANSACTION");
        file.close();
    }

    emit fileFinished();
}

/*
void OurairportsReader::loadRunways(QMultiHash<QString, QStringList> &table)
{
    QFile file("runways.csv");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    const QRegExp regExp("^\"|\"$");
    QTextStream in(&file);
    while (!in.atEnd()) {
        QString line = in.readLine();
        QStringList runway = line.split(",");
        for (int i = 0; i < runway.size(); i++)
        {
            runway[i].replace(regExp, "");
        }

        if (runway.at(0).compare("id") == 0)
        {
            continue;
        }
        QString key = runway.at(2);
        QStringList data;
        // Data: runway id 1 / id 2, length, width, surface, closed
        QString length = QString::number((int)(runway.at(3).toFloat() * 0.3048));
        QString width = QString::number((int)(runway.at(4).toFloat() * 0.3048));
        data << runway.at(8) << runway.at(14) << length << width << runway.at(5) << runway.at(7);
        table.insert(key, data);
    }
    file.close();
}
*/

void OurairportsReader::readNavaidsFile(QStringList fileNames)
{
    QString ident;
    QString type;
    QString name;
    float lat;
    float lon;
    float elev;
    QString country;
    float freq;
    QString power;


    for (int i = 0; i < fileNames.size(); i++)
    {
        query.exec("BEGIN TRANSACTION");

        QFile file(fileNames.at(i));
        if (!file.open(QIODevice::ReadOnly)) {
    //     out << "  Couldn't open the file." << endl << endl << endl;
         continue;
        }

        const QRegExp regExp("^\"|\"$");
        QTextStream in(&file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList navaid = line.split(",");
            for (int i = 0; i < navaid.size(); i++)
            {
                navaid[i].replace(regExp, ""); // remove text delimiter "
            }

            if (navaid.at(0).compare("id") == 0)
            {
                continue;
            }
            // fields in CSV
            // "id","filename","ident","name","type","frequency_khz","latitude_deg","longitude_deg","elevation_ft","iso_country","dme_frequency_khz",
            // "dme_channel","dme_latitude_deg","dme_longitude_deg","dme_elevation_ft","slaved_variation_deg","magnetic_variation_deg","usageType",
            //"power","associated_airport",
            ident = navaid.at(2);
            type = navaid.at(4);
            name = navaid.at(3);
            lat = navaid.at(6).toFloat();
            lon = navaid.at(7).toFloat();
            elev = navaid.at(8).toFloat();
            country = navaid.at(9);
            freq = navaid.at(5).toFloat() / 1000;
            power = navaid.at(18);


            QString sQuery = "UPDATE navaids SET ident='%1',type='%2', name='%3', lat_deg='%4', lon_deg='%5', elev_ft='%6', country='%7', continent='%8', freq_mhz='%9', power='%10', source=source|'%11' WHERE ident='%1' AND country='%7';";
            sQuery = sQuery.arg(ident).arg(type).arg(name).arg(lat).arg(lon).arg(elev).arg(country).arg(cntry_continent.value(country)).arg(freq).arg(power).arg(2);
            query.exec(sQuery);
            if (query.numRowsAffected() == 0)  // try to update
            {
                sQuery = "INSERT INTO navaids (ident,type, name, lat_deg, lon_deg, elev_ft, country, continent, freq_mhz, power, source) VALUES ('%1','%2','%3','%4','%5','%6','%7', '%8', '%9', '%10', '%11');";
                sQuery = sQuery.arg(ident).arg(type).arg(name).arg(lat).arg(lon).arg(elev).arg(country).arg(cntry_continent.value(country)).arg(freq).arg(power).arg(2);
                query.exec(sQuery);
            }
            emit recordProceeded(QString("Navaid %1 processed").arg(ident));
        }
        file.close();
        query.exec("END TRANSACTION");

    }

    emit fileFinished();
}

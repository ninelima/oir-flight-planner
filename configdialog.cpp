//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "configdialog.h"
#include "ui_configdialog.h"

#include <QSettings>

#include <MarblePluginSettingsWidget.h>
#include <MarbleLocale.h>
#include "mapwidget.h"
#include "speedunitdelegate.h"
#include "rateunitdelegate.h"
#include "aircraftconfigmodel.h"


ConfigDialog::ConfigDialog(MapWidget *mapWidget, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigDialog),
    m_pMapWidget(mapWidget),
    m_pSettings(new QSettings())
{
    ui->setupUi(this);

    connect( ui->buttons, SIGNAL(accepted()), this, SLOT(accept()) ); // Ok
    connect( ui->buttons, SIGNAL(rejected()), this, SLOT(close()) ); // Cancel: close and delete_later()
//    connect( ui->buttons, SIGNAL(rejected()), this, SLOT(reject()) ); // Cancel
    connect( ui->buttons->button( QDialogButtonBox::Apply ),SIGNAL(clicked()),
             this, SLOT(writeSettings()) );                         // Apply
    // If the dialog is accepted. Save the settings.
    connect( this, SIGNAL(accepted()), this, SLOT(writeSettings()));



    m_pluginModel.setRenderPlugins( mapWidget->renderPlugins() );
    m_pluginConfig = new MarblePluginSettingsWidget(this);
    m_pluginConfig->setModel(&m_pluginModel);
    ui->tabWidget->addTab(m_pluginConfig, tr("Plugins"));

    connect( this, SIGNAL(rejected()), &m_pluginModel, SLOT(retrievePluginState()) );
    connect( this, SIGNAL(accepted()), &m_pluginModel, SLOT(applyPluginState()) );

    setupTabWebview();
    setupTabAircraft();
    setupTabContinents();
    setupTabGeneral();
}

ConfigDialog::~ConfigDialog()
{
    delete m_pSettings;
    delete m_pluginConfig;
    delete ui;
}

bool ConfigDialog::close()
{
    this->deleteLater();
    return QWidget::close();
}

void ConfigDialog::setupTabWebview()
{
    m_pSettings->beginGroup("Webview");
    QTableWidgetItem *newItem;
    int size = m_pSettings->beginReadArray("Urls");
    ui->twURLs->setRowCount(size);
    for (int i = 0; i < size; ++i) {
         m_pSettings->setArrayIndex(i);
         newItem = new QTableWidgetItem(m_pSettings->value("description").toString());
         ui->twURLs->setItem(i,0, newItem);
         newItem = new QTableWidgetItem(m_pSettings->value("url").toString());
         ui->twURLs->setItem(i,1, newItem);
    }
    m_pSettings->endArray();
    m_pSettings->endGroup();
    ui->twURLs->resizeColumnsToContents();
    ui->twURLs->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->twURLs->setSelectionBehavior(QAbstractItemView::SelectRows);
    connect(ui->pbAddWVItem, SIGNAL(clicked()), this, SLOT(addWebViewItem()));
    connect(ui->pbRemoveWVItem, SIGNAL(clicked()), this, SLOT(removeWebViewItem()));


}

void ConfigDialog::setupTabContinents()
{
    QStringList activeContinents = m_pSettings->value("view/activeContinents", QStringList("EU")).toStringList();

    for (int i = 0; i < ui->twContinents->rowCount(); i++)
    {
        if (activeContinents.contains(ui->twContinents->item(i,0)->text()))
        {
            ui->twContinents->item(i,0)->setCheckState(Qt::Checked);
        }
        else
        {
            ui->twContinents->item(i,0)->setCheckState(Qt::Unchecked);
        }

    }

}

void ConfigDialog::setupTabAircraft()
{
    SpeedUnitDelegate *speedDelegate = new SpeedUnitDelegate;
    ui->tvAircraft->setItemDelegateForColumn(7, speedDelegate);

    RateUnitDelegate *rateDelegate = new RateUnitDelegate;
    ui->tvAircraft->setItemDelegateForColumn(8, rateDelegate);


    connect(ui->pbAddACItem, SIGNAL(clicked()), this, SLOT(addAircraftItem()));
    connect(ui->pbRemoveACItem, SIGNAL(clicked()), this, SLOT(removeAircraftItem()));
}

void ConfigDialog::setupTabGeneral()
{
    // temporarely hide groupBox_2 ("cache")
    ui->groupBox_2->setVisible(false);

    switch (m_pSettings->value("view/measurementSystem", MarbleLocale::NauticalSystem).toInt())
    {
    case MarbleLocale::MetricSystem:
        ui->rbMetric->setChecked(true);
        break;
    case MarbleLocale::ImperialSystem:
        ui->rbImperial->setChecked(true);
        break;
    case MarbleLocale::NauticalSystem:
        ui->rbNautical->setChecked(true);
        break;
    default:
        // invalid value for MeasurementSystem, it does not make sense to check one of the radio buttons
        break;

    }

}

void ConfigDialog::saveWebviewSettings()
{
    m_pSettings->beginGroup("Webview");
    int size = ui->twURLs->rowCount();
    m_pSettings->beginWriteArray("Urls", size);
    for (int i = 0; i < size; i++)
    {
       m_pSettings->setArrayIndex(i);
       QAbstractItemModel *model = ui->twURLs->model();
       m_pSettings->setValue("description", model->data(model->index(i, 0)).toString());
       m_pSettings->setValue("url", model->data(model->index(i, 1)).toString());
    }
    m_pSettings->endArray();
    m_pSettings->endGroup();
    m_pSettings->sync();

}

void ConfigDialog::saveContinentsSettings()
{
    int size = ui->twContinents->rowCount();
    QStringList activeContinents;
    for (int i = 0; i < size; i++)
    {
        if (ui->twContinents->item(i,0)->checkState() == Qt::Checked)
        {
            activeContinents.append(ui->twContinents->item(i,0)->text());
        }
    }
    m_pSettings->setValue("view/activeContinents", activeContinents);
    m_pSettings->sync();
}

void ConfigDialog::saveAircraftSettings()
{

    ui->tvAircraft->saveToSettings();
}

void ConfigDialog::saveGeneralSettings()
{
    MarbleLocale::MeasurementSystem measurementSystem;
    if (ui->rbMetric->isChecked())
    {
        measurementSystem = MarbleLocale::MetricSystem;
    }
    else if (ui->rbImperial->isChecked())
    {
        measurementSystem = MarbleLocale::ImperialSystem;
    }
    else if (ui->rbNautical->isChecked())
    {
        measurementSystem = MarbleLocale::NauticalSystem;
    }

    m_pSettings->setValue("view/measurementSystem", measurementSystem);

}

void ConfigDialog::addWebViewItem()
{
    QAbstractItemModel *model = ui->twURLs->model();
    model->insertRow(model->rowCount());
}

void ConfigDialog::removeWebViewItem()
{
    QItemSelectionModel *selectionModel = ui->twURLs->selectionModel();
    QAbstractItemModel *model = ui->twURLs->model();

    if (selectionModel->selectedRows().size() > 0)
    {
        for (int i = selectionModel->selectedRows().size() - 1 ; i >= 0 ; i--)
        {
            int row = selectionModel->selectedRows().at(i).row();
            model->removeRow(row);
        }
    }
}

void ConfigDialog::addAircraftItem()
{
    AircraftConfigModel *model = dynamic_cast<AircraftConfigModel*>(ui->tvAircraft->model());
    model->insertRow(model->rowCount());

}

void ConfigDialog::removeAircraftItem()
{
    QItemSelectionModel *selectionModel = ui->tvAircraft->selectionModel();
    AircraftConfigModel *model = dynamic_cast<AircraftConfigModel*>(ui->tvAircraft->model());

    if (selectionModel->selectedRows().size() > 0)
    {
        for (int i = selectionModel->selectedRows().size() - 1 ; i >= 0 ; i--)
        {
            int row = selectionModel->selectedRows().at(i).row();
            model->removeRow(row);
        }
    }

}

void ConfigDialog::writeSettings()
{
    saveWebviewSettings();
    saveAircraftSettings();
    saveContinentsSettings();
    saveGeneralSettings();
    m_pMapWidget->writePluginSettings(*m_pSettings);

}

//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 47 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:02:47 +0000 (Wed, 07 Nov 2018) $
//


#include "databasehandler.h"

#include <QSqlQuery>
#include <QVariant>
#include <QDebug>

#include <GeoDataCoordinates.h>
#include <GeoDataPlacemark.h>
#include <GeoDataLinearRing.h>
#include <GeoDataLineStyle.h>
#include <GeoDataStyle.h>
#include <MarbleDirs.h>

#include "defs.h"
#include "sql_queries.h"

DatabaseHandler::DatabaseHandler(QObject *parent) :
    QObject(parent),
    upperLimit(15000),
    lowerLimit(0),
    m_airspacesCount(0),
    m_airportsCount(0),
    m_runwaysCount(0),
    m_navaidsCount(0)
{

}

int DatabaseHandler::connectDatabase()
{
#ifdef WIN32
    QString path(qgetenv("PROGRAMDATA")); // usually points to c:\ProgramData
    QString databaseFile = path.append("/%1/%2").arg(appName).arg(dbFilename);
    if (!QFile::exists(databaseFile))
    {
        databaseFile = dbFilename; // take database in app path
    }
#else
    QString databaseFile = dbFilename; // take database in app path with name from defs.h
#endif
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    if(!m_db.isValid()){
        // Ausgabe der Fehlermeldung wird durch das QSqlDatabase vorgenommen.
        // Ausserdem werden im Fehlerfall auch alle verfügbaren Treiber angezeigt.
        return -1;
    }

    if (!QFile::exists(databaseFile))
    {
        return -2;
    }
    m_db.setDatabaseName(databaseFile);
    if(!m_db.open()){
        return -3;
    }

    if(!m_db.isOpen()){
        return -4;
    }

    //qDebug() << " sqlite has query size:" << m_db.driver()->hasFeature(QSqlDriver::QuerySize); -> false

    checkDatabase();
    return 0;
}


#if 0
void DatabaseHandler::getAirports(Marble::GeoDataDocument &document)
{
    QMultiHash<qlonglong, QStringList> runways;

    loadRunways(runways);

    QString sQuery;
    sQuery = "SELECT rowid, icao, type, name, lat_deg, lon_deg, elev_ft, country, continent, municipality, home_url, wikipedia_url, source  FROM airports WHERE continent = \"EU\" AND NOT type LIKE 'HELI%' AND NOT type='GLIDING' AND NOT type='LIGHT_AIRCRAFT';";
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    // save Marble dataPath for later reset
    QString dataPath = Marble::MarbleDirs::marbleDataPath();
    Marble::MarbleDirs::setMarbleDataPath(QDir::currentPath());

    while (query.next())
    {
        //Create the placemark representing the airport
        QString code;
        if (query.value(1).toString() == "-" || query.value(1).toString().isEmpty())
        {
            code = query.value(3).toString();
        }
        else
        {
            code = query.value(1).toString();
        }
        Marble::GeoDataPlacemark *placemarkTemp = new Marble::GeoDataPlacemark(code);
        float fLatitude = query.value(4).toFloat();
        float fLongitude = query.value(5).toFloat();
        float fAltitude = query.value(6).toFloat();
        placemarkTemp->setCoordinate( fLongitude, fLatitude, fAltitude/3.28084, Marble::GeoDataCoordinates::Degree );
        placemarkTemp->setCountryCode(query.value(7).toString());

        qlonglong airportId = query.value(0).toLongLong(); // rowid
        QString sDescription = query.value(3).toString();  // Name
        sDescription.append("<br>");
        sDescription.append(query.value(2).toString()); // Type
        sDescription.append("<br>");
        sDescription.append(query.value(9).toString()); // Municipality
        sDescription.append(" ");
        sDescription.append(query.value(7).toString()); // Country
        sDescription.append("<br>");


        QList<QStringList> runwayData = runways.values(airportId);
        for (int i = 0; i < runwayData.size(); i++)
        {
            QStringList data = runwayData.at(i);
            QString t = QString("Runway %1 %2x%3 %4 <br>Status: %5").arg(data.at(0), data.at(1), data.at(2), data.at(3), data.at(4));
//            QString t = QString("Runway %1 %2x%3 %4 <br>Status: %5 Lighted: %6").arg(data.at(0), data.at(1), data.at(2), data.at(3), data.at(4), data.at(5));
            sDescription.append(t);
            sDescription.append("<br>");
        }

        sDescription.append(query.value(10).toString()); // Home URL
        sDescription.append("<br>");
        sDescription.append(query.value(11).toString()); // Wiki URL
        sDescription.append("<br>");
        sDescription.append("Quellen:"); // Source
        sDescription.append("<br>");

        if (query.value(12).toInt() & 0x01 ) // Source
        {
            sDescription.append("www.openaip.net"); // Source
            sDescription.append("<br>");
        }
        if (query.value(12).toInt() & 0x02 ) // Source
        {
            sDescription.append("www.ourairports.net"); // Source
            sDescription.append("<br>");
        }


        placemarkTemp->setDescription(sDescription);
        //placemarkTemp->set
        //placemarkTemp->setAbstractView();
        //placemarkTemp->setStyle();


        //Add styles (icons, colors, etc.) to the placemarks
        Marble::GeoDataStyle *styleTemp = new Marble::GeoDataStyle;
        Marble::GeoDataIconStyle iconStyle;
        iconStyle.setIconPath(Marble::MarbleDirs::path("data/bitmaps/airport-marker.png"));
        styleTemp->setIconStyle(iconStyle);
        placemarkTemp->setStyle(styleTemp);


        document.append(placemarkTemp );
    }
    Marble::MarbleDirs::setMarbleDataPath(dataPath);

}
#endif

#ifdef OURAIRPORTS
void DatabaseHandler::getAirports(Marble::GeoDataDocument &document)
{
    QMultiHash<int, QStringList> runways;

    loadRunways(runways);

    QString sQuery;
    sQuery = "SELECT ident, name, latitude_deg, longitude_deg, elevation_ft, iso_country, municipality, home_link, id FROM airports WHERE continent = \"EU\" AND (ident LIKE \"L%\" OR ident LIKE \"E%\");";
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    // save Marble dataPath for later reset
    QString dataPath = Marble::MarbleDirs::marbleDataPath();
    Marble::MarbleDirs::setMarbleDataPath(QDir::currentPath());

    while (query.next())
    {

        //Create the placemark representing the airport
        Marble::GeoDataPlacemark *placemarkTemp = new Marble::GeoDataPlacemark(query.value(0).toString());
        float fLatitude = query.value(2).toFloat();
        float fLongitude = query.value(3).toFloat();
        float fAltitude = query.value(4).toFloat() * 0.3048; // ft to meters
        placemarkTemp->setCoordinate( fLongitude, fLatitude, fAltitude, Marble::GeoDataCoordinates::Degree );
        placemarkTemp->setCountryCode(query.value(5).toString());

        int airportId = query.value(8).toInt();
        QString sDescription = query.value(1).toString();
        sDescription.append("<br>");
        sDescription.append(query.value(6).toString());
        sDescription.append(" ");
        sDescription.append(query.value(5).toString());
        sDescription.append("<br>");
        sDescription.append(query.value(7).toString());
        sDescription.append("<br>");

        QList<QStringList> runwayData = runways.values(airportId);
        for (int i = 0; i < runwayData.size(); i++)
        {
            QStringList data = runwayData.at(i);
            QString t = QString("Runway %1/%2 %3x%4 %5").arg(data.at(0), data.at(1), data.at(2), data.at(3), data.at(4));
            if (data.at(5).compare("0") != 0)
            {
                t.append(" closed");
            }
            sDescription.append(t);
            sDescription.append("<br>");
        }

        placemarkTemp->setDescription(sDescription);
        //placemarkTemp->set
        //placemarkTemp->setAbstractView();
        //placemarkTemp->setStyle();


        //Add styles (icons, colors, etc.) to the placemarks
    // icon wird nich angezeigt , vorerst default lassen
        Marble::GeoDataStyle *styleTemp = new Marble::GeoDataStyle;
        Marble::GeoDataIconStyle iconStyle;
        iconStyle.setIconPath(Marble::MarbleDirs::path("data/bitmaps/airport-marker.png"));
        styleTemp->setIconStyle(iconStyle);
        placemarkTemp->setStyle(styleTemp);


        document.append(placemarkTemp );
    }
    Marble::MarbleDirs::setMarbleDataPath(dataPath);

}
#endif // OURAIRPORTS

#if 0
void DatabaseHandler::getNavaids(Marble::GeoDataDocument &document)
{
    QString sQuery;
    sQuery = "SELECT ident, type, name, lat_deg, lon_deg, elev_ft, country, freq_mhz, power, source FROM navaids WHERE continent = 'EU';";
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    QString dataPath = Marble::MarbleDirs::marbleDataPath();
    Marble::MarbleDirs::setMarbleDataPath(QDir::currentPath());

    while (query.next())
    {

        //Create the placemark representing the airport
        Marble::GeoDataPlacemark *placemarkTemp = new Marble::GeoDataPlacemark(query.value(0).toString());
        float fLatitude = query.value(3).toFloat();
        float fLongitude = query.value(4).toFloat();
        float fElevation = query.value(5).toFloat();
        placemarkTemp->setCoordinate( fLongitude, fLatitude, fElevation/3.28084, Marble::GeoDataCoordinates::Degree );
        placemarkTemp->setCountryCode(query.value(6).toString());

        QString sDescription = query.value(2).toString();
        QString type = query.value(1).toString();
        QString freq = query.value(7).toString();
        sDescription.append("<br>");
        sDescription.append(type);
        sDescription.append("<br>");
        sDescription.append(freq);
        sDescription.append("<br>");
//        sDescription.append(query.value(8).toString());  // power
//        sDescription.append("<br>");

        sDescription.append("Quellen:"); // Source
        sDescription.append("<br>");

        if (query.value(9).toInt() & 0x01 ) // Source
        {
            sDescription.append("www.openaip.net"); // Source
            sDescription.append("<br>");
        }
        if (query.value(9).toInt() & 0x02 ) // Source
        {
            sDescription.append("www.ourairports.com"); // Source
            sDescription.append("<br>");
        }


        placemarkTemp->setDescription(sDescription);
        //placemarkTemp->set
        //placemarkTemp->setAbstractView();
        //placemarkTemp->setStyle();


        //Add styles (icons, colors, etc.) to the placemarks
    // icon wird nich angezeigt , vorerst default lassen
        Marble::GeoDataStyle *styleTemp = new Marble::GeoDataStyle;
        Marble::GeoDataIconStyle iconStyle;

        if (type == "VOR" || type == "DVOR")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/vor-marker.png" ));
//            iconStyle.setIconPath(Marble::MarbleDirs::path( ":/waypoints/vor-marker.png" ));
        }
        else if (type == "VOR-DME" || type == "DVOR-DME")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/vor-dme-marker.png" ));
//            iconStyle.setIconPath(Marble::MarbleDirs::path( ":/waypoints/vor-dme-marker.png" ));
        }
        else if (type == "VORTAC" || type == "DVORTAC")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/vortac-marker.png" ));
        }
        else if (type == "DME")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/dme-marker.png" ));
        }
        else if (type.compare("NDB") >= 0 )
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/ndb-marker.png" ));
        }
        styleTemp->setIconStyle(iconStyle);
        placemarkTemp->setStyle(styleTemp);

        document.append(placemarkTemp );
    }
    Marble::MarbleDirs::setMarbleDataPath(dataPath);
}
#endif


#ifdef OURAIRPORTS
void DatabaseHandler::getNavaids(Marble::GeoDataDocument &document)
{
    QString sQuery;
    sQuery = "SELECT ident, name, latitude_deg, longitude_deg, type, iso_country, id FROM navaids;";
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    QString dataPath = Marble::MarbleDirs::marbleDataPath();
    Marble::MarbleDirs::setMarbleDataPath(QDir::currentPath());

    while (query.next())
    {

        //Create the placemark representing the airport
        Marble::GeoDataPlacemark *placemarkTemp = new Marble::GeoDataPlacemark(query.value(0).toString());
        float fLatitude = query.value(2).toFloat();
        float fLongitude = query.value(3).toFloat();
        placemarkTemp->setCoordinate( fLongitude, fLatitude, 0, Marble::GeoDataCoordinates::Degree );
        placemarkTemp->setCountryCode(query.value(5).toString());

        QString sDescription = query.value(1).toString();
        sDescription.append("<br>");
        QString type = query.value(4).toString();
        sDescription.append(type);
        /*
        sDescription.append(" ");
        sDescription.append(query.value(5).toString());
        sDescription.append("<br>");
        sDescription.append(query.value(7).toString());
        sDescription.append("<br>");
        */

        placemarkTemp->setDescription(sDescription);
        //placemarkTemp->set
        //placemarkTemp->setAbstractView();
        //placemarkTemp->setStyle();


        //Add styles (icons, colors, etc.) to the placemarks
    // icon wird nich angezeigt , vorerst default lassen
        Marble::GeoDataStyle *styleTemp = new Marble::GeoDataStyle;
        Marble::GeoDataIconStyle iconStyle;

        if (type == "VOR")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/vor-marker.png" ));
        }
        else if (type == "VOR-DME")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/vor-dme-marker.png" ));
        }
        else if (type == "VORTAC")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/vortac-marker.png" ));
        }
        else if (type == "DME")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/dme-marker.png" ));
        }
        else if (type.compare("NDB") >= 0 )
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/ndb-marker.png" ));
        }
        styleTemp->setIconStyle(iconStyle);
        placemarkTemp->setStyle(styleTemp);

        document.append(placemarkTemp );
    }
    Marble::MarbleDirs::setMarbleDataPath(dataPath);
}
#endif // OURAIRPORTS

#if 0
QStringList DatabaseHandler::getWaypointInfo(QString code, QString country, int type)
{
    QString sQuery;
    QStringList result;
    if (type == 0)
    {
        sQuery = QString("SELECT icao, name, lon_deg, lat_deg, type, country FROM airports WHERE icao ='%1' OR name='%1' AND country ='%2' LIMIT 1;").arg(code,country);
    }
    else
    {
        sQuery = QString("SELECT ident, name, lon_deg, lat_deg, type, country FROM navaids WHERE ident ='%1' AND country ='%2' LIMIT 1;").arg(code,country);
    }

    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    while (query.next())
    {
        result << query.value(0).toString() << query.value(1).toString() << query.value(4).toString() << query.value(2).toString() << query.value(3).toString();

    }
    return result;

}
#endif

QList<QStringList> DatabaseHandler::findWaypoint(QString wpt, bool icaoonly)
{
    QString sQuery;
    QList<QStringList> result;
    if (icaoonly)
    {
        sQuery = QString("SELECT icao, name, type, country, lon_deg, lat_deg FROM airports WHERE icao LIKE '\%%1\%';").arg(wpt);
    }
    else
    {
        sQuery = QString("SELECT icao, name, type, country, lon_deg, lat_deg FROM airports WHERE icao LIKE '\%%1\%' OR name LIKE '\%%1\%';").arg(wpt);
    }
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);
    while (query.next())
    {
        QStringList record;
        record << QString("A") << query.value(0).toString() << query.value(1).toString() << query.value(2).toString() << query.value(3).toString() << query.value(4).toString() << query.value(5).toString();
        result.append(record);
    }

    sQuery = QString("SELECT ident, name, type, country, lon_deg, lat_deg FROM navaids WHERE ident LIKE '%1';").arg(wpt);
    query.exec(sQuery);
    while (query.next())
    {
        QStringList record;
        record << QString("N") << query.value(0).toString() << query.value(1).toString() << query.value(2).toString() << query.value(3).toString() << query.value(4).toString() << query.value(5).toString();
        result.append(record);
    }

    sQuery = QString("SELECT ident, country, lon_deg, lat_deg FROM icao5lnc WHERE ident LIKE '\%%1\%';").arg(wpt);
    query.exec(sQuery);
    while (query.next())
    {
        QStringList record;
        record << QString("F") << query.value(0).toString() << query.value(0).toString() << "5LNC" << query.value(1).toString() << query.value(2).toString() << query.value(3).toString();
        result.append(record);
    }
    return result;

}

bool DatabaseHandler::cleanDatabase()
{
    bool ret = true;
    QSqlQuery query;
    QString sQuery;
    sQuery = DROP_AIRPORTS;
    ret &= query.exec(sQuery);
    sQuery = CREATE_AIRPORTS;
    ret &= query.exec(sQuery);
    sQuery = UPD_FILES_AFT_DROP_APT;
    ret &= query.exec(sQuery);

    sQuery = DROP_RUNWAYS;
    ret &= query.exec(sQuery);
    sQuery = CREATE_RUNWAYS;
    ret &= query.exec(sQuery);

    sQuery = DROP_AIRSPACES;
    ret &= query.exec(sQuery);
    sQuery = CREATE_AIRSPACES;
    ret &= query.exec(sQuery);
    sQuery = UPD_FILES_AFT_DROP_ASP;
    ret &= query.exec(sQuery);

    sQuery = DROP_NAVAIDS;
    ret &= query.exec(sQuery);
    sQuery = CREATE_NAVAIDS;
    ret &= query.exec(sQuery);
    sQuery = UPD_FILES_AFT_DROP_NAV;
    ret &= query.exec(sQuery);

    sQuery = "VACUUM;";
    ret &= query.exec(sQuery);
    return ret;
}

bool DatabaseHandler::resetUpdateStatus()
{
    QString sQuery;
    QSqlQuery query;
    sQuery = RESET_UPD_STATUS;
    return query.exec(sQuery);
}

bool DatabaseHandler::checkDatabase()
{
    QString sQuery;
    QSqlQuery query;
    query.setForwardOnly(true);
    int result = 0;
    bool ret = false;


    sQuery = "SELECT cat, polygon, top, topunit, bottom, bottomunit, name FROM airspaces LIMIT 1;";
    query.exec(sQuery);
    if (query.isActive())
    {
        sQuery = "SELECT count() as nbr FROM airspaces";
        query.exec(sQuery);
        query.first();
        m_airspacesCount = query.value(0).toInt();
        qDebug() << m_airspacesCount << "airspace records found in database";
    }
    else
    {
        result++;
    }

    sQuery = "SELECT rowid, icao, type, name, lat_deg, lon_deg, elev_ft, country, continent, municipality, home_url, wikipedia_url, source  FROM airports LIMIT 1;";
    query.exec(sQuery);
    if (query.isActive())
    {
        sQuery = "SELECT count() as nbr FROM airports";
        query.exec(sQuery);
        query.first();
        m_airportsCount = query.value(0).toInt();
        qDebug() << m_airportsCount << "airport records found in database";
    }
    else
    {
        result++;
    }

    sQuery = "SELECT airports_rowid, icao, name, sfc, status, length_m, width_m, lighted FROM runways LIMIT 1;";
    query.exec(sQuery);
    if (query.isActive())
    {
        sQuery = "SELECT count() as nbr FROM runways";
        query.exec(sQuery);
        query.first();
        m_runwaysCount = query.value(0).toInt();
        qDebug() << m_runwaysCount << "runway records loaded";
    }
    else
    {
        result++;
    }

    sQuery = "SELECT ident, type, name, lat_deg, lon_deg, elev_ft, country, freq_mhz, power, source FROM navaids LIMIT 1;";
    query.exec(sQuery);
    if (query.isActive())
    {
        sQuery = "SELECT count() as nbr FROM navaids";
        query.exec(sQuery);
        query.first();
        m_navaidsCount = query.value(0).toInt();
        qDebug() << m_navaidsCount << "navigation aid records found in database";
    }
    else
    {
        result++;
    }

    if (result == 0)
    {
        ret = true;
    }
    return ret;
}

#if 0
void DatabaseHandler::loadRunways(QMultiHash<qlonglong, QStringList> &table)
{

    QString sQuery = "SELECT airports_rowid, icao, name, sfc, status, length_m, width_m, lighted FROM runways;";
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);
    while (query.next())
    {
        qlonglong key = query.value(0).toLongLong();
        QStringList data;
        // Data: runway name, length, width, surface, status, lighted
        QString length = QString::number(query.value(5).toInt());
        QString width = QString::number(query.value(6).toInt());
        data << query.value(2).toString() << length << width << query.value(3).toString() << query.value(4).toString() << query.value(7).toString();
        table.insert(key, data);
    }
    query.finish();
}
#endif

#ifdef OURAIRPORTS
void DatabaseHandler::loadRunways(QMultiHash<int, QStringList> &table)
{
    QString sQuery2 = "SELECT airport_ref, length_ft, width_ft, surface, le_ident, he_ident, closed FROM runways;";
    QSqlQuery query2;
    query2.setForwardOnly(true);
    query2.exec(sQuery2);
    while (query2.next())
    {
        int key = query2.value(0).toInt();
        QStringList data;
        // Data: runway id 1 / id 2, length, width, surface, closed
        QString length = QString::number((int)(query2.value(1).toFloat() * 0.3048));
        QString width = QString::number((int)(query2.value(2).toFloat() * 0.3048));
        data << query2.value(4).toString() << query2.value(5).toString() << length << width << query2.value(3).toString() << query2.value(6).toString();
        table.insert(key, data);
    }
    query2.finish();
}
#endif

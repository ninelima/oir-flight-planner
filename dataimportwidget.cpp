//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include <QFileDialog>

#include "dataimportwidget.h"
#include "ui_dataimportwidget.h"


#include "openaipreader.h"
#include "ourairportsreader.h"


DataImportWidget::DataImportWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DataImportWidget),
    m_isWorking(false)
{
    ui->setupUi(this);

    aipreader = new OpenAIPReader;
    aipreader->moveToThread(&aipreadWorker);

    oareader = new OurairportsReader;
    oareader->moveToThread(&ourreadWorker);

    connect(aipreader, SIGNAL(recordProceeded(QString)), ui->teLog, SLOT(appendPlainText(QString)));
    connect(oareader, SIGNAL(recordProceeded(QString)), ui->teLog, SLOT(appendPlainText(QString)));
    connect(&aipreadWorker, SIGNAL(finished()), aipreader, SLOT(deleteLater()));
    connect(&ourreadWorker, SIGNAL(finished()), oareader, SLOT(deleteLater()));
    connect(ui->pbAirports, SIGNAL(clicked()), this, SLOT(readAirportFile()));
    connect(this, SIGNAL(rtrAirportFileAIP(QStringList)), aipreader, SLOT(readAirportFiles(QStringList)));
    connect(this, SIGNAL(rtrAirportFileOur(QStringList)), oareader, SLOT(readAirportFile(QStringList)));
    connect(ui->pbNavaids, SIGNAL(clicked()), this, SLOT(readNavaidsFile()));
    connect(this, SIGNAL(rtrNavaidsFileAIP(QStringList)), aipreader, SLOT(readNavaidsFiles(QStringList)));
    connect(this, SIGNAL(rtrNavaidsFileOur(QStringList)), oareader, SLOT(readNavaidsFile(QStringList)));
    connect(ui->pbAirspaces, SIGNAL(clicked()), this, SLOT(readAirspaceFile()));
    connect(this, SIGNAL(rtrAirspaceFile(QStringList)), aipreader, SLOT(readAirspaceFiles(QStringList)));
    connect(aipreader, SIGNAL(fileFinished()), this, SLOT(processingFinished()));
    connect(oareader, SIGNAL(fileFinished()), this, SLOT(processingFinished()));
    aipreadWorker.start();
    ourreadWorker.start();
}

DataImportWidget::~DataImportWidget()
{
    aipreadWorker.quit();
    aipreadWorker.wait();
    ourreadWorker.quit();
    ourreadWorker.wait();
    delete ui;
}

void DataImportWidget::endisControls(bool enable)
{
    ui->pbAirports->setEnabled(enable);
    ui->pbAirspaces->setEnabled(enable);
    ui->pbNavaids->setEnabled(enable);
}

void DataImportWidget::readAirspaceFile()
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Update airspaces"), "", tr("AIP files (openaip_airspace*.aip)"));
    if (fileNames.empty()) return;

    endisControls(false);
    m_isWorking = true;
    emit rtrAirspaceFile(fileNames);
}

void DataImportWidget::readAirportFile()
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Update airports"), "", tr("AIP files (openaip_airports*.aip);; Ourairports files (*.csv)"));
    if (fileNames.empty()) return;
    endisControls(false);
    m_isWorking = true;

    QFileInfo fi(fileNames.at(0));
    if (fi.suffix().compare("csv", Qt::CaseInsensitive) == 0)
    {
        emit rtrAirportFileOur(fileNames);
    }
    else
    {
        emit rtrAirportFileAIP(fileNames);
    }
}

void DataImportWidget::readNavaidsFile()
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Update navaids"), "", tr("OpenAIP files (openaip_navaid*.aip);; Ourairports files (*.csv)"));
    if (fileNames.empty()) return;
    endisControls(false);
    m_isWorking = true;

    QFileInfo fi(fileNames.at(0));
    if (fi.suffix().compare("csv", Qt::CaseInsensitive) == 0)
    {
        emit rtrNavaidsFileOur(fileNames);
    }
    else
    {
        emit rtrNavaidsFileAIP(fileNames);
    }

}

void DataImportWidget::processingFinished()
{
    endisControls(true);
    m_isWorking = false;
    emit workerFinished();

}

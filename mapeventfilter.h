//
//
// This program is free software licensed under the GNU LGPL. You can
// find a copy of this license in LICENSE.txt in the top directory of
// the source code.
//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>

//

#ifndef MAPEVENTFILTER_H
#define MAPEVENTFILTER_H

#include <QObject>
#include <QPoint>

class MapEventFilter : public QObject
{
    Q_OBJECT

public:
    MapEventFilter(QObject * parent = 0);


protected:
     bool eventFilter(QObject *obj, QEvent *event);

Q_SIGNALS:
     void mouseMove(QPoint);
     void mouseLeftButtonPress(QPoint);

};

#endif // MAPEVENTFILTER_H

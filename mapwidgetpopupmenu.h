//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef MAPWIDGETPOPUPMENU_H
#define MAPWIDGETPOPUPMENU_H

#include <QObject>
#include <Qt>
#include <QPoint>

class QAction;
class QMenu;
class MapWidget;

namespace Marble {
class MarbleModel;
}

using namespace Marble;

class MapWidgetPopupMenu : public QObject
{
    Q_OBJECT
public:
    explicit MapWidgetPopupMenu(MapWidget*, const MarbleModel*);
    void addActionForApt( Qt::MouseButton button, QAction* action ); // actions that will only be shown clicking an airport item (with ICAO-Code)
    QPoint mousePosition() const;

signals:

public slots:
    void showLmbMenu(int x, int y);
    void showRmbMenu(int x, int y);
    void addLmbActionsGeneral(QList<QAction *> list);
    void addRmbActionsGeneral(QList<QAction *> list);

private:
    MapWidget *m_widget;
    QMenu *m_lmbMenu;
    QMenu *m_rmbMenu;
    QPoint m_mousePosition;
    QList<QAction *> m_rmbActionListGen;
    QList<QAction *> m_rmbActionListItems;

};


#endif // MAPWIDGETPOPUPMENU_H

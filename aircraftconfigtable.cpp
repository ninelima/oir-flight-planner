//
// Copyright 2016 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "aircraftconfigtable.h"
#include <QKeyEvent>
#include <QMenu>
#include <QContextMenuEvent>
#include <QAction>
#include <QSettings>
#include <QHeaderView>
#include <QDebug>

#include <aircraftconfigmodel.h>



AircraftConfigTable::AircraftConfigTable(QWidget *parent) :
    QTableView(parent),
    m_contextMenuActions(QList<QAction*>()),
    mClickedIndex(QModelIndex())
{
    m_pModel = new AircraftConfigModel();
    this->setModel(m_pModel);
    setAlternatingRowColors(true);
    resizeColumnsToContents();

}

AircraftConfigTable::~AircraftConfigTable()
{
    delete m_pModel;
}

void AircraftConfigTable::saveToSettings()
{
    AircraftConfigModel *model = dynamic_cast<AircraftConfigModel*>(this->model());
    model->saveConfig();
}


void AircraftConfigTable::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Delete)
    {

    }
    else if (event->key() == Qt::Key_Up)
    {

    }
    else if (event->key() == Qt::Key_Down)
    {

    }

    else
    {
        QTableView::keyPressEvent(event);
    }
}

void AircraftConfigTable::contextMenuEvent(QContextMenuEvent * event)
{
     mClickedIndex = this->indexAt(event->pos());
    if (mClickedIndex.isValid())
    {
        QMenu contextMenu(this);
        for (int i = 0; i < m_contextMenuActions.size(); i++)
        {
            contextMenu.addAction(m_contextMenuActions.at(i));
        }
        contextMenu.addSeparator();
//        contextMenu.addAction(m_pActGotoWpt);
        QPoint menuPos = event->globalPos();
        menuPos.setX(menuPos.x()+5);
        menuPos.setY(menuPos.y()+5);

        contextMenu.exec(menuPos);
    }
}





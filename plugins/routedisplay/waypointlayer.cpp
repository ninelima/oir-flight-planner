#include "waypointlayer.h"

#include <QPixmap>
#include <QMouseEvent>
#include <QDebug>


#include <marble/MarbleWidget.h>
#include <marble/GeoDataCoordinates.h>
#include <marble/GeoPainter.h>
#include <marble/ViewportParams.h>
#include <marble/GeoDataLatLonAltBox.h>
#include <marble/GeoDataPoint.h>
#include <geodata/graphicsitem/GeoLineStringGraphicsItem.h>


//#include "waypoint.h"
#include "routemodel.h"

namespace Marble
{

WaypointLayer::WaypointLayer(MarbleWidget *widget, QWidget *parent) :
    QObject(parent),
    m_marbleWidget(widget),
    m_mark( ":/mark.png" ),
#ifdef Q_OS_MACX
    m_font_regular( QFont( "Sans Serif", 10, 50, false ) ),
#else
    m_font_regular( QFont( "Sans Serif",  8, 50, false ) ),
#endif
    m_fontascent( QFontMetrics( m_font_regular ).ascent() ),
    m_pen( Qt::red ),
    m_showDistanceLabel( true ),
    m_showBearingLabel( true ),
    gi(0)
{
}

WaypointLayer::~WaypointLayer()
{

}

QStringList WaypointLayer::renderPosition() const
{
    //return QStringList() << "HOVERS_ABOVE_SURFACE";
    return QStringList() << "USER_TOOLS";
}

qreal WaypointLayer::zValue() const
{
    return 1.0;
}

bool WaypointLayer::render(GeoPainter *painter, ViewportParams *viewport, const QString &renderPos, GeoSceneLayer *layer)
{
//    Q_UNUSED(viewport)
    Q_UNUSED(renderPos)
    Q_UNUSED(layer)

    GeoDataFeature df("route");
    GeoDataLineString ls = m_routeModel->getLineString();
    gi = new GeoLineStringGraphicsItem(&df, &ls);
    gi->setFlags(GeoGraphicsItem::ItemIsMovable | GeoGraphicsItem::ItemIsSelectable);

    gi->paint(painter, viewport);
return true;

    // No way to paint anything if the list is empty.
//    if ( m_measureLineString.isEmpty() )
    if (m_routeModel->isEmpty())
        return true;

    painter->save();

    // Prepare for painting the measure line string and paint it.
    painter->setPen( m_pen );

    if ( m_showDistanceLabel || m_showBearingLabel) {
        drawSegments( painter );
    } else {
        painter->drawPolyline( m_routeModel->getLineString());
//        painter->drawPolyline( m_measureLineString );
    }

    // Paint the nodes of the paths.
    drawMeasurePoints( painter );

    // Paint the total distance in the upper left corner.
//    qreal totalDistance = m_routeModel->getLineString().length( marbleModel()->planet()->radius() );
    qreal totalDistance = 0;
    if ( m_routeModel->rowCount() > 1 )
        drawTotalDistanceLabel( painter, totalDistance );

    painter->restore();

    return true;
/*
    QPixmap pixmap(":/waypoints/vor-dme-marker.png");
    painter->save();
    for (int i = 0; i < m_pNavaids->size(); i++)
    {
        Waypoint *wp = m_pNavaids->at(i);
        GeoDataLatLonAltBox box(wp->getCoordinates());
        if ( box.intersects( viewport->viewLatLonAltBox() ) ) {
            //GeoDataPoint point(wp->getCoordinates());
            painter->drawPixmap(wp->getCoordinates(), pixmap);
//            ++painted;
        }

    }

    painter->restore();
    return true;
*/
}

void WaypointLayer::drawSegments( GeoPainter* painter )
{
    GeoDataLineString measureLineString = m_routeModel->getLineString();
    for ( int segmentIndex = 0; segmentIndex < measureLineString.size() - 1; ++segmentIndex ) {
        GeoDataLineString segment( Tessellate );
        segment << measureLineString[segmentIndex] ;
        segment << measureLineString[segmentIndex + 1];

        QPen shadowPen( Oxygen::aluminumGray5 );
        shadowPen.setWidthF(4.0);
        painter->setPen( shadowPen );
        painter->drawPolyline( segment );

        QString infoString;
        MarbleLocale::MeasurementSystem measurementSystem = m_routeModel->getMeasurementSystem();

        if ( m_showDistanceLabel ) {
            qreal segmentLength = m_routeModel->getDistance(segmentIndex+1);

            if ( measurementSystem == MarbleLocale::MetricSystem ) {
                    infoString = tr("%1 km").arg( segmentLength, 0, 'f', 2 );
            } else if (measurementSystem == MarbleLocale::ImperialSystem) {
                infoString = QString("%1 mi").arg( segmentLength, 0, 'f', 2 );
            } else if (measurementSystem == MarbleLocale::NauticalSystem) {
                infoString = QString("%1 nm").arg( segmentLength, 0, 'f', 2 );
            }
        }

        if ( m_showBearingLabel ) {
            qreal bearing = m_routeModel->getBearing(segmentIndex+1);
            QString bearingString = QString::fromUtf8( "%1°" ).arg( bearing, 0, 'f', 0 );
            if ( !infoString.isEmpty() ) {
                infoString.append( "\n" );
            }
            infoString.append( bearingString );
        }

        if ( !infoString.isEmpty() ) {
            QPen linePen;

            // have three alternating colors for the segments
/*
            switch ( segmentIndex % 3 ) {
            case 0:
                linePen.setColor( Oxygen::brickRed4 );
                break;
            case 1:
                linePen.setColor( Oxygen::forestGreen4 );
                break;
            case 2:
                linePen.setColor( Oxygen::skyBlue4 );
                break;
            }
*/
            linePen.setColor( Oxygen::brickRed4 );
            linePen.setWidthF(2.0);
            painter->setPen( linePen );
            painter->drawPolyline( segment, infoString, LineCenter );
        }
    }
}

void WaypointLayer::drawMeasurePoints( GeoPainter *painter ) const
{
    GeoDataLineString measureLineString = m_routeModel->getLineString();
    // Paint the marks.
    GeoDataLineString::const_iterator itpoint = measureLineString.constBegin();
    GeoDataLineString::const_iterator const endpoint = measureLineString.constEnd();
    for (; itpoint != endpoint; ++itpoint )
    {
        painter->drawPixmap( *itpoint, m_mark );
    }

}

void WaypointLayer::drawTotalDistanceLabel( GeoPainter *painter,
                                          qreal totalDistance ) const
{
    QString  distanceString;

    MarbleLocale::MeasurementSystem measurementSystem;
    measurementSystem = MarbleGlobal::getInstance()->locale()->measurementSystem();

    if ( measurementSystem == MarbleLocale::MetricSystem ) {
        if ( totalDistance >= 1000.0 ) {
            distanceString = tr("Total Distance: %1 km").arg( totalDistance/1000.0 );
        }
        else {
            distanceString = tr("Total Distance: %1 m").arg( totalDistance );
        }
    }
    else if (measurementSystem == MarbleLocale::ImperialSystem) {
        distanceString = QString("Total Distance: %1 mi").arg( totalDistance/1000.0 * KM2MI );
    } else if (measurementSystem == MarbleLocale::NauticalSystem) {
        distanceString = QString("Total Distance: %1 nm").arg( totalDistance/1000.0 * KM2NM );
    }

    painter->setPen( QColor( Qt::black ) );
    painter->setBrush( QColor( 192, 192, 192, 192 ) );

    painter->drawRect( 10, 105, 10 + QFontMetrics( m_font_regular ).boundingRect( distanceString ).width() + 5, 10 + m_fontascent + 2 );
    painter->setFont( m_font_regular );
    painter->drawText( 15, 110 + m_fontascent, distanceString );
}

RenderState WaypointLayer::renderState() const
{
    return RenderState( "Routing", Complete );
}

void WaypointLayer::setRouteModel(RouteModel *model)
{
    m_routeModel = model;
}

bool WaypointLayer::eventFilter(QObject *o, QEvent *e)
{
    if (e->type() == QEvent::MouseMove)
    {
        if (m_mouseButtonDown)
        {
        //qDebug() << "Move";
        return true;
        }
    }
    else if (e->type() == QEvent::MouseButtonRelease)
    {
        m_mouseButtonDown = false;
        //qDebug() << "ButtonRelease";
    }
    else if (e->type() == QEvent::MouseButtonPress) {
         //qDebug() << "ButtonPress";
         QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(e);
         if (mouseEvent->button() == Qt::LeftButton)
         {
             m_mouseButtonDown = true;
             qDebug() << "LeftButton";
             GeoDataCoordinates coord;
             qreal lat;
             qreal lon;
             m_marbleWidget->geoCoordinates(mouseEvent->x(),mouseEvent->y(), lon, lat, GeoDataCoordinates::Degree);
             coord.setLongitude(lon, GeoDataCoordinates::Degree);
             coord.setLatitude(lat, GeoDataCoordinates::Degree);
             //QPoint mousePos(mouseEvent->x(),mouseEvent->y());
             //GeoDataCoordinates g();
//             GeoDataLatLonAltBox b(coord);
             GeoDataLatLonAltBox b = gi->latLonAltBox();
             if (b.contains(coord))
             {
                 qDebug() << "contains mousepos";
              }
//             QVector<const GeoDataPlacemark *> placeMark = m_marbleWidget->whichFeatureAt(mousePos);

//             for (int i = 0; i < placeMark.size(); i++)
//             {
//                 qDebug() << placeMark.at(i)->coordinate().toString();
//             }

             //m_routeModel->getLineString();

        }
    }

}









} // namespace Marble

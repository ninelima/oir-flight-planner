//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 52 $
//  $Author: brougham73 $
//  $Date: 2019-06-04 16:46:04 +0000 (Tue, 04 Jun 2019) $
//

#ifndef ROUTEWAYPOINT_H
#define ROUTEWAYPOINT_H

#include <QObject>
#include <QTime>
#include <QRegion>
#include <GeoDataCoordinates.h>

using namespace Marble;

class RouteWaypoint : public QObject
{
    Q_OBJECT
public:
    explicit RouteWaypoint(QObject *parent = 0);
    void clear();
    void setName(const QString &name) { m_name = name; }
    void setCode(const QString &code) { m_code = code; }
    void setType(const QString &type) { m_type = type; }
    void setSubtype(const QString &subtype) { m_subtype = subtype; }
    void setLat(qreal lat);
    void setLon(qreal lon);
    void setCoordinates(GeoDataCoordinates coord);
    void setCourse(qreal course) { m_course = course; }
    void setDistance(qreal distance) { m_distance = distance; }
    void setDistCumulated(qreal distance) { m_distCumulated = distance; }
    void setDistCumulatedRoute(qreal distance) { m_distCumulatedRoute = distance; }
    void setTime(QTime time) { m_time = time; }
    void setTimeCumulated(QTime time) { m_timeCumulated = time; }
    void setTimeCumulatedRoute(QTime time) { m_timeCumulatedRoute = time; }
    void setRegion(QRegion region) { m_region = region; }
    void setConnectionRegion(QRegion region) { m_connRegion = region; }
    void setTotalizer(bool totalizer) { m_isTotalizer = totalizer; }


    QString getName() { return m_name; }
    QString getCode() { return m_code; }
    QString getType() { return m_type; }
    QString getSubtype() { return m_subtype; }
    GeoDataCoordinates getCoordinates();
    qreal getLon();
    qreal getLat();
    qreal getCourse() { return m_course; }
    qreal getDistance() { return m_distance; }
    qreal getDistCumulated() { return m_distCumulated; }
    qreal getDistCumulatedRoute() { return m_distCumulatedRoute; }
    QTime getTime() { return m_time; }
    QTime getTimeCumulated() { return m_timeCumulated; }
    QTime getTimeCumulatedRoute() { return m_timeCumulatedRoute; }
    QRegion getRegion() { return m_region; }
    QRegion getConnectionRegion() { return m_connRegion; }
    bool isTotalizer() { return m_isTotalizer; }



signals:

public slots:

private:
    QString m_name;
    QString m_code;
    QString m_type;
    QString m_subtype;
    GeoDataCoordinates m_coord;
    qreal m_course;
    qreal m_distance;
    qreal m_distCumulated;
    qreal m_distCumulatedRoute;
    QTime m_time;
    QTime m_timeCumulated;
    QTime m_timeCumulatedRoute;
    QRegion m_region;
    QRegion m_connRegion;
    bool m_isTotalizer;

};

#endif // ROUTEWAYPOINT_H

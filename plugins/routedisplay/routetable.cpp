//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "routetable.h"
#include <QKeyEvent>
#include <QMenu>
#include <QContextMenuEvent>
#include <QAction>
#include <QSettings>
#include <QHeaderView>

#include "routemodel.h"
#include "customizableheaderview.h"


RouteTable::RouteTable(QWidget *parent) :
    QTableView(parent),
    m_contextMenuActions(QList<QAction*>()),
    mClickedIndex(QModelIndex()),
    m_pActionGotoWpt(0),
    m_pActionToggleItemTotalizer(0),
    m_pActionRemoveWaypoint(0)
{
    QSettings settings;
    settings.beginGroup("Webview");
    int size = settings.beginReadArray("Urls");
    for (int i = 0; i < size; ++i) {
         settings.setArrayIndex(i);
         QString desc = QString("Show '%1'").arg(settings.value("description").toString());
         m_contextMenuActions.append(new QAction(desc, this));
         connect(m_contextMenuActions.at(i), SIGNAL(triggered()), this, SLOT(showInfoClicked()));

    }
    settings.endArray();

    m_pActionGotoWpt = new QAction(tr("Goto waypoint"), this);
    connect(m_pActionGotoWpt, SIGNAL(triggered()), this, SLOT(gotoWptClicked()));

    m_pActionToggleItemTotalizer = new QAction(tr("Toggle stopover"), this);
    connect(m_pActionToggleItemTotalizer, SIGNAL(triggered()), this, SIGNAL(toggleItemTotalizer()));

    m_pActionRemoveWaypoint = new QAction(tr("Remove item"), this);
    connect(m_pActionRemoveWaypoint, SIGNAL(triggered()), this, SIGNAL(removeItem()));

    m_pActionMoveUp = new QAction(tr("Move up"), this);
    connect(m_pActionMoveUp, SIGNAL(triggered()), this, SIGNAL(moveItemUp()));

    m_pActionMoveDown = new QAction(tr("Move down"), this);
    connect(m_pActionMoveDown, SIGNAL(triggered()), this, SIGNAL(moveItemDown()));

    m_pActionInsert = new QAction(tr("Insert"), this);
    connect(m_pActionInsert, SIGNAL(triggered()), this, SIGNAL(insertItem()));

    // set special horizontal header
    m_pHeaderView = new CustomizableHeaderView(this->horizontalHeader()->orientation(), this);
    this->setHorizontalHeader(m_pHeaderView);
    m_pHeaderView->setSectionsMovable(true);

}


void RouteTable::keyPressEvent(QKeyEvent *event)
{
    RouteModel *routeModel = dynamic_cast<RouteModel*>(this->model());
    if (event->key() == Qt::Key_Delete)
    {
        routeModel->removeRow(this->selectionModel()->currentIndex().row());
    }
    else if (event->key() == Qt::Key_Up)
    {
        routeModel->moveUp(this->selectionModel()->currentIndex().row());
    }
    else if (event->key() == Qt::Key_Down)
    {
        routeModel->moveDown(this->selectionModel()->currentIndex().row());
    }

    else
    {
        QTableView::keyPressEvent(event);
    }
}

void RouteTable::contextMenuEvent(QContextMenuEvent * event)
{
     mClickedIndex = this->indexAt(event->pos());
    if (mClickedIndex.isValid())
    {
        QMenu contextMenu(this);
        for (int i = 0; i < m_contextMenuActions.size(); i++)
        {
            contextMenu.addAction(m_contextMenuActions.at(i));
        }
        contextMenu.addSeparator();
        contextMenu.addAction(m_pActionGotoWpt);
        contextMenu.addAction(m_pActionToggleItemTotalizer);
        contextMenu.addAction(m_pActionMoveUp);
        contextMenu.addAction(m_pActionMoveDown);
        contextMenu.addAction(m_pActionRemoveWaypoint);
//        contextMenu.addAction(m_pActionInsert);

        QPoint menuPos = event->globalPos();
        menuPos.setX(menuPos.x()+5);
        menuPos.setY(menuPos.y()+5);

        contextMenu.exec(menuPos);
    }
}

void RouteTable::showInfoClicked()
{
    QString code = this->model()->data(this->model()->index(mClickedIndex.row(), 0)).toString();

    QObject *senderObj = this->sender();
    for (int i = 0; i < m_contextMenuActions.size(); i++)
    {
        if (m_contextMenuActions.at(i) == senderObj)
        {
            emit showAptInfo(i, code);
            break;
        }
    }

}

void RouteTable::gotoWptClicked()
{
    qreal lon = this->model()->data(this->model()->index(mClickedIndex.row(), 3)).toFloat();
    qreal lat = this->model()->data(this->model()->index(mClickedIndex.row(), 4)).toFloat();
    emit gotoWpt(lon, lat);
}



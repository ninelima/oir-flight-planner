//
// Copyright (C) 2008-2010  Lukas Sommer < SommerLuk at gmail dot com >
// Copyright 2016 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef CUSTOMIZABLEHEADERVIEW_H
#define CUSTOMIZABLEHEADERVIEW_H

#include <QHeaderView>
#include <QPointer>

class CustomizableHeaderView : public QHeaderView
{

  Q_OBJECT


  public:
    /*!
      * Creates a new generic header.
      * @param orientation Qt::Horizontal or Qt::Vertical
      * @param parent      parent widget
      */
    explicit CustomizableHeaderView(Qt::Orientation orientation, QWidget * parent = 0);
    /** Generic destructor. */
    virtual ~CustomizableHeaderView();

  signals:
    /** This signal is emitted immediately before hiding a row or column
      * because the user used the context menu. It is \e not emitted when hideSection() or
      * setSectionHidden() was called.
      * @param index index of the row or column that will be hidden.
      * @param orientation The orientation of the header. If Qt::Horizontal,
      *                    than a column will be hidden, if Qt::Vertical,
      *                    a row will be hidden. */
    void sectionAboutToBeHidden(int index, Qt::Orientation orientation);
    /** This signal is emitted immediately before showing a row or column
      * because the user used the context menu. It is \e not emitted when
      * setSectionHidden() was called.
      * @param index index of the row or column that will be shown.
      * @param orientation The orientation of the header. If Qt::Horizontal,
      *                    than a column will be shown, if Qt::Vertical,
      *                    a row will be shown. */
    void sectionAboutToBeShown(int index, Qt::Orientation orientation);
    /** This signal is emitted immediately after hiding a row or column
      * because the user used the context menu. It is \e not emitted when hideSection() or
      * setSectionHidden() was called.
      * @param index index of the row or column that has been hidden.
      * @param orientation The orientation of the header. If Qt::Horizontal,
      *                    than a column was hidden, if Qt::Vertical,
      *                    a row was hidden. */
    void sectionHidden(int index, Qt::Orientation orientation);
    /** This signal is emitted immediately after showing a row or column
      * because the user used the context menu. It is \e not emitted when
      * setSectionHidden() was called.
      * @param index index of the row or column that has been shown.
      * @param orientation The orientation of the header. If Qt::Horizontal,
      *                    than a column was shown, if Qt::Vertical,
      *                    a row was shown. */
    void sectionShown(int index, Qt::Orientation orientation);

  protected:
    /** Displays the context menu.
      *
      * Reimplemented from class QAbstractScrollArea. */
    virtual void contextMenuEvent (QContextMenuEvent *e);

  private:
};

#endif

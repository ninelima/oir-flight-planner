//
//
// This program is free software licensed under the GNU LGPL. You can
// find a copy of this license in LICENSE.txt in the top directory of
// the source code.
//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>

//

#include "mapeventfilter.h"

#include <MarbleWidget.h>
#include <MarbleWidgetInputHandler.h>


#include <QEvent>
#include <QKeyEvent>
#include <QDebug>


MapEventFilter::MapEventFilter(QObject *parent):
    QObject(parent),
    m_activationKeyDown(false)
{

}


bool MapEventFilter::eventFilter(QObject *o, QEvent *e)
{
/*
    if (e->type() == QEvent::MouseButtonPress)
    {
        qDebug() << "MapEventFilter: MouseButtonPress";
    }
*/
    Marble::MarbleWidget *m = dynamic_cast<Marble::MarbleWidget*>(o);
    Marble::MarbleWidgetInputHandler *inputHandler = m->inputHandler();

    if (e->type() == QEvent::MouseMove)
    {
        if (m_activationKeyDown)
        {
            e->accept();
            return false;
        }
        else
        {
            QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(e);
            emit mouseMove(mouseEvent->pos());
            return true;
        }
    }
    else if (e->type() == QEvent::KeyPress)
    {
        QKeyEvent* keyEvent = dynamic_cast<QKeyEvent*>(e);
        if (keyEvent->key() == Qt::Key_Shift)
        {
            m_activationKeyDown = true;
            inputHandler->setMouseButtonPopupEnabled(Qt::LeftButton, true);
        }
    }
    else if (e->type() == QEvent::KeyRelease)
    {
        m->setCursor(Qt::ArrowCursor);
        m_activationKeyDown = false;
        inputHandler->setMouseButtonPopupEnabled(Qt::LeftButton, false);
    }

    return false;



}



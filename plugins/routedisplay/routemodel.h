//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//


#ifndef ROUTEMODEL_H
#define ROUTEMODEL_H

#include <QAbstractTableModel>
#include <QTime>
#include <GeoDataLineString.h>
#include <MarbleLocale.h>

class RouteWaypoint;

class RouteModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum SpeedUnit {
        KTS=0,
        MPH,
        KMH
    };
    explicit RouteModel(QObject *parent = 0);

    int rowCount (const QModelIndex & = QModelIndex()) const;
    int columnCount (const QModelIndex & = QModelIndex()) const;
    QModelIndex index ( int row, int column, const QModelIndex & parent = QModelIndex() ) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    Qt::ItemFlags flags (const QModelIndex & index) const;

    void appendRow(RouteWaypoint *waypoint);
    void insertRow(int row, RouteWaypoint *waypoint);
    void clear();
    bool isEmpty();
    bool removeRow(int row, const QModelIndex &parent = QModelIndex() );
    void setPlanetRadius(qreal radius);
    void setSpeed(qreal speed);
    qreal getSpeed();
    Marble::GeoDataLineString getLineString();
    QList<Marble::GeoDataLineString> getLineStringList();
    Marble::MarbleLocale::MeasurementSystem getMeasurementSystem() const;

    QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;
    QVariant headerData (int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    qreal getCourse(int row);
    qreal getDistance(int row);
    RouteWaypoint *getWaypoint(int row);
    bool isRowTotalizer(int row);
    void setRowTotalizer(int row, bool totalizer);
    void toggleRowTotalizer(int row);

    qreal distTotal() const;
    void setDistTotal(const qreal &distTotal);

    QTime timeTotal() const;
    void setTimeTotal(const QTime &timeTotal);

signals:
    void routeChanged();
    void totalsChanged();
    void speedChanged(int);
    void speedUnitChanged(SpeedUnit);
public slots:
    void moveUp(int row);
    void moveDown(int row);
    void waypointMoved(int row);
    void setSpeed(int speed);
    void setSpeedUnit(int speedUnit);
    QStringList getRouteIcaoCodes();


private:
    QList<RouteWaypoint*> m_waypoints;
    Marble::MarbleLocale::MeasurementSystem m_measurementSystem;
    qreal m_planetRadius;
    qreal m_speed;
    SpeedUnit m_speedUnit;
    qreal m_distTotal;
    QTime m_timeTotal;

private slots:
    void calcDistances();


};

#endif // ROUTEMODEL_H

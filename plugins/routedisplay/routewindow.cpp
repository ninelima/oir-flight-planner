//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//

#include "routewindow.h"
#include "ui_routewindow.h"

#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QDebug>
#include <QSettings>
#include <QSortFilterProxyModel>

#include "GeoDataDocument.h"
#include "GeoWriter.h"
#include "geodata/handlers/kml/KmlElementDictionary.h"

#include "GeoDataPlacemark.h"

#include "routemodel.h"
#include "routewaypoint.h"

RouteWindow::RouteWindow(RouteModel *routeModel, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RouteWindow),
    m_pRouteModel(routeModel),
    m_pProxyModel(new QSortFilterProxyModel()),
    m_pAircraftList(0)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Window);
    this->setWindowTitle(tr("Route"));
    m_pProxyModel->setSourceModel(m_pRouteModel);  //allow to filter e.g. waypoints, not yet implemented
    //ui->routeTable->setModel(m_pProxyModel);  --> check RouteTable etc. for access to model via function model() -> won't work with ProxyModel!!
    ui->routeTable->setModel(m_pRouteModel);

    QItemSelectionModel *selectionModel = new QItemSelectionModel(m_pRouteModel);
    ui->routeTable->setSelectionModel(selectionModel);
    ui->routeTable->hideColumn(3); // longitude
    ui->routeTable->hideColumn(4); // latitude
    //ui->routeTable->hideColumn(5);
    ui->routeTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->routeTable->setAlternatingRowColors(true);
    ui->routeTable->resizeColumnsToContents();
    connect(ui->pbExport, SIGNAL(clicked()), this, SLOT(exportRoute()));
    connect(ui->pbImport, SIGNAL(clicked()), this, SLOT(importRoute()));
    connect(ui->pbRemove, SIGNAL(clicked()), this, SLOT(removeItem()));
    connect(ui->pbClear, SIGNAL(clicked()), this, SLOT(clearRoute()));
    connect(ui->pbUp, SIGNAL(clicked()), this, SLOT(moveItemUp()));
    connect(ui->pbDown, SIGNAL(clicked()), this, SLOT(moveItemDown()));
    connect(m_pRouteModel, SIGNAL(routeChanged()), ui->routeTable, SLOT(resizeColumnsToContents()));
    connect(ui->sbSpeed, SIGNAL(valueChanged(int)), m_pRouteModel, SLOT(setSpeed(int)));
    connect(ui->sbSpeed, SIGNAL(valueChanged(int)), this, SIGNAL(speedSettingsChanged()));
    connect(m_pRouteModel, SIGNAL(speedChanged(int)), ui->sbSpeed, SLOT(setValue(int)));
    connect(ui->pbTotalizer, SIGNAL(clicked()), this, SLOT(toggleItemTotalizer()));
    connect(m_pRouteModel, SIGNAL(totalsChanged()), this, SLOT(updateTotals()));
    connect(ui->routeTable, SIGNAL(showAptInfo(int,QString)), this, SIGNAL(showAptInfo(int,QString)));
    connect(ui->routeTable, SIGNAL(gotoWpt(qreal,qreal)), this, SIGNAL(gotoWpt(qreal,qreal)));
    connect(ui->pbRouteNotams, SIGNAL(clicked()), this, SLOT(showRouteNotam()));

    connect(ui->cbSpeedUnit, SIGNAL(currentIndexChanged(int)), m_pRouteModel, SLOT(setSpeedUnit(int)));
    connect(ui->cbSpeedUnit, SIGNAL(currentIndexChanged(int)), this, SIGNAL(speedSettingsChanged()));
    connect(ui->cbAircraft, SIGNAL(currentIndexChanged(int)), this, SLOT(aircraftSelected(int)));

    connect(ui->routeTable, SIGNAL(toggleItemTotalizer()), this, SLOT(toggleItemTotalizer()));
    connect(ui->routeTable, SIGNAL(removeItem()), this, SLOT(removeItem()));
    connect(ui->routeTable, SIGNAL(moveItemUp()), this, SLOT(moveItemUp()));
    connect(ui->routeTable, SIGNAL(moveItemDown()), this, SLOT(moveItemDown()));
    connect(ui->routeTable, SIGNAL(insertItem()), this, SLOT(insertItem()));

    connect(this, SIGNAL(speedSettingsChanged()), this, SLOT(setAicraftUserSpecific()));

    ui->cbSpeedUnit->addItems(Aircraft::speedUnits());


}

RouteWindow::~RouteWindow()
{
    delete ui;
}
QString RouteWindow::impexportFilePath() const
{
    return m_impexportFilePath;
}

void RouteWindow::setImpexportFilePath(const QString &impexportFilePath)
{
    m_impexportFilePath = impexportFilePath;
}

void RouteWindow::setAicraftList(AircraftList *aircraftList)
{
    m_pAircraftList = aircraftList;
    for (int i = 0; i < m_pAircraftList->size(); i++)
    {
        ui->cbAircraft->addItem(m_pAircraftList->at(i)->getImmatriculation());
    }
}



void RouteWindow::exportRouteKML(QString fileName)
{
    QFile file(fileName);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    GeoDataDocument document;
    // set the waypoints in the KML
    for (int i = 0; i < m_pRouteModel->rowCount(); i++)
    {
        RouteWaypoint *waypoint = m_pRouteModel->getWaypoint(i);
        GeoDataPlacemark *placemarkTemp = new Marble::GeoDataPlacemark(waypoint->getName());
        placemarkTemp->setCoordinate(waypoint->getCoordinates());
        placemarkTemp->setId(waypoint->getCode());
        placemarkTemp->setName(waypoint->getName());
        placemarkTemp->setDescription(waypoint->getType());
        document.append(placemarkTemp);
    }

    // set the path in the KML
    GeoDataPlacemark *placemarkTemp2 = new Marble::GeoDataPlacemark("route");
    placemarkTemp2->setId("route");
    GeoDataLineString *lineString = new GeoDataLineString(m_pRouteModel->getLineString());
    placemarkTemp2->setGeometry(lineString);
    document.append(placemarkTemp2);

    GeoWriter writer;
    writer.setDocumentType( kml::kmlTag_nameSpaceOgc22 );
    writer.write(&file, &document);

    file.close();

}

void RouteWindow::exportRoute()
{
    QString selectedFilter;
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save route"), m_impexportFilePath, tr("CSV File (*.csv);; KML File (*.kml)"), &selectedFilter);

    QFileInfo fi(fileName);
    m_impexportFilePath = fi.absolutePath();
    emit settingsChanged();
    if (fi.suffix().isEmpty())
    {
        fileName.append(selectedFilter.right(5).left(4));  // append the suffic from the filter
    }
    fi.setFile(fileName);
    if (fi.suffix().compare("kml", Qt::CaseInsensitive) == 0)
    {
        exportRouteKML(fileName);
    }
    else
    {
        exportRouteCSV(fileName);
    }


}

void RouteWindow::exportRouteCSV(QString fileName)
{

    QFile file(fileName);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    // add header
    out << "code;name;type;longitude;latitude;true course;distance;dist cumul;time;time cumul;break\n";

    RouteModel *model = static_cast<RouteModel*>(ui->routeTable->model());
    for (int i=0; i < model->rowCount(); i++)
    {
        RouteWaypoint *waypoint = model->getWaypoint(i);
        QString name = waypoint->getName();
        if (name.isEmpty())
        {
            name = QString("WPT %1").arg(i+1);  // name it "WPT 'row'"
        }
        out << "\"" << waypoint->getCode() << "\";"
               << "\"" << name << "\";"
               << "\"" << waypoint->getType() << "\";"
               << waypoint->getLon() << ";"
               << waypoint->getLat() << ";"
               << QString::number(waypoint->getCourse(), 'f', 0) << ";"
               << QString::number(waypoint->getDistance(), 'f', 1) << ";"
               << QString::number(waypoint->getDistCumulated(), 'f', 1) << ";"
               << waypoint->getTime().toString("HH:mm") << ";"
               << waypoint->getTimeCumulated().toString("HH:mm") << ";"
               << waypoint->isTotalizer() << "\n";
    }
    out << "Total;Distance;" <<  QString::number(model->distTotal(), 'f', 1) <<";Time;" << model->timeTotal().toString("HH:mm") << "\n";
    file.close();

}

void RouteWindow::importRoute()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open route"), m_impexportFilePath, tr("Text files (*.csv)"));
    QFileInfo fi(fileName);
    m_impexportFilePath = fi.absolutePath();
    emit settingsChanged();
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
//     out << "  Couldn't open the file." << endl << endl << endl;
     return;
    }
    QTextStream in(&file);
    // add header
    //out << "code;name;type;longitude;latitude;distance;true course\n";

   // RouteModel *model = static_cast<RouteModel*>(ui->routeTable->model());
    const QRegExp regExp("^\"|\"$");
    while (!in.atEnd())
    {
        QString line = in.readLine();
        QStringList wpt = line.split(";");
        if (wpt.size() < 5) continue;
        for (int i = 0; i < wpt.size(); i++)
        {
            wpt[i].replace(regExp, "");
        }
        if (wpt.at(0).compare("code") == 0 || wpt.at(0).compare("Total") == 0)
        {
            continue;
        }
        RouteWaypoint *waypoint = new RouteWaypoint;
        waypoint->setCode(wpt.at(0));
        waypoint->setName(wpt.at(1));
        waypoint->setType(wpt.at(2));
        GeoDataCoordinates coord((qreal)(wpt.at(3).toFloat()), (qreal)(wpt.at(4).toFloat()), 0, GeoDataCoordinates::Degree);
        waypoint->setCoordinates(coord);
        if (wpt.size() >= 11)
        {
            waypoint->setTotalizer((bool)(wpt.at(10).toInt()));
        }

        m_pRouteModel->appendRow(waypoint);
    }
    file.close();

}

void RouteWindow::clearRoute()
{
    int ret = QMessageBox::warning(this, tr("Route"),
                                    tr("The whole route will be cleared.\n"
                                       "Do you want to continue?"),
                                    QMessageBox::Yes | QMessageBox::No,
                                    QMessageBox::No);
    if (ret == QMessageBox::Yes)
    {
        m_pRouteModel->clear();
    }
}

void RouteWindow::moveItemUp()
{
    QItemSelectionModel *selectionModel = ui->routeTable->selectionModel();
    QModelIndexList selectedIndex = selectionModel->selectedRows();

    for (int i= selectedIndex.size()-1; i >= 0 ; i--)
    {
        int row = selectedIndex.first().row();
        m_pRouteModel->moveUp(row);
    }

}

void RouteWindow::moveItemDown()
{
    QItemSelectionModel *selectionModel = ui->routeTable->selectionModel();
    QModelIndexList selectedIndex = selectionModel->selectedRows();

    for (int i= selectedIndex.size()-1; i >= 0 ; i--)
    {
        int row = selectedIndex.first().row();
        m_pRouteModel->moveDown(row);
    }
}

void RouteWindow::removeItem()
{
    QItemSelectionModel *selectionModel = ui->routeTable->selectionModel();
    QModelIndexList selectedIndex = selectionModel->selectedRows();

    for (int i= selectedIndex.size()-1; i >= 0 ; i--)
    {
        int row = selectedIndex.first().row();
        m_pRouteModel->removeRow(row);
    }
}

void RouteWindow::insertItem()
{
//    getWaypointInfo routToolPlugin
//    m_pRouteModel->appendRow(new RouteWaypoint);
}

void RouteWindow::toggleItemTotalizer()
{
    QItemSelectionModel *selectionModel = ui->routeTable->selectionModel();
    QModelIndexList selectedIndex = selectionModel->selectedRows();

    for (int i= selectedIndex.size()-1; i >= 0 ; i--)
    {
        int row = selectedIndex.first().row();
        m_pRouteModel->toggleRowTotalizer(row);
    }

}

void RouteWindow::updateTotals()
{
    ui->leTotalDist->setText(QString::number(m_pRouteModel->distTotal(), 'f', 1));
    ui->leTotalTime->setText(m_pRouteModel->timeTotal().toString("HH:mm"));

}

void RouteWindow::showRouteNotam()
{
    emit routeNotamRequested(m_pRouteModel->getRouteIcaoCodes());
}

void RouteWindow::aircraftSelected(int index)
{
    if (index > 0)
    {
        disconnect(this, SIGNAL(speedSettingsChanged()), this, SLOT(setAicraftUserSpecific()));
        ui->sbSpeed->setValue(m_pAircraftList->at(index-1)->getCruiseSpeed()); // index = 0 is "user specific"
        ui->cbSpeedUnit->setCurrentIndex(m_pAircraftList->at(index-1)->getSpeedUnit());
        connect(this, SIGNAL(speedSettingsChanged()), this, SLOT(setAicraftUserSpecific()));
    }
}

void RouteWindow::setAicraftUserSpecific()
{
    ui->cbAircraft->setCurrentIndex(0);
}

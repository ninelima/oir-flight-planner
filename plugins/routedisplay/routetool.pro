CONFIG -= debug_and_release
CONFIG( debug, debug|release )  {
  CONFIG -= release
}
else {
  CONFIG -= debug
  CONFIG += release
}


QT += core widgets sql
TEMPLATE = lib
CONFIG += plugin
TARGET = routetool

include(../../common.pri)


win32 {
#Windows
        DLLDESTDIR = $$DIST_DIR/plugins
} else {
#Linux
        DESTDIR = $$DIST_DIR/plugins
}

INCLUDEPATH += $$MARBLE_INC $$MARBLE_SRC


# Input
HEADERS += RouteToolPlugin.h \
    ../../RouteToolPluginInterface.h \
    routewindow.h \
    routetable.h \
    routemodel.h \
    routewaypoint.h \
    customizableheaderview.h \
    colordisplay.h

FORMS += RouteConfigWidget.ui \
        routewindow.ui
SOURCES += RouteToolPlugin.cpp \
    routewindow.cpp \
    routetable.cpp \
    routemodel.cpp \
    routewaypoint.cpp \
    customizableheaderview.cpp \
    colordisplay.cpp

RESOURCES += routetool.qrc \
    ../../data/resources.qrc


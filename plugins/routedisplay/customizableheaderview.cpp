//
// Copyright (C) 2008-2010  Lukas Sommer < SommerLuk at gmail dot com >
// Copyright 2016 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "customizableheaderview.h"
#include <QMenu>
#include <QPointer>
#include <QContextMenuEvent>
#include <QAction>

CustomizableHeaderView::CustomizableHeaderView(Qt::Orientation orientation, QWidget * parent)
  : QHeaderView(orientation, parent)
{
}

CustomizableHeaderView::~CustomizableHeaderView()
{
}


void CustomizableHeaderView::contextMenuEvent(QContextMenuEvent * e)
{
  int sectionCount;
  int i;
  QPointer<QAction> temp_action;
  QPointer<QAction> activated_action;
  bool isOkay;
  int index;

  QMenu m_contextMenu(this);

  // The menu is contructed each time again, because the numer of columns/rows in the model
  // could have changed since the last call.

  if (orientation() == Qt::Horizontal) {
    sectionCount = model()->columnCount();
  } else {
    sectionCount = model()->rowCount();
  };
  for (i=0; i < sectionCount; ++i) {
    temp_action = new QAction(model()->headerData(i,
                                                    orientation(),
                                                    Qt::DisplayRole).toString(),
                               &m_contextMenu);  // because m_contextMenu is parent of the
                                                  // actions, all of them will be deleted
                                                  // automatically when this function stops.
    temp_action->setCheckable(true);
    temp_action->setChecked(! isSectionHidden(i));
    temp_action->setData(i);
    m_contextMenu.addAction(temp_action);
  };
  activated_action = 0;
  activated_action = m_contextMenu.exec(e->globalPos());
  // process QAction after closing menu
  if (! (activated_action == 0)) {
      isOkay = 1;  // following the doc for QVariant, the return type of QAction::data(),
      // isOkay must be non-null for that an value is set.
      index = activated_action->data().toInt(&isOkay);
      if (isOkay) {
        if (isSectionHidden(index)) {
          emit sectionAboutToBeShown(index, orientation());
          setSectionHidden(index, false);
          emit sectionShown(index, orientation());
        } else {
          emit sectionAboutToBeHidden(index, orientation());
          setSectionHidden(index, true);
          emit sectionHidden(index, orientation());
        };
      };
  };
}

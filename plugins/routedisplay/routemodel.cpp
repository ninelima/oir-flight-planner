//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 47 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:02:47 +0000 (Wed, 07 Nov 2018) $
//

#include "routemodel.h"

#include <QBrush>
#include <QColor>

#include "routewaypoint.h"



using namespace Marble;

RouteModel::RouteModel(QObject *parent) :
    QAbstractTableModel(parent),
    m_waypoints(QList<RouteWaypoint*>()),
    m_planetRadius(6371000),
    m_speed(100),
    m_speedUnit(KTS),
    m_distTotal(0),
    m_timeTotal(QTime(0,0))
{
    connect(this, SIGNAL(routeChanged()), this, SLOT(calcDistances()));
}

int RouteModel::rowCount(const QModelIndex & /* parent */) const
{
    return m_waypoints.size();
}

int RouteModel::columnCount(const QModelIndex & /* parent */) const
{
    return 12;
}

QModelIndex RouteModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return createIndex(row, column);
}

bool RouteModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(role)
    if (index.column() == 1)
    {
        m_waypoints[index.row()]->setName(value.toString());
        return true;
    }
    return false;
}

Qt::ItemFlags RouteModel::flags(const QModelIndex &index) const
{
    if (index.column() == 1)
    {
        if (m_waypoints.at(index.row())->getType().isEmpty())  // item ist editable in column 1 and for user waypoints only
        {
            return Qt::ItemIsEditable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        }
    }
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

void RouteModel::appendRow(RouteWaypoint *waypoint)
{
    beginInsertRows(QModelIndex(), m_waypoints.size(), m_waypoints.size());
    m_waypoints << waypoint; 
    endInsertRows();

    emit routeChanged();
}

void RouteModel::insertRow(int row, RouteWaypoint *waypoint)
{
    beginInsertRows(QModelIndex(), row, row);
    m_waypoints.insert(row, waypoint);
    endInsertRows();

    emit routeChanged();
}

void RouteModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, m_waypoints.size()-1);
    for (int i = 0; i < m_waypoints.size(); i++)
    {
        delete m_waypoints.at(i);
    }
    m_waypoints.clear();
    endRemoveRows();
    emit routeChanged();
}

bool RouteModel::isEmpty()
{
    return m_waypoints.isEmpty();
}

bool RouteModel::removeRow(int row, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginRemoveRows(QModelIndex(), row, row);
    delete m_waypoints.at(row);
    m_waypoints.removeAt(row);
    endRemoveRows();
    emit routeChanged();
    return true;
}

void RouteModel::setPlanetRadius(qreal radius)
{
    m_planetRadius = radius;
}

void RouteModel::setSpeed(qreal speed)
{
    if (speed > 0)  // avoid division by zero
    {
        m_speed = speed;
        calcDistances();

        emit dataChanged(index(8,0), index(9,rowCount()-1));
        emit speedChanged((int)m_speed);
    }
}

qreal RouteModel::getSpeed()
{
    return m_speed;
}

GeoDataLineString RouteModel::getLineString()
{
    GeoDataLineString lineString( Tessellate);
    for (int i = 0; i < m_waypoints.size(); i++)
    {
        lineString << m_waypoints.at(i)->getCoordinates();
    }
    return lineString;
}

QList<GeoDataLineString> RouteModel::getLineStringList()
{
    QList<GeoDataLineString> lineStringList;
    GeoDataLineString lineString(Tessellate);

    for (int i = 0; i < m_waypoints.size(); i++)
    {
        lineString << m_waypoints.at(i)->getCoordinates();
        if (m_waypoints.at(i)->isTotalizer())
        {
          lineStringList << lineString;
          lineString.clear();
          lineString << m_waypoints.at(i)->getCoordinates();  // set new starting point
        }
        else if (i == m_waypoints.size() - 1)
        {
          lineStringList << lineString;
        }

    }

    return lineStringList;
}

MarbleLocale::MeasurementSystem RouteModel::getMeasurementSystem() const
{
    return m_measurementSystem;
}

QVariant RouteModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        QString name;
        switch (index.column())
        {
        case 0:
            return m_waypoints.at(index.row())->getCode();
            break;
        case 1:
            name = m_waypoints.at(index.row())->getName();
            if (name.isEmpty())
            {
                return QString("WPT %1").arg(index.row()+1);  // name it "WPT 'row'"
            }
            else
            {
                return name;
            }
            break;
        case 2:
            name = m_waypoints.at(index.row())->getSubtype();
            if (name.isEmpty())
            {
                return tr("USER");
            }
            else
            {
                return name;
            }
            break;
        case 3:
            return m_waypoints.at(index.row())->getLon();
            break;
        case 4:
            return m_waypoints.at(index.row())->getLat();
            break;
        case 5:
            return QString::number(m_waypoints.at(index.row())->getCourse(), 'f', 0);
            break;
        case 6:
            return QString::number(m_waypoints.at(index.row())->getDistance(),'f', 1);
            break;
        case 7:
            return QString::number(m_waypoints.at(index.row())->getDistCumulated(),'f', 1);
            break;
        case 8:
            return QString::number(m_waypoints.at(index.row())->getDistCumulatedRoute(),'f', 1);
            break;
        case 9:
            return m_waypoints.at(index.row())->getTime().toString("HH:mm:ss");
            break;
        case 10:
            return m_waypoints.at(index.row())->getTimeCumulated().toString("HH:mm:ss");
            break;
        case 11:
            return m_waypoints.at(index.row())->getTimeCumulatedRoute().toString("HH:mm:ss");
            break;
        default:
            return QVariant();
        }

    }
    else if (role == Qt::BackgroundRole)
    {
        if (m_waypoints.at(index.row())->isTotalizer())
        {
            return QBrush(QColor(Qt::blue));
        }
    }

    return QVariant();
}

QVariant RouteModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal)
        {
            switch (section)
            {
            case 0:
                return tr("Code");
                break;
            case 1:
                return tr("Name");
                break;
            case 2:
                return tr("Type");
                break;
            case 3:
                return tr("Lon");
                break;
            case 4:
                return tr("Lat");
                break;
            case 5:
                return tr("TC");
                break;
            case 6:
                return tr("Dist");
                break;
            case 7:
                return tr("Dist(cum.)");
                break;
            case 8:
                return tr("Dist(t)");
                break;
            case 9:
                return tr("Time");
                break;
            case 10:
                return tr("Time(cum.)");
                break;
            case 11:
                return tr("Time(t)");
                break;
            default:
                return QVariant();
            }
        }
        else if (orientation == Qt::Vertical)
        {
            return QString::number(section+1);
        }
    }

    return QVariant();
}

qreal RouteModel::getCourse(int row)
{
    return m_waypoints.at(row)->getCourse();
}

qreal RouteModel::getDistance(int row)
{
    return m_waypoints.at(row)->getDistance();
}

RouteWaypoint *RouteModel::getWaypoint(int row)
{
    return m_waypoints.at(row);
}

bool RouteModel::isRowTotalizer(int row)
{
    return m_waypoints.at(row)->isTotalizer();
}

void RouteModel::setRowTotalizer(int row, bool totalizer)
{
    m_waypoints.at(row)->setTotalizer(totalizer);
    emit routeChanged();  // recalculates the times and distances
    emit dataChanged(createIndex(row, 0), createIndex(rowCount()-1, columnCount()-1), QVector<int>(Qt::DisplayRole));  // repaints the table
}

void RouteModel::toggleRowTotalizer(int row)
{
    setRowTotalizer(row, !isRowTotalizer(row));
}

void RouteModel::moveUp(int row)
{

    if (row > 0)
    {
        beginMoveRows(QModelIndex(), row, row, QModelIndex(), row - 1);
        m_waypoints.move(row, row-1);
        endMoveRows();
    }
    emit routeChanged();
}

void RouteModel::moveDown(int row)
{
    if (row < m_waypoints.size()-1)
    {
        beginMoveRows(QModelIndex(), row, row, QModelIndex(), row + 2);
        m_waypoints.move(row, row+1);
        endMoveRows();
    }
    emit routeChanged();
}

void RouteModel::waypointMoved(int row)
{
    emit dataChanged(createIndex(row, 0), createIndex(row, columnCount()-1), QVector<int>(Qt::DisplayRole));
    emit routeChanged();
}

void RouteModel::setSpeed(int speed)
{
    setSpeed(static_cast<qreal> (speed));
}

void RouteModel::setSpeedUnit(int speedUnit)
{
    m_speedUnit = static_cast<SpeedUnit> (speedUnit);
    calcDistances();

    emit dataChanged(index(8,0), index(9,rowCount()-1));
    emit speedUnitChanged(m_speedUnit);

}

QStringList RouteModel::getRouteIcaoCodes()
{
    QStringList icaoCodes;
    icaoCodes.append(m_waypoints.first()->getCode());
    for (int i = 1; i < m_waypoints.size()-1; i++)
    {
        if (m_waypoints.at(i)->isTotalizer())
        {
            icaoCodes.append(m_waypoints.at(i)->getCode());
        }
    }
    icaoCodes.append(m_waypoints.last()->getCode());
    return icaoCodes;
}

QTime RouteModel::timeTotal() const
{
    return m_timeTotal;
}

void RouteModel::setTimeTotal(const QTime &timeTotal)
{
    m_timeTotal = timeTotal;
}

qreal RouteModel::distTotal() const
{
    return m_distTotal;
}

void RouteModel::setDistTotal(const qreal &distTotal)
{
    m_distTotal = distTotal;
}



void RouteModel::calcDistances()
{
    qreal distCumulated = 0;  // day counter
    QTime timeCumulated(0,0); // day counter
    // reset totals
    m_distTotal = 0;
    m_timeTotal = QTime(0,0);
    // reset values in first waypoint
    if (!m_waypoints.isEmpty())
    {
        m_waypoints.at(0)->setCourse(0);
        m_waypoints.at(0)->setDistance(0);
        m_waypoints.at(0)->setDistCumulated(0);
        m_waypoints.at(0)->setDistCumulatedRoute(0);
        m_waypoints.at(0)->setTime(QTime(0,0));
        m_waypoints.at(0)->setTimeCumulated(QTime(0,0));
        m_waypoints.at(0)->setTimeCumulatedRoute(QTime(0,0));
    }

    for ( int segmentIndex = 0; segmentIndex < m_waypoints.size() - 1; ++segmentIndex ) {
        GeoDataLineString segment( Tessellate );
        segment << m_waypoints.at(segmentIndex)->getCoordinates();
        segment << m_waypoints.at(segmentIndex+1)->getCoordinates();


        m_measurementSystem = Marble::MarbleGlobal::getInstance()->locale()->measurementSystem();

        qreal distance = segment.length(m_planetRadius);

        if ( m_measurementSystem == Marble::MarbleLocale::MetricSystem )
        {
            distance = distance / 1000.0;
        }
        else if (m_measurementSystem == Marble::MarbleLocale::ImperialSystem)
        {
            distance = distance / 1000.0 * Marble::KM2MI;
        }
        else if (m_measurementSystem == Marble::MarbleLocale::NauticalSystem)
        {
            distance = distance / 1000.0 * Marble::KM2NM;
        }
        distCumulated += distance;
        m_distTotal += distance;
        m_waypoints.at(segmentIndex+1)->setDistance(distance);
        m_waypoints.at(segmentIndex+1)->setDistCumulated(distCumulated);
        m_waypoints.at(segmentIndex+1)->setDistCumulatedRoute(m_distTotal);

        int secs = 0;
        switch (m_speedUnit)
        {
        case KTS:
            // segment.length is in m
            secs = segment.length(m_planetRadius) * Marble::KM2NM / m_speed * 3.6;
            break;
        case MPH:
            secs = segment.length(m_planetRadius) * Marble::KM2MI / m_speed * 3.6;
            break;
        case KMH:
            secs = segment.length(m_planetRadius)/m_speed*3.6;
            break;
        }


        QTime time = QTime(0,0).addSecs(secs);
        timeCumulated = QTime(timeCumulated.hour(), timeCumulated.minute()).addSecs(secs);
        m_timeTotal = QTime(m_timeTotal.hour(), m_timeTotal.minute()).addSecs(secs);
        m_waypoints.at(segmentIndex+1)->setTime(time);
        m_waypoints.at(segmentIndex+1)->setTimeCumulated(timeCumulated);
        m_waypoints.at(segmentIndex+1)->setTimeCumulatedRoute(m_timeTotal);
        if (m_waypoints.at(segmentIndex+1)->isTotalizer())
        {
            distCumulated = 0;
            timeCumulated = QTime(0,0);
        }


        GeoDataCoordinates coordinates = segment.first();
        qreal bearing = coordinates.bearing( segment.last(), GeoDataCoordinates::Degree );
        if ( bearing < 0 ) {
            bearing += 360;
        }
        m_waypoints.at(segmentIndex+1)->setCourse(bearing);
    }
    emit totalsChanged();
}


#ifndef WAYPOINTLAYER_H
#define WAYPOINTLAYER_H

#include "marble/LayerInterface.h"
#include <QFont>
#include <QPen>

class Waypoint;
class RouteModel;

namespace Marble
{
class MarbleWidget;
class GeoDataDocument;
class GeoDataTreeModel;
class GeoLineStringGraphicsItem;


class WaypointLayer : public QObject, public LayerInterface
{
    Q_OBJECT
public:
    explicit WaypointLayer(MarbleWidget *widget, QWidget *parent = 0);
    ~WaypointLayer();
    QStringList renderPosition() const;
    qreal zValue() const;
    bool render( GeoPainter *painter, ViewportParams *viewport, const QString &renderPos = "NONE", GeoSceneLayer *layer = 0 );
    RenderState renderState() const;
//    void setAirports(GeoDataDocument *document);
    void setRouteModel(RouteModel *model);

signals:

public slots:
    bool  eventFilter(QObject *o, QEvent *e );


private:
    void  drawMeasurePoints( GeoPainter *painter ) const;
    void  drawTotalDistanceLabel( GeoPainter *painter,
                                  qreal totalDistance ) const;
    void  drawSegments( GeoPainter *painter );

//    GeoDataDocument *m_pAirports;
    QList<Waypoint*> *m_pNavaids;
//    GeoDataTreeModel *m_pTreeModel;
    RouteModel *m_routeModel;
    MarbleWidget *m_marbleWidget;
    const QPixmap m_mark;
    QFont   m_font_regular;
    int     m_fontascent;
    QPen    m_pen;
    bool m_showDistanceLabel;
    bool m_showBearingLabel;
    bool m_mouseButtonDown;
    GeoLineStringGraphicsItem *gi;
};
} // namespace Marble

#endif // WAYPOINTLAYER_H

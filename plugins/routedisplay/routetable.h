//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#ifndef ROUTETABLE_H
#define ROUTETABLE_H

#include <QTableView>
#include <QList>

class QAction;
class CustomizableHeaderView;


class RouteTable : public QTableView
{
    Q_OBJECT
public:
    explicit RouteTable(QWidget *parent = 0);
signals:

public slots:

protected:
    void keyPressEvent(QKeyEvent * event);
    void contextMenuEvent(QContextMenuEvent *event);

protected:

    QList<QAction *> m_contextMenuActions;
    QModelIndex mClickedIndex;
    QAction *m_pActionGotoWpt;
    QAction *m_pActionToggleItemTotalizer;
    QAction *m_pActionRemoveWaypoint;
    QAction *m_pActionMoveUp;
    QAction *m_pActionMoveDown;
    QAction *m_pActionInsert;
    CustomizableHeaderView *m_pHeaderView;
private slots:
    void showInfoClicked();
    void gotoWptClicked();

signals:
    void showAptInfo(int index, QString code);
    void gotoWpt(qreal lon, qreal lat);
    void toggleItemTotalizer();
    void removeItem();
    void moveItemUp();
    void moveItemDown();
    void insertItem();

};

#endif // ROUTETABLE_H

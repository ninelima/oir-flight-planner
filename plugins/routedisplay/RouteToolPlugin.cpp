//
// Copyright 2015-2019 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//

#include "RouteToolPlugin.h"
#include "ui_RouteConfigWidget.h"

#include "routewindow.h"

#include <GeoPainter.h>
#include <MarbleDebug.h>
#include <MarbleMath.h>
#include <MarbleModel.h>
#include <MarbleLocale.h>
#include <Planet.h>
#include <GeoDataPlacemark.h>
#include <ElevationModel.h>

#include <QColor>
#include <QPen>
#include <QPixmap>
#include <QPushButton>
#include <QCheckBox>
#include <QtSql/QSqlQuery>
#include <QModelIndex>
#include <QMouseEvent>
#include <QObject>
#include <QMenu>
#include <QAction>
#include <QColorDialog>

#include <QDebug>

#include "routemodel.h"
#include "routewaypoint.h"

namespace Marble
{

RouteToolPlugin::RouteToolPlugin( const MarbleModel *marbleModel )
    : RenderPlugin( marbleModel ),
      m_mark( ":/mark.png" ),
#ifdef Q_OS_MACX
      m_font_regular( QFont( "Sans Serif", 10, 50, false ) ),
#else
      m_font_regular( QFont( "Sans Serif",  8, 50, false ) ),
#endif
      m_fontascent( QFontMetrics( m_font_regular ).ascent() ),
      m_pen( Qt::red ),
      m_pAddMeasurePointAction( 0 ),
      m_pShowRouteWindowAction(0),
      m_pSeparator( 0 ),
      m_pWidget(0),
      m_pMap(0),
      m_pMarbleModel(marbleModel),
      m_pConfigDialog( 0 ),
      m_pUiConfigWidget( 0 ),
      m_pRouteModel(new RouteModel()),
      m_pRouteWindow(new RouteWindow(m_pRouteModel)),
      m_showDistanceLabel( true ),
      m_showBearingLabel( true ),
      m_iSelectedWaypoint(-1),
      m_actionList(QList<QAction *>())

{
    m_pen.setWidthF( 2.0 );
    this->w = this->newModule();
    connect(m_pRouteModel, SIGNAL(routeChanged()), this, SIGNAL(repaintNeeded()));
    connect(m_pRouteWindow, SIGNAL(settingsChanged()), this, SLOT(writePathSetting()));
    connect(m_pRouteWindow, SIGNAL(showAptInfo(int,QString)), this->w, SIGNAL(showAptInfo(int,QString)));
    connect(m_pRouteWindow, SIGNAL(gotoWpt(qreal,qreal)), this->w, SIGNAL(gotoWpt(qreal,qreal)));
    connect(this->w, SIGNAL(mouseMove(QPoint)), this, SLOT(moveWaypoint(QPoint)));
    connect(this->w, SIGNAL(posClicked(int,int)), this, SLOT(setPosClicked(int,int)));
    connect(this, SIGNAL(mouseMoved(QPoint)), this, SLOT(moveWaypoint(QPoint)));
    connect(m_pRouteWindow, SIGNAL(routeNotamRequested(QStringList)), this->w, SIGNAL(routeNotamsRequested(QStringList)));
}

RouteToolPlugin::~RouteToolPlugin()
{
    delete m_pRouteModel;
    delete m_pRouteWindow;
}

QStringList RouteToolPlugin::backendTypes() const
{
    return QStringList( "routetool" );
}

QString RouteToolPlugin::renderPolicy() const
{
    return QString( "ALWAYS" );
}

QStringList RouteToolPlugin::renderPosition() const
{
    return QStringList() << "USER_TOOLS";
}

QString RouteToolPlugin::name() const
{
    return tr( "Route Tool" );
}

QString RouteToolPlugin::guiString() const
{
    return tr( "&Route Tool" );
}

QString RouteToolPlugin::nameId() const
{
    return QString( "route-tool" );
}

QString RouteToolPlugin::version() const
{
    return "1.2";
}

QString RouteToolPlugin::description() const
{
    return tr( "Route between different points." );
}

QString RouteToolPlugin::copyrightYears() const
{
    return "2015-2016";
}

QVector<PluginAuthor> RouteToolPlugin::pluginAuthors() const
{
    return QVector<PluginAuthor>()
            << PluginAuthor( "Pascal Tritten", "ptritten@gmail.com" );
}

QIcon RouteToolPlugin::icon () const
{
    return QIcon(":/icons/measure.png");
}

void RouteToolPlugin::initialize ()
{
}

bool RouteToolPlugin::isInitialized () const
{
    return true;
}

QDialog *RouteToolPlugin::configDialog()
{
    if ( !m_pConfigDialog ) {
        m_pConfigDialog = new QDialog();
        m_pUiConfigWidget = new Ui::RouteConfigWidget;
        m_pUiConfigWidget->setupUi( m_pConfigDialog );
        connect( m_pUiConfigWidget->pbLineColor1, SIGNAL(clicked()), this, SLOT(changeLineColor1()));
        connect( m_pUiConfigWidget->pbLineColor2, SIGNAL(clicked()), this, SLOT(changeLineColor2()));
        connect( m_pUiConfigWidget->m_buttonBox, SIGNAL(accepted()),
                SLOT(writeSettings()) );
        connect(m_pUiConfigWidget->m_buttonBox, SIGNAL(accepted()), m_pConfigDialog, SLOT(close()));
        connect(m_pUiConfigWidget->m_buttonBox, SIGNAL(rejected()), m_pConfigDialog, SLOT(close()));

        QPushButton *applyButton = m_pUiConfigWidget->m_buttonBox->button( QDialogButtonBox::Apply );
        connect( applyButton, SIGNAL(clicked()),
                 this,        SLOT(writeSettings()) );
    }

    m_pUiConfigWidget->m_showDistanceLabelsCheckBox->setChecked( m_showDistanceLabel );
    m_pUiConfigWidget->m_showBearingLabelsCheckBox->setChecked( m_showBearingLabel );
    m_pUiConfigWidget->lblLineColor1->setText(m_lineColor1.name());
    m_pUiConfigWidget->lblLineColor2->setText(m_lineColor2.name());
    m_pUiConfigWidget->widLineColor1->setColor(m_lineColor1);
    m_pUiConfigWidget->widLineColor2->setColor(m_lineColor2);
    m_pUiConfigWidget->sbRoutingSpeed->setValue((int)(m_pRouteModel->getSpeed()));
    m_pUiConfigWidget->sbLineWidth->setValue((int)m_lineWidth);
    return m_pConfigDialog;
}

QHash<QString,QVariant> RouteToolPlugin::settings() const
{
    QHash<QString, QVariant> settings = RenderPlugin::settings();

    settings.insert( "showDistanceLabel", m_showDistanceLabel );
    settings.insert( "showBearingLabel", m_showBearingLabel );
    settings.insert( "lineColor1", m_lineColor1);
    settings.insert( "lineColor2", m_lineColor2);
    settings.insert("lineWidth", m_lineWidth);
    settings.insert( "routingSpeed", m_pRouteModel->getSpeed());
    settings.insert( "impexpFilePath", m_pRouteWindow->impexportFilePath());

    return settings;
}

void RouteToolPlugin::setSettings( const QHash<QString,QVariant> &settings )
{
    RenderPlugin::setSettings( settings );

    m_showDistanceLabel = settings.value( "showDistanceLabel", true ).toBool();
    m_showBearingLabel = settings.value( "showBearingLabel", true ).toBool();
    m_lineColor1 = settings.value("lineColor1", QColor(Qt::red)).value<QColor>();
    m_lineColor2 = settings.value("lineColor2", QColor(Qt::green)).value<QColor>();
    m_lineWidth = settings.value("lineWidth", 2).toReal();
    m_pRouteModel->setSpeed(settings.value("routingSpeed", 100).toFloat());
    m_pRouteWindow->setImpexportFilePath(settings.value("impexpFilePath", "").toString());
}


void RouteToolPlugin::addWaypoint(qreal lon, qreal lat, QString code, QString type)
{
    RouteWaypoint *waypoint = new RouteWaypoint;
    waypoint->setCoordinates(GeoDataCoordinates(lon,lat,0, GeoDataCoordinates::Degree));
    getWaypointInfo(waypoint, code, type);
    m_pRouteModel->appendRow(waypoint);

    showRouteWindow();
    emit numberOfMeasurePointsChanged( m_pRouteModel->rowCount() );
}

void RouteToolPlugin::closePopup()
{
    if (m_pRouteWindow) m_pRouteWindow->close();
}

void RouteToolPlugin::setMap(MarbleMap *map)
{
    m_pMap = map;
    m_pRouteModel->setPlanetRadius(m_pMap->model()->planet()->radius());

}

void RouteToolPlugin::setACList(AircraftList *aircraftList)
{
    m_pRouteWindow->setAicraftList(aircraftList);
}

QList<QAction *> RouteToolPlugin::getActions()
{
    return m_actionList;
}


RouteToolPluginWorker *RouteToolPlugin::newModule()
{
    return new RouteToolPluginWorker;
}


void RouteToolPlugin::writeSettings()
{
    m_showDistanceLabel = m_pUiConfigWidget->m_showDistanceLabelsCheckBox->isChecked();
    m_showBearingLabel = m_pUiConfigWidget->m_showBearingLabelsCheckBox->isChecked();
    m_lineWidth = m_pUiConfigWidget->sbLineWidth->value();
    m_pRouteModel->setSpeed((qreal)(m_pUiConfigWidget->sbRoutingSpeed->value()));

    emit settingsChanged( nameId() );
    emit repaintNeeded();
}

void RouteToolPlugin::writePathSetting()
{
    emit settingsChanged( nameId() );
}

bool RouteToolPlugin::render( GeoPainter *painter,
                          ViewportParams *viewport,
                          const QString& renderPos,
                          GeoSceneLayer * layer )
{

    Q_UNUSED(viewport)
    Q_UNUSED(renderPos)
    Q_UNUSED(layer)

    // No way to paint anything if the list is empty.
    if (m_pRouteModel->isEmpty())
        return true;

    painter->save();

    // Prepare for painting the measure line string and paint it.
    m_pen.setColor(m_lineColor1);
    painter->setPen( m_pen );

//    if ( m_showDistanceLabel || m_showBearingLabel) {
        drawSegments( painter );
//    } else {
//        painter->drawPolyline( m_pRouteModel->getLineString());
//    }

    // Paint the nodes of the paths.
    drawMeasurePoints( painter );

    // Paint the total distance in the upper left corner.
    qreal totalDistance = m_pRouteModel->getLineString().length( marbleModel()->planet()->radius() );

    if ( m_pRouteModel->rowCount() > 1 )
        drawTotalDistanceLabel( painter, totalDistance );

    painter->restore();

    return true;
}

void RouteToolPlugin::drawSegments( GeoPainter* painter )
{
    int waypointIndex = 0;
    MarbleLocale::MeasurementSystem measurementSystem = m_pRouteModel->getMeasurementSystem();
    QList<GeoDataLineString> lineStringList = m_pRouteModel->getLineStringList();

    for (int stringListIndex = 0; stringListIndex < lineStringList.size(); stringListIndex++)
    {
        GeoDataLineString measureLineString = lineStringList.at(stringListIndex);
        for ( int segmentIndex = 0; segmentIndex < measureLineString.size() - 1; ++segmentIndex ) {
            waypointIndex++;
            GeoDataLineString segment( Tessellate );
            segment << measureLineString[segmentIndex] ;
            segment << measureLineString[segmentIndex + 1];

            QPen shadowPen( Oxygen::aluminumGray5 );
            shadowPen.setWidthF(m_lineWidth+2.0);
            painter->setPen( shadowPen );
            painter->drawPolyline( segment );

            m_pRouteModel->getWaypoint(waypointIndex)->setConnectionRegion(painter->regionFromPolyline(segment));
            QString infoString;

            if ( m_showDistanceLabel ) {
                qreal segmentLength = m_pRouteModel->getDistance(waypointIndex);

                if ( measurementSystem == MarbleLocale::MetricSystem ) {
                        infoString = tr("%1 km").arg( segmentLength, 0, 'f', 2 );
                } else if (measurementSystem == MarbleLocale::ImperialSystem) {
                    infoString = QString("%1 mi").arg( segmentLength, 0, 'f', 2 );
                } else if (measurementSystem == MarbleLocale::NauticalSystem) {
                    infoString = QString("%1 nm").arg( segmentLength, 0, 'f', 2 );
                }
            }

            if ( m_showBearingLabel ) {
                qreal bearing = m_pRouteModel->getCourse(waypointIndex);
                QString bearingString = QString::fromUtf8( "%1°" ).arg( bearing, 0, 'f', 0 );
                if ( !infoString.isEmpty() ) {
                    infoString.append( "\n" );
                }
                infoString.append( bearingString );
            }

            QPen linePen;

            if (stringListIndex % 2)
            {
                linePen.setColor( m_lineColor2 );
            }
            else
            {
                linePen.setColor( m_lineColor1 );
            }
            linePen.setWidthF(m_lineWidth);
            painter->setPen(linePen);
            if ( !infoString.isEmpty() ) {
                painter->drawPolyline( segment, infoString, LineCenter );
            }
            else
            {
                painter->drawPolyline( segment );
            }
        }
    }
}

void RouteToolPlugin::drawMeasurePoints( GeoPainter *painter ) const
{
        for (int i = 0; i < m_pRouteModel->rowCount(); i++)
        {
            painter->drawPixmap(m_pRouteModel->getWaypoint(i)->getCoordinates(), m_mark);
            m_pRouteModel->getWaypoint(i)->setRegion(painter->regionFromPoint(m_pRouteModel->getWaypoint(i)->getCoordinates(), 10));
        }
}

void RouteToolPlugin::drawTotalDistanceLabel( GeoPainter *painter,
                                          qreal totalDistance ) const
{
    QString  distanceString;

    MarbleLocale::MeasurementSystem measurementSystem;
    measurementSystem = MarbleGlobal::getInstance()->locale()->measurementSystem();

    if ( measurementSystem == MarbleLocale::MetricSystem ) {
        if ( totalDistance >= 1000.0 ) {
            distanceString = tr("Total Distance: %1 km").arg( totalDistance/1000.0 );
        }
        else {
            distanceString = tr("Total Distance: %1 m").arg( totalDistance );
        }
    }
    else if (measurementSystem == MarbleLocale::ImperialSystem) {
        distanceString = QString("Total Distance: %1 mi").arg( totalDistance/1000.0 * KM2MI );
    } else if (measurementSystem == MarbleLocale::NauticalSystem) {
        distanceString = QString("Total Distance: %1 nm").arg( totalDistance/1000.0 * KM2NM );
    }

    painter->setPen( QColor( Qt::black ) );
    painter->setBrush( QColor( 192, 192, 192, 192 ) );

    painter->drawRect( 10, 105, 10 + QFontMetrics( m_font_regular ).boundingRect( distanceString ).width() + 5, 10 + m_fontascent + 2 );
    painter->setFont( m_font_regular );
    painter->drawText( 15, 110 + m_fontascent, distanceString );
}


 /* addMeasurePoint used after right clicking into the map and choosing "add waypoint" */
void RouteToolPlugin::addMeasurePoint(QPoint mousePos)
{
    qreal  lat;
    qreal  lon;

    m_pMap->geoCoordinates(mousePos.x(), mousePos.y(), lon, lat, GeoDataCoordinates::Radian);
    RouteWaypoint *waypoint = new RouteWaypoint;
    waypoint->setCoordinates(GeoDataCoordinates(lon,lat));

    qDebug() << "Altitude" << m_pMarbleModel->elevationModel()->height(lon, lat);

    showRouteWindow();

    QVector<const GeoDataFeature *> placeMark = m_pMap->whichFeatureAt(mousePos);
    if (placeMark.empty())
    {
        m_pRouteModel->appendRow(waypoint);
        return;  // no placemark at this position
    }

    waypoint->setCoordinates(dynamic_cast<const Marble::GeoDataPlacemark*>(placeMark.first())->coordinate());
    getWaypointInfo(waypoint, placeMark.first()->name(), placeMark.first()->description(), dynamic_cast<const Marble::GeoDataPlacemark*>(placeMark.first())->countryCode());
    m_pRouteModel->appendRow(waypoint);
    emit numberOfMeasurePointsChanged( m_pRouteModel->rowCount() );
}

/* insertMeasurePoint used when clicking anywhere onto the existing route line */
void RouteToolPlugin::insertMeasurePoint(int pos, QPoint mousePos)
{
    qreal  lat;
    qreal  lon;
    m_pMap->geoCoordinates( mousePos.x(), mousePos.y(), lon, lat, GeoDataCoordinates::Radian );

    RouteWaypoint *waypoint = new RouteWaypoint;
    waypoint->setCoordinates(GeoDataCoordinates( lon, lat ));
    showRouteWindow();
    QVector<const GeoDataFeature *> placeMark = m_pMap->whichFeatureAt(mousePos);
    if (placeMark.empty())
    {
        m_pRouteModel->insertRow(pos, waypoint);
        return;  // no placemark at this position
    }

    waypoint->setCoordinates(dynamic_cast<const Marble::GeoDataPlacemark*>(placeMark.first())->coordinate());
    getWaypointInfo(waypoint, placeMark.first()->name(), placeMark.first()->description(), dynamic_cast<const Marble::GeoDataPlacemark*>(placeMark.first())->countryCode());
    qDebug() << "insertRow:" << pos << waypoint->getName();
    m_pRouteModel->insertRow(pos, waypoint);

    emit numberOfMeasurePointsChanged( m_pRouteModel->rowCount() );
}


void RouteToolPlugin::getWaypointInfo(RouteWaypoint *waypoint, QString code, QString type, QString country)
{
    QString sQuery;
    if (type == "A")
    {
        sQuery = QString("SELECT icao, name, type AS subtype, country FROM airports WHERE (icao ='%1' OR name='%1') AND country LIKE '%2' LIMIT 1;").arg(code,country);
    }
    else if (type == "F")
    {
        sQuery = QString("SELECT ident, ident, '5LNC' AS subtype, country FROM icao5lnc WHERE ident ='%1' AND country LIKE '%2' LIMIT 1;").arg(code,country);
    }
    else if (type == "N")
    {
        sQuery = QString("SELECT ident, name, type AS subtype, country FROM navaids WHERE ident ='%1' AND country LIKE '%2' LIMIT 1;").arg(code,country);
    }

    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    while (query.next())
    {
        waypoint->setCode(query.value(0).toString());
        waypoint->setName(query.value(1).toString());
        waypoint->setType(type);
        waypoint->setSubtype(query.value(2).toString());  // subtype like VOR, DME, NDB...
    }


}



void RouteToolPlugin::removeLastMeasurePoint()
{
    if (!m_pRouteModel->isEmpty())
    m_pRouteModel->removeRow(m_pRouteModel->rowCount()-1);
    emit numberOfMeasurePointsChanged( m_pRouteModel->rowCount() );
}

void RouteToolPlugin::removeMeasurePoints()
{
    m_pRouteModel->clear();
    emit numberOfMeasurePointsChanged( m_pRouteModel->rowCount() );
}

void RouteToolPlugin::addContextItems()
{
    // Connect the inputHandler and the measure tool to the popup menu
    m_pAddMeasurePointAction = new QAction( QIcon(":/icons/measure.png"), tr( "Add &Wayoint" ), this );
    m_pShowRouteWindowAction = new QAction(tr("Show route window"), this);

    m_pSeparator = new QAction( this );
    m_pSeparator->setSeparator( true );

    //if ( ! (MarbleGlobal::getInstance()->profiles() & MarbleGlobal::SmallScreen) ) {
        // add actions to action list, which will added to the popup in MainWindow
        m_actionList.append(m_pAddMeasurePointAction);
        m_actionList.append(m_pShowRouteWindowAction);
        m_actionList.append(m_pSeparator);
    //}

    connect( m_pAddMeasurePointAction, SIGNAL(triggered()), SLOT(addMeasurePointEvent()) );
    connect(m_pShowRouteWindowAction, SIGNAL(triggered()), SLOT(showRouteWindow()));
    emit actionListChanged(m_actionList);  // currently not used (fires only before it gets connected)
}

void RouteToolPlugin::removeContextItems()
{
    delete m_pAddMeasurePointAction;
    delete m_pShowRouteWindowAction;
    delete m_pSeparator;
}

void RouteToolPlugin::addMeasurePointEvent()
{
    addMeasurePoint(m_posLastClick);
}

bool RouteToolPlugin::eventFilter( QObject *object, QEvent *e )
{
    if (e->type() == QEvent::MouseMove)
    {
        if (m_iSelectedWaypoint != -1)
        {
            QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(e);
            mouseMoved(mouseEvent->pos());
            return true;  // filter event
        }
    }
    else if (e->type() == QEvent::MouseButtonRelease)
    {
        m_iSelectedWaypoint = -1;
    }
    else if (e->type() == QEvent::MouseButtonPress) {
         QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(e);
         if (mouseEvent->button() == Qt::LeftButton)
         {
             QPoint mousePos(mouseEvent->x(),mouseEvent->y());
             for (int i = 0; i < m_pRouteModel->rowCount(); i++)
             {
                if (m_pRouteModel->getWaypoint(i)->getRegion().contains(mousePos))
                {
                    m_iSelectedWaypoint = i;
                    break;
                }
                else if (m_pRouteModel->getWaypoint(i)->getConnectionRegion().contains(mousePos))
                {
                    insertMeasurePoint(i, mousePos);
                    break;
                }
             }
        }
    }
    if ( m_pWidget && !enabled() ) {
        m_pWidget = 0;
        removeContextItems();
        m_pRouteModel->clear();
    }

    if ( m_pWidget || !enabled() || !visible() ) {
        return RenderPlugin::eventFilter( object, e );
    }

    QWidget *widget = qobject_cast<QWidget*>( object );
    if ( widget ) {
        m_pWidget = widget;
        addContextItems();
    }
    return RenderPlugin::eventFilter( object, e );
}

void RouteToolPlugin::showRouteWindow()
{
    if (m_pRouteWindow == 0)
    {
        m_pRouteWindow = new RouteWindow(m_pRouteModel);
    }
    m_pRouteWindow->show();

}

void RouteToolPlugin::moveWaypoint(QPoint pos)
{
    if (m_iSelectedWaypoint >= 0)
    {
        qreal lat;
        qreal lon;
        m_pMap->geoCoordinates(pos.x(),pos.y(), lon, lat, GeoDataCoordinates::Degree);

        QVector<const GeoDataFeature *> placeMark = m_pMap->whichFeatureAt(pos);
        if (placeMark.empty())
        {
            RouteWaypoint *waypoint = m_pRouteModel->getWaypoint(m_iSelectedWaypoint);
            waypoint->clear();
            waypoint->setLat(lat);
            waypoint->setLon(lon);
            waypoint->setSubtype(QString("USER"));
            m_pRouteModel->waypointMoved(m_iSelectedWaypoint);
            //emit repaintNeeded();
        }
        else
        {
            RouteWaypoint *waypoint = m_pRouteModel->getWaypoint(m_iSelectedWaypoint);
            waypoint->setCoordinates(dynamic_cast<const Marble::GeoDataPlacemark*>(placeMark.first())->coordinate());
            getWaypointInfo(waypoint, placeMark.first()->name(), placeMark.first()->description(), dynamic_cast<const Marble::GeoDataPlacemark*>(placeMark.first())->countryCode());
            m_pRouteModel->waypointMoved(m_iSelectedWaypoint);
            // signal repaintNeeded() emitted by the route model
        }
    }
}

void RouteToolPlugin::setPosClicked(int x, int y)
{
    m_posLastClick.setX(x);
    m_posLastClick.setY(y);
}

void RouteToolPlugin::changeLineColor1()
{
    m_lineColor1 = QColorDialog::getColor(m_lineColor1, m_pConfigDialog);
    m_pUiConfigWidget->widLineColor1->setColor(m_lineColor1);
    m_pUiConfigWidget->lblLineColor1->setText(m_lineColor1.name());
}

void RouteToolPlugin::changeLineColor2()
{
    m_lineColor2 = QColorDialog::getColor(m_lineColor2, m_pConfigDialog);
    m_pUiConfigWidget->widLineColor2->setColor(m_lineColor2);
    m_pUiConfigWidget->lblLineColor2->setText(m_lineColor2.name());
}

/*
void RouteToolPlugin::customEvent(QEvent *event)
{


}
*/

}

//Q_EXPORT_PLUGIN2( RouteToolPlugin, Marble::RouteToolPlugin )



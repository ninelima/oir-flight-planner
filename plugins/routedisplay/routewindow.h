//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//

#ifndef ROUTEWINDOW_H
#define ROUTEWINDOW_H

#include <QWidget>
#include "../../aircraft.h"


namespace Ui {
    class RouteWindow;
}
class RouteModel;
class QSortFilterProxyModel;

class RouteWindow : public QWidget
{
    Q_OBJECT

public:
    explicit RouteWindow(RouteModel *routeModel,  QWidget *parent = 0);
    ~RouteWindow();

    QString impexportFilePath() const;
    void setImpexportFilePath(const QString &impexportFilePath);
    void setAicraftList(AircraftList *aircraftList);

public slots:

private:
    Ui::RouteWindow *ui;
    RouteModel *m_pRouteModel;
    QSortFilterProxyModel *m_pProxyModel;
    AircraftList *m_pAircraftList;
    QString m_impexportFilePath;

    void exportRouteCSV(QString fileName);
    void exportRouteKML(QString fileName);
private slots:
    void exportRoute();
    void importRoute();
    void clearRoute();
    void moveItemUp();
    void moveItemDown();
    void removeItem();
    void insertItem();
    void toggleItemTotalizer();
    void updateTotals();
    void showRouteNotam();
    void aircraftSelected(int index);
    void setAicraftUserSpecific();

signals:
    void settingsChanged();
    void showAptInfo(int index, QString code);
    void gotoWpt(qreal lon, qreal lat);
    void routeNotamRequested(QStringList icaoCodes);
    void speedSettingsChanged();

};

#endif // ROUTEWINDOW_H

//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//

#ifndef MARBLE_ROUTETOOLPLUGIN_H
#define MARBLE_ROUTETOOLPLUGIN_H

#include <DialogConfigurationInterface.h>
#include <GeoDataLineString.h>
#include <RenderPlugin.h>
#include "../../RouteToolPluginInterface.h"

#include <QObject>
#include <QFont>
#include <QPen>
#include <QPoint>
#include <QColor>

namespace Ui {
    class RouteConfigWidget;
}

class RouteWindow;
class RouteWaypoint;
class RouteModel;
class QWidget;
class QAction;

namespace Marble
{


class RouteToolPlugin : public RenderPlugin, public DialogConfigurationInterface, public RouteToolPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA( IID "org.kde.edu.marble.MeasureToolPlugin" )
    Q_INTERFACES( Marble::RenderPluginInterface )
    Q_INTERFACES( Marble::DialogConfigurationInterface )
    Q_INTERFACES(RouteToolPluginInterface)
    MARBLE_PLUGIN( RouteToolPlugin )

 public:
    explicit RouteToolPlugin( const MarbleModel *marbleModel = 0 );
    ~RouteToolPlugin();

    QStringList backendTypes() const;
    QString renderPolicy() const;
    QStringList renderPosition() const;
    QString name() const;
    QString guiString() const;
    QString nameId() const;

    QString version() const;

    QString description() const;

    QString copyrightYears() const;

    QVector<PluginAuthor> pluginAuthors() const;

    QIcon icon () const;

    void initialize ();

    bool isInitialized () const;

    bool render( GeoPainter *painter, ViewportParams *viewport, const QString& renderPos, GeoSceneLayer * layer = 0 );

    QDialog *configDialog();
    QHash<QString,QVariant> settings() const;
    void setSettings( const QHash<QString,QVariant> &settings );

    // from RouteToolPluginInteface
    void addWaypoint(qreal lon, qreal lat, QString code, QString type);
    void closePopup();
    void setMap(MarbleMap *map);
    void setACList(AircraftList *aircraftList);
    QList<QAction *> getActions();
    RouteToolPluginWorker *newModule();

Q_SIGNALS:
    void numberOfMeasurePointsChanged( int newNumber );
    void showAptInfo(int index, QString code);  // from RouteToolPluginInterface.h
    void gotoWpt(qreal lon, qreal lat);
    void actionListChanged(QList<QAction *> actionList);
    void mouseMoved(QPoint);
    void routeNotamsRequested(QStringList icaoCodes);


 public Q_SLOTS:
    bool eventFilter( QObject *object, QEvent *event );
    void showRouteWindow();


protected:
//    void customEvent(QEvent *event);

protected slots:
private:
    void  drawMeasurePoints( GeoPainter *painter ) const;
    void  drawTotalDistanceLabel( GeoPainter *painter,
                                  qreal totalDistance ) const;
    void  drawSegments( GeoPainter *painter );
    void  addContextItems();
    void  removeContextItems();
    void  getWaypointInfo(RouteWaypoint *waypoint, QString code, QString type, QString country = QString("%"));

 private Q_SLOTS:
    void  addMeasurePointEvent();

    void  addMeasurePoint(QPoint mousePos);
    void  insertMeasurePoint(int pos, QPoint mousePos);
    void  removeLastMeasurePoint();
    void  removeMeasurePoints();

    void writeSettings();

    void writePathSetting();
    void moveWaypoint(QPoint pos);
    void setPosClicked(int x, int y);
    void changeLineColor1();
    void changeLineColor2();
private:
    Q_DISABLE_COPY( RouteToolPlugin )

    const QPixmap m_mark;
    QFont   m_font_regular;
    int     m_fontascent;

    QPen    m_pen;
    QAction *m_pAddMeasurePointAction;
    QAction *m_pShowRouteWindowAction;
    QAction *m_pSeparator;

    QWidget *m_pWidget;
    MarbleMap *m_pMap;
    const MarbleModel *m_pMarbleModel;

    QDialog * m_pConfigDialog;
    Ui::RouteConfigWidget * m_pUiConfigWidget;
    RouteModel *m_pRouteModel;  // Model must be ready before RouteWindow
    RouteWindow * m_pRouteWindow;
    bool m_showDistanceLabel;
    bool m_showBearingLabel;
    QColor m_lineColor1;
    QColor m_lineColor2;
    qreal m_lineWidth;
    int m_iSelectedWaypoint;
    QList<QAction *> m_actionList;
    QPoint m_posLastClick;

};

}

#endif // MARBLE_ROUTETOOLPLUGIN_H

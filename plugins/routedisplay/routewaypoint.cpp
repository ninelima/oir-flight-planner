//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 47 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:02:47 +0000 (Wed, 07 Nov 2018) $
//

#include "routewaypoint.h"

RouteWaypoint::RouteWaypoint(QObject *parent) :
    QObject(parent),
    m_name(QString()),
    m_code(QString()),
    m_type(QString()),
    m_course(0),
    m_distance(0),
    m_distCumulated(0),
    m_time(QTime()),
    m_timeCumulated(QTime()),
    m_isTotalizer(false)
{
}

void RouteWaypoint::clear()
{
    m_name = QString();
    m_code = QString();
    m_type = QString();
    m_course = 0;
    m_distance = 0;
    m_distCumulated = 0;
    m_time = QTime();
    m_timeCumulated = QTime();
}

void RouteWaypoint::setLat(qreal lat)
{
    m_coord.setLatitude(lat, GeoDataCoordinates::Degree);
}

void RouteWaypoint::setLon(qreal lon)
{
    m_coord.setLongitude(lon, GeoDataCoordinates::Degree);
}

void RouteWaypoint::setCoordinates(GeoDataCoordinates coord)
{
    m_coord = coord;
}

GeoDataCoordinates RouteWaypoint::getCoordinates()
{
    return m_coord;
}

qreal RouteWaypoint::getLon()
{
    return m_coord.longitude(GeoDataCoordinates::Degree);
}
qreal RouteWaypoint::getLat()
{
    return m_coord.latitude(GeoDataCoordinates::Degree);
}



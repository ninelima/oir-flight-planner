//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//

#ifndef MARBLE_AIRSPACESPLUGIN_H
#define MARBLE_AIRSPACESPLUGIN_H

#include <DialogConfigurationInterface.h>
#include <RenderPlugin.h>
#include "../../AirspacesPluginInterface.h"

#include "geodataairspace.h"

#include <QObject>
#include <QFont>
#include <QPen>
#include <QAction>

namespace Ui {
    class AirspacesConfigWidget;
}

class QPlainTextEdit;
class QTimer;
class AirspacePopup;

namespace Marble
{
class MarbleMap;

class AirspacesPlugin : public RenderPlugin, public DialogConfigurationInterface, public AirspacesPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA( IID "org.kde.edu.marble.AirspacesPlugin" )
    Q_INTERFACES( Marble::RenderPluginInterface )
    Q_INTERFACES( Marble::DialogConfigurationInterface )
    Q_INTERFACES(AirspacesPluginInterface)
    MARBLE_PLUGIN( AirspacesPlugin )

 public:
    explicit AirspacesPlugin( const MarbleModel *marbleModel = 0 );

    QStringList backendTypes() const;
    QString renderPolicy() const;
    QStringList renderPosition() const;
    QString name() const;
    QString guiString() const;
    QString nameId() const;

    QString version() const;

    QString description() const;

    QString copyrightYears() const;

    QVector<PluginAuthor> pluginAuthors() const;

    QIcon icon () const;

    void initialize ();

    bool isInitialized () const;


    bool render( GeoPainter *painter, ViewportParams *viewport, const QString& renderPos, GeoSceneLayer * layer = 0 );

    QDialog *configDialog();
    QHash<QString,QVariant> settings() const;
    void setSettings( const QHash<QString,QVariant> &settings );

    // from AirspacesPluginInterface
    void setMap(MarbleMap *map, QStringList *activeContinents);

    AirspacesPluginWorker *newModule();

 Q_SIGNALS:

 public Q_SLOTS:
    bool  eventFilter( QObject *object, QEvent *event );
    void setShowLabels(bool enable);
    void setShowPopup(bool enable);


 private:
    void  drawAirspaceLabel( GeoPainter *painter,
                                  GeoDataAirspace *airspaces) const;
    void  drawAirspaceBorders(GeoPainter *painter , ViewportParams *viewport);
    void  addContextItems();
    void  removeContextItems();
    void  getAirspaces();

 private Q_SLOTS:

    void writeSettings();
    void changeUpperLimit(int upper);
    void changeLowerLimit(int lower);
    void changePopupTimeout(int timeout);
    void drawAirspacePopup(QList<GeoDataAirspace*> airspaces, QPoint mousePos);

 private:
    Q_DISABLE_COPY( AirspacesPlugin )


    int     m_upperLimit;
    int     m_lowerLimit;
    bool    m_initialized;
    QFont   m_font_regular;
    int     m_fontascent;
    bool    m_showlabels;
    bool    m_showPopup;

    QPen    m_pen;

    QWidget *m_widget;
    MarbleMap *m_map;
    QDialog * m_configDialog;
    Ui::AirspacesConfigWidget * m_uiConfigWidget;

    QList<GeoDataAirspace*> m_airspaces;
    AirspacePopup *m_airspacePopup;
    QTimer *m_popupTimer;
    int m_popupTimeout;
    QStringList *m_pActiveContinents;

};

}

#endif // MARBLE_AIRSPACESPLUGIN_H

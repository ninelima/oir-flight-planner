CONFIG -= debug_and_release
CONFIG( debug, debug|release )  {
  CONFIG -= release
}
else {
  CONFIG -= debug
  CONFIG += release
}

QT += core widgets sql
TEMPLATE = lib
CONFIG += plugin
TARGET = airspaces

include(../../common.pri)

win32 {
#Windows
        DLLDESTDIR = $$DIST_DIR/plugins
} else {
#Linux
        DESTDIR = $$DIST_DIR/plugins
}

INCLUDEPATH +=  $$MARBLE_INC


# Input
HEADERS += AirspacesPlugin.h \
    ../../AirspacesPluginInterface.h \
    geodataairspace.h \
    airspacepopup.h
FORMS += AirspacesConfigWidget.ui
SOURCES += AirspacesPlugin.cpp \
    geodataairspace.cpp \
    airspacepopup.cpp


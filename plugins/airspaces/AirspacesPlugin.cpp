//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//

#include "AirspacesPlugin.h"
#include "ui_AirspacesConfigWidget.h"

#include <GeoPainter.h>
#include <MarbleDebug.h>
#include <MarbleMath.h>
#include <MarbleModel.h>
#include <MarbleLocale.h>
#include <Planet.h>
#include <ViewportParams.h>
#include <GeoDataLatLonAltBox.h>

#include <QDebug>
#include <QColor>
#include <QPen>
#include <QPixmap>
#include <QPushButton>
#include <QCheckBox>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QPlainTextEdit>
#include <QTimer>

#include <QtSql/QSqlQuery>

#include "airspacepopup.h"

namespace Marble
{

AirspacesPlugin::AirspacesPlugin( const MarbleModel *marbleModel )
    : RenderPlugin( marbleModel ),
      m_upperLimit(15000),
      m_lowerLimit(0),
      m_initialized(false),
      m_font_regular( QFont( "Sans Serif",  8, 50, false ) ),
      m_fontascent( QFontMetrics( m_font_regular ).ascent() ),
      m_showlabels(false),
      m_showPopup(true),
      m_pen( Qt::red ),
      m_widget(0),
      m_map(0),
      m_configDialog( 0 ),
      m_uiConfigWidget( 0 ),
      m_airspacePopup( 0 ),
      m_popupTimer(new QTimer()),
      m_popupTimeout(5000),
      m_pActiveContinents(0)

{
    m_pen.setWidthF( 2.0 );
    this->w = this->newModule();
    m_popupTimer->setSingleShot(true);
    initialize();
}

QStringList AirspacesPlugin::backendTypes() const
{
    return QStringList( "airspaces" );
}

QString AirspacesPlugin::renderPolicy() const
{
    return QString( "ALWAYS" );
}

QStringList AirspacesPlugin::renderPosition() const
{
    return QStringList() << "HOVERS_ABOVE_SURFACE";
}

QString AirspacesPlugin::name() const
{
    return tr( "Airspaces Display" );
}

QString AirspacesPlugin::guiString() const
{
    return tr( "&Airspaces Display" );
}

QString AirspacesPlugin::nameId() const
{
    return QString( "airspaces-display" );
}

QString AirspacesPlugin::version() const
{
    return "1.0";
}

QString AirspacesPlugin::description() const
{
    return tr( "Shows airspaces." );
}

QString AirspacesPlugin::copyrightYears() const
{
    return "2015-2016";
}

QVector<PluginAuthor> AirspacesPlugin::pluginAuthors() const
{
    return QVector<PluginAuthor>()
            << PluginAuthor( "Pascal Tritten", " ptritten@gmail.com" );
}

QIcon AirspacesPlugin::icon () const
{
    return QIcon(":/icons/measure.png");
}



void AirspacesPlugin::initialize ()
{
    if (m_initialized) return;

    setVisible(true);



}


bool AirspacesPlugin::isInitialized () const
{
    return true;
}

void AirspacesPlugin::setMap(MarbleMap *map, QStringList *activeContinents)
{
    m_map = map;
    m_pActiveContinents = activeContinents;
    getAirspaces();

}

AirspacesPluginWorker *AirspacesPlugin::newModule()
{
    return new AirspacesPluginWorker;
}

QDialog *AirspacesPlugin::configDialog()
{
    if ( !m_configDialog ) {
        m_configDialog = new QDialog();
        m_uiConfigWidget = new Ui::AirspacesConfigWidget;
        m_uiConfigWidget->setupUi( m_configDialog );
        connect(m_uiConfigWidget->m_buttonBox, SIGNAL(accepted()),
                SLOT(writeSettings()));
        connect(m_uiConfigWidget->m_buttonBox, SIGNAL(accepted()), m_configDialog, SLOT(close()));
        connect(m_uiConfigWidget->m_buttonBox, SIGNAL(rejected()), m_configDialog, SLOT(close()));
        QPushButton *applyButton = m_uiConfigWidget->m_buttonBox->button( QDialogButtonBox::Apply );
        connect(applyButton, SIGNAL(clicked()),
                 this,        SLOT(writeSettings()));
        connect(m_uiConfigWidget->sbLowerLimit, SIGNAL(valueChanged(int)), this, SLOT(changeLowerLimit(int)));
        connect(m_uiConfigWidget->sbUpperLimit, SIGNAL(valueChanged(int)), this, SLOT(changeUpperLimit(int)));
        connect(m_uiConfigWidget->sbPopupTimeout, SIGNAL(valueChanged(int)), this, SLOT(changePopupTimeout(int)));
        connect(m_uiConfigWidget->cbShowLabels, SIGNAL(clicked(bool)), this, SLOT(setShowLabels(bool)));
        connect(m_uiConfigWidget->cbShowPopup, SIGNAL(clicked(bool)), this, SLOT(setShowPopup(bool)));
    }

    m_uiConfigWidget->sbLowerLimit->setValue(m_lowerLimit);
    m_uiConfigWidget->sbUpperLimit->setValue(m_upperLimit);
    m_uiConfigWidget->sbPopupTimeout->setValue(m_popupTimeout);

    return m_configDialog;
}

QHash<QString,QVariant> AirspacesPlugin::settings() const
{
    QHash<QString, QVariant> settings = RenderPlugin::settings();
    settings.insert( "lowerLimit", m_lowerLimit );
    settings.insert( "upperLimit", m_upperLimit );
    settings.insert("popupTimeout", m_popupTimeout);
    return settings;
}

void AirspacesPlugin::setSettings( const QHash<QString,QVariant> &settings )
{
    RenderPlugin::setSettings( settings );
    m_lowerLimit = settings.value( "lowerLimit", 0 ).toInt();
    m_upperLimit = settings.value( "upperLimit", 15000 ).toInt();
    m_popupTimeout = settings.value("popupTimeout", 5000).toInt();
}


void AirspacesPlugin::writeSettings()
{
    m_lowerLimit = m_uiConfigWidget->sbLowerLimit->value();
    m_upperLimit = m_uiConfigWidget->sbUpperLimit->value();
    m_popupTimeout = m_uiConfigWidget->sbPopupTimeout->value();

    emit settingsChanged( nameId() );
    emit repaintNeeded();
}

void AirspacesPlugin::changeUpperLimit(int upper)
{
    m_upperLimit = upper;
    emit repaintNeeded();
}

void AirspacesPlugin::changeLowerLimit(int lower)
{
    m_lowerLimit = lower;
    emit repaintNeeded();
}

void AirspacesPlugin::changePopupTimeout(int timeout)
{
    m_popupTimeout = timeout;
}

void AirspacesPlugin::drawAirspacePopup(QList<GeoDataAirspace *> airspaces, QPoint mousePos)
{
    Q_UNUSED(mousePos)
    if (!m_showPopup) return;

    if (m_airspacePopup == 0)
    {
        m_airspacePopup = new AirspacePopup(m_widget);
        m_airspacePopup->setWindowFlags(Qt::WindowStaysOnTopHint);
        m_airspacePopup->setWindowTitle(tr("Airspaces"));
        connect(m_popupTimer, SIGNAL(timeout()), m_airspacePopup, SLOT(hide()));
    }
    m_airspacePopup->clearText();
    for (int i = 0; i < airspaces.size(); i++)
    {
        GeoDataAirspace *asp = airspaces.at(i);
        m_airspacePopup->appendText(QString("%1 %2 %3 %4").arg(asp->airspaceClass).arg(asp->lower).arg(asp->upper).arg(asp->name));
    }
    m_airspacePopup->show();
    m_popupTimer->start(m_popupTimeout);
}


bool AirspacesPlugin::render( GeoPainter *painter,
                          ViewportParams *viewport,
                          const QString& renderPos,
                          GeoSceneLayer * layer )
{
    Q_UNUSED(renderPos)
    Q_UNUSED(layer)

    if (!this->visible()) return true;

    painter->save();
    painter->setPen( m_pen );
    drawAirspaceBorders( painter, viewport );
    painter->restore();

    return true;
}

void AirspacesPlugin::drawAirspaceBorders( GeoPainter* painter, ViewportParams * viewport )
{
    for (int i = 0; i < m_airspaces.size(); i++)
    {
        GeoDataAirspace *airspace = m_airspaces.at(i);
        if (airspace->lower > m_upperLimit) continue;
        if (airspace->upper < m_lowerLimit) continue;

        GeoDataAirspace ring = *(airspace);

        if ( ! viewport->viewLatLonAltBox().intersects(ring.latLonAltBox()))
        {
            //qDebug() << "skip" << airspace->name;
            m_airspaces.at(i)->onMap = false;
            continue;
        }
        //qDebug() << "draw" << airspace->name;
        m_airspaces.at(i)->onMap = true;

        QPen linePen;
        QPen innerPen;
        bool innerBorder = false;
/*
        A
        B
        C
        CTR
        D
        DANGER
        E
        F
        G
        GLIDING
        OTH
        RESTRICTED
        TMA
        TMZ
        WAVE
        PROHIBITED
        FIR
        UIR
        RMZ
*/
        if (airspace->airspaceClass == "A")
        {
            linePen.setColor(QColor(200,0,0, 100));
            linePen.setWidthF(4.0);

        }
/*        else if (airspace->airspaceClass == "B")
        {

        }*/
        else if (airspace->airspaceClass == "C" || airspace->airspaceClass == "D")
        {
            linePen.setColor(QColor(0,0,200, 100));
            linePen.setWidthF(4.0);

        }
        else if (airspace->airspaceClass == "CTR")
        {
            linePen.setColor(QColor(0,0,200, 100));
            linePen.setWidthF(4.0);
            linePen.setStyle(Qt::DashLine);
            innerPen.setColor(QColor(0,0,200, 50));
            innerPen.setWidth(16.0);
            innerBorder = true;
        }
        else if (airspace->airspaceClass == "DANGER")
        {
            linePen.setColor(QColor(200,0,0, 100));
            linePen.setWidthF(2.0);

        }
        else if (airspace->airspaceClass == "E" || airspace->airspaceClass == "F")
        {
            linePen.setColor(QColor(0,0,200, 100));
            linePen.setWidthF(2.0);
        }
        else if (airspace->airspaceClass == "RESTRICTED")
        {
            linePen.setColor(QColor(200,0,0, 100));
            linePen.setWidthF(2.0);

        }
        else if (airspace->airspaceClass == "PROHIBITED")
        {
            linePen.setColor(QColor(200,0,0, 100));
            linePen.setWidthF(2.0);

        }
        else
        {
            linePen.setColor(QColor(200,0,0, 100));
            linePen.setWidthF(4.0);
        }

        painter->setPen( linePen );

        if (innerBorder)
        {
            painter->drawPolygon(ring); // draw outer line of shape
            painter->setClipRegion(painter->regionFromPolygon(ring, Qt::OddEvenFill));
            painter->setPen(innerPen);
            painter->drawPolygon(ring); // draw inner line of shape
            painter->setClipRegion(QRegion(), Qt::NoClip);  // disable further clipping
        }
        else
        {
            painter->drawPolygon(ring);
        }
        drawAirspaceLabel(painter, airspace);
    }

}



void AirspacesPlugin::drawAirspaceLabel(GeoPainter *painter,
                                          GeoDataAirspace *airspaces ) const
{
    if (!m_showlabels) return;
    painter->setPen( QColor( Qt::blue ) );

    GeoDataLatLonAltBox box = airspaces->latLonAltBox();
    //painter->drawRect( 10, 105, 10 + QFontMetrics( m_font_regular ).boundingRect( distanceString ).width() + 5, 10 + m_fontascent + 2 );
    painter->setFont( m_font_regular );
    QString str = QString("%1 / %2 | %3").arg(airspaces->airspaceClass).arg(airspaces->lower).arg(airspaces->upper);
    painter->drawText( box.center(), str);
}




void AirspacesPlugin::addContextItems()
{
//    MapWidgetPopupMenu *menu = m_marbleWidget->popupMenu();

    // Connect the inputHandler and the measure tool to the popup menu
    /*
    m_addMeasurePointAction = new QAction( QIcon(":/icons/measure.png"), tr( "Add &Measure Point" ), this );
    m_removeLastMeasurePointAction = new QAction( tr( "Remove &Last Measure Point" ), this );
    m_removeLastMeasurePointAction->setEnabled( false );
    m_removeMeasurePointsAction = new QAction( tr( "&Remove Measure Points" ), this );
    m_removeMeasurePointsAction->setEnabled( false );
    m_separator = new QAction( this );
    m_separator->setSeparator( true );

    if ( ! MarbleGlobal::getInstance()->profiles() & MarbleGlobal::SmallScreen ) {
        menu->addAction( Qt::RightButton, m_addMeasurePointAction );
        menu->addAction( Qt::RightButton, m_removeLastMeasurePointAction );
        menu->addAction( Qt::RightButton, m_removeMeasurePointsAction );
        menu->addAction( Qt::RightButton, m_separator );
    }

    connect( m_addMeasurePointAction, SIGNAL(triggered()), SLOT(addMeasurePointEvent()) );
    connect( m_removeLastMeasurePointAction, SIGNAL(triggered()), SLOT(removeLastMeasurePoint()) );
    connect( m_removeMeasurePointsAction, SIGNAL(triggered()), SLOT(removeMeasurePoints()) );

    connect( this, SIGNAL(numberOfMeasurePointsChanged(int)), SLOT(setNumberOfMeasurePoints(int)) );
    */
}

void AirspacesPlugin::removeContextItems()
{
    /*
    delete m_addMeasurePointAction;
    delete m_removeLastMeasurePointAction;
    delete m_removeMeasurePointsAction;
    delete m_separator;
    */
}

void AirspacesPlugin::getAirspaces()
{
    QString sQuery;
    QString activeContinents = m_pActiveContinents->join("' OR continent = '");
    sQuery = QString("SELECT cat, polygon, top, topunit, bottom, bottomunit, name FROM airspaces WHERE (continent = '%1') AND NOT cat='WAVE';").arg(activeContinents);
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    while (query.next())
    {
        QString cat = query.value(0).toString();
        if (cat == "FIR")  // we don not show FIR's
            continue;

        int upper = query.value(2).toInt();
        QString upperUnit = query.value(3).toString();
        if (upperUnit == "FL")
        {
            upper *= 100;
        }
        int lower = query.value(4).toInt();
        QString lowerUnit = query.value(5).toString();
        if (lowerUnit == "FL")
        {
            lower *= 100;
        }

        QStringList polygon = query.value(1).toString().split(",");
        GeoDataAirspace *ring = new GeoDataAirspace;
        ring->upper = upper;
        ring->lower = lower;
        ring->airspaceClass = cat;
        ring->name = query.value(6).toString();
        for (int i = 0; i < polygon.size(); i++)
        {
            QStringList coord = polygon.at(i).trimmed().split(" ");
            if (coord.size() > 1)
            {
               *ring << GeoDataCoordinates(coord.at(0).toFloat(), coord.at(1).toFloat(), 0, GeoDataCoordinates::Degree);
            }
            // TODO: log output if coord.size is < 2!!
        }
        m_airspaces << ring;
    }
}


bool AirspacesPlugin::eventFilter( QObject *object, QEvent *e )
{
     if (e->type() == QEvent::MouseButtonPress) {
         QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(e);
         if (mouseEvent->modifiers() == Qt::ControlModifier)
         {
             if (mouseEvent->button() == Qt::LeftButton)
             {
                 GeoDataCoordinates coord;
                 qreal lat;
                 qreal lon;
                 m_map->geoCoordinates(mouseEvent->x(),mouseEvent->y(), lon, lat, GeoDataCoordinates::Degree);
                 coord.setLongitude(lon, GeoDataCoordinates::Degree);
                 coord.setLatitude(lat, GeoDataCoordinates::Degree);
                 QList<GeoDataAirspace*> airspacesAtPos;
                 for (int i = 0; i < m_airspaces.size(); i++)
                 {
                     GeoDataAirspace *asp = m_airspaces.at(i);

                     if (!asp->onMap) continue;
                     if (asp->contains(coord))
                     {
                        //qDebug() << asp->name << asp->lower << asp->upper << asp->airspaceClass;
                        airspacesAtPos << asp;
                     }
                 }
                 if (airspacesAtPos.size() > 0)
                 {
                     drawAirspacePopup(airspacesAtPos, QPoint (mouseEvent->x(), mouseEvent->y()));
                 }
            }
        }
    }



    if ( m_widget && !enabled() ) {
        m_widget = 0;
//        removeContextItems();
    }

    if ( m_widget || !enabled() || !visible() ) {
        return RenderPlugin::eventFilter( object, e );
    }

    QWidget *widget = qobject_cast<QWidget*>( object );
    if ( widget ) {
        m_widget = widget;
//        addContextItems();
    }

    return RenderPlugin::eventFilter( object, e );
}

void AirspacesPlugin::setShowLabels(bool enable)
{
    m_showlabels = enable;
}

void AirspacesPlugin::setShowPopup(bool enable)
{
    m_showPopup = enable;
}

}

//Q_EXPORT_PLUGIN2( AirspacesPlugin, Marble::AirspacesPlugin )



//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "airspacepopup.h"
#include <QPainter>


AirspacePopup::AirspacePopup(QWidget *parent) :
    QWidget(parent),
    m_TextList(QStringList())
{
    QPalette pal(palette());
    // set black background
    pal.setColor(QPalette::Window, Qt::black);
    pal.setColor(QPalette::WindowText, Qt::lightGray);
    setAutoFillBackground(true);
    setPalette(pal);
}

void AirspacePopup::appendText(QString text)
{
    m_TextList << text;
    repaint();
}

void AirspacePopup::clearText()
{
    m_TextList.clear();
    repaint();
}

void AirspacePopup::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter painter(this);
    int y = 5;
    int h = 0;
    int w = 0;
    int max_h = 0;
    for (int i = 0; i < m_TextList.size(); i++)
    {
        ;
        QRect rect = painter.boundingRect(QRect(5,y,w,h), Qt::AlignLeft, m_TextList.at(i));
        painter.drawText(rect, Qt::AlignLeft|Qt::AlignVCenter, m_TextList.at(i));
        y += rect.height();
        max_h += rect.height();

        if (rect.width() > w) w = rect.width();
    }
    this->setGeometry(QRect(10, 10, w + 10, max_h + 10));
}

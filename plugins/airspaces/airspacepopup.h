//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//


#ifndef AIRSPACEPOPUP_H
#define AIRSPACEPOPUP_H

#include <QWidget>
#include <QString>

class AirspacePopup : public QWidget
{
    Q_OBJECT
public:
    explicit AirspacePopup(QWidget *parent = 0);

signals:

public slots:
    void appendText(QString text);
    void clearText();

protected:
   void	paintEvent(QPaintEvent *event);

   QString m_text;
   QStringList m_TextList;

};

#endif // AIRSPACEPOPUP_H

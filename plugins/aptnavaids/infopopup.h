//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//


#ifndef INFOPOPUP_H
#define INFOPOPUP_H

#include <QWidget>
#include <QTextCharFormat>


namespace Ui {
class InfoPopup;
}

class InfoPopup : public QWidget
{
    Q_OBJECT

public:
    explicit InfoPopup(QWidget *parent = 0);
    ~InfoPopup();
    QTextCharFormat *getCharFormat();

public slots:
    void appendText(QString text);
    void setCharFormat(const QTextCharFormat &m_format);
    void clearText();


private:
    Ui::InfoPopup *ui;
    QString m_text;
    QStringList m_TextList;
    QTextCharFormat m_format;
};

#endif // INFOPOPUP_H

//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//

#include "AptNavaidsPlugin.h"
#include "ui_AptNavaidsConfigWidget.h"

#include <GeoPainter.h>
#include <MarbleDebug.h>
#include <MarbleMath.h>
#include <MarbleModel.h>
#include <MarbleLocale.h>
#include <MarbleDirs.h>
#include <Planet.h>
#include <GeoDataPlacemark.h>
#include <GeoDataTreeModel.h>
#include <GeoDataStyle.h>
#include <GeoDataIconStyle.h>
#include <GeoDataDocument.h>
#include <ViewportParams.h>

#include <QDebug>
#include <QColor>
#include <QPen>
#include <QPixmap>
#include <QPushButton>
#include <QCheckBox>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QPlainTextEdit>
#include <QTimer>

#include <QtSql/QSqlQuery>

#include "infopopup.h"

namespace Marble
{

AptNavaidsPlugin::AptNavaidsPlugin( const MarbleModel *marbleModel )
    : RenderPlugin( marbleModel ),
      m_showApts(true),
      m_showNavaids(true),
      m_show5LNC(true),
      m_initialized(false),
      m_font_regular( QFont( "Sans Serif",  8, 50, false ) ),
      m_fontascent( QFontMetrics( m_font_regular ).ascent() ),
      m_widget(nullptr),
      m_map(0),
      m_configDialog( 0 ),
      m_uiConfigWidget( 0 ),
      m_infoPopup( 0 ),
      m_pActiveContinents(0)
{
    this->w = this->newModule();
    initialize();
}

QStringList AptNavaidsPlugin::backendTypes() const
{
    return QStringList( "aptnavaids" );
}

QString AptNavaidsPlugin::renderPolicy() const
{
    return QString( "ALWAYS" );
}

QStringList AptNavaidsPlugin::renderPosition() const
{
    return QStringList() << "HOVERS_ABOVE_SURFACE";
}

QString AptNavaidsPlugin::name() const
{
    return tr( "Airports and Navaids Display" );
}

QString AptNavaidsPlugin::guiString() const
{
    return tr( "Airports and &Navaids Display" );
}

QString AptNavaidsPlugin::nameId() const
{
    return QString( "aptnavaids-display" );
}

QString AptNavaidsPlugin::version() const
{
    return "1.1";
}

QString AptNavaidsPlugin::description() const
{
    return tr( "Shows airports and navaids." );
}

QString AptNavaidsPlugin::copyrightYears() const
{
    return "2015-2018";
}

QVector<PluginAuthor> AptNavaidsPlugin::pluginAuthors() const
{
    return QVector<PluginAuthor>()
            << PluginAuthor( "Pascal Tritten", " ptritten@gmail.com" );
}

QIcon AptNavaidsPlugin::icon () const
{
    return QIcon(":/icons/measure.png");
}



void AptNavaidsPlugin::initialize ()
{
    if (m_initialized) return;


}


bool AptNavaidsPlugin::isInitialized () const
{
    return true;
}

void AptNavaidsPlugin::setMap(MarbleMap *map, QStringList *activeContinents)
{
    m_map = map;
    m_pActiveContinents = activeContinents;
    m_pAirports = new GeoDataDocument;
    getAirports(*m_pAirports);
    m_pNavaids = new GeoDataDocument;
    getNavaids(*m_pNavaids);
    m_p5LNC = new GeoDataDocument;
    get5LNC(*m_p5LNC);
    indexAirports = m_map->model()->treeModel()->addDocument(m_pAirports);
    indexNavaids = m_map->model()->treeModel()->addDocument(m_pNavaids);
    index5LNC = m_map->model()->treeModel()->addDocument(m_p5LNC);

    setVisible(true);

}

AptNavaidsPluginWorker *AptNavaidsPlugin::newModule()
{
    return new AptNavaidsPluginWorker;
}

QDialog *AptNavaidsPlugin::configDialog()
{
    if ( !m_configDialog ) {
        m_configDialog = new QDialog();
        m_uiConfigWidget = new Ui::AptNavaidsConfigWidget;
        m_uiConfigWidget->setupUi( m_configDialog );
        connect(m_uiConfigWidget->m_buttonBox, SIGNAL(accepted()),
                SLOT(writeSettings()));
        connect(m_uiConfigWidget->m_buttonBox, SIGNAL(accepted()), m_configDialog, SLOT(close()));
        connect(m_uiConfigWidget->m_buttonBox, SIGNAL(rejected()), m_configDialog, SLOT(close()));

        QPushButton *applyButton = m_uiConfigWidget->m_buttonBox->button( QDialogButtonBox::Apply );
        connect(applyButton, SIGNAL(clicked()),
                 this,        SLOT(writeSettings()));
        connect(m_uiConfigWidget->cbShowNavaids, SIGNAL(clicked(bool)), this, SLOT(setShowNavaids(bool)));
        connect(m_uiConfigWidget->cbShow5LNC, SIGNAL(clicked(bool)), this, SLOT(setShow5LNC(bool)));
        connect(m_uiConfigWidget->cbShowApts, SIGNAL(clicked(bool)), this, SLOT(setShowApts(bool)));
        connect(m_uiConfigWidget->rbRwyFeet, SIGNAL(clicked(bool)), this, SLOT(rwySizeUnitClicked()));
        connect(m_uiConfigWidget->rbRwyMeters, SIGNAL(clicked(bool)), this, SLOT(rwySizeUnitClicked()));
    }

    m_uiConfigWidget->cbShowNavaids->setChecked(m_showNavaids);
    m_uiConfigWidget->cbShow5LNC->setChecked(m_show5LNC);
    m_uiConfigWidget->cbShowApts->setChecked(m_showApts);

    if (m_rwySizeUnit == 0)
    {
        m_uiConfigWidget->rbRwyMeters->setChecked(true);
    }
    else
    {
        m_uiConfigWidget->rbRwyFeet->setChecked(true);
    }


    return m_configDialog;
}

QHash<QString,QVariant> AptNavaidsPlugin::settings() const
{
    QHash<QString, QVariant> settings = RenderPlugin::settings();
    settings.insert( "showNavaids", m_showNavaids );
    settings.insert( "show5LNC", m_show5LNC );
    settings.insert( "showAirports", m_showApts );
    settings.insert("rwySizeUnit", m_rwySizeUnit);
    return settings;
}

void AptNavaidsPlugin::setSettings( const QHash<QString,QVariant> &settings )
{
    RenderPlugin::setSettings( settings );
    m_showNavaids = settings.value( "showNavaids", true ).toBool();
    m_show5LNC = settings.value( "show5LNC", true ).toBool();
    m_showApts= settings.value( "showAirports", true ).toBool();
    m_rwySizeUnit = settings.value("rwySizeUnit", 0).toInt();
}


void AptNavaidsPlugin::writeSettings()
{
    m_showNavaids = m_uiConfigWidget->cbShowNavaids->isChecked();
    m_show5LNC = m_uiConfigWidget->cbShow5LNC->isChecked();
    m_showApts = m_uiConfigWidget->cbShowApts->isChecked();
    if (m_uiConfigWidget->rbRwyMeters->isChecked())
    {
        m_rwySizeUnit = 0;
    }
    else
    {
        m_rwySizeUnit = 1;
    }
    emit settingsChanged( nameId() );
    emit repaintNeeded();
}


void AptNavaidsPlugin::drawInfoPopup(QVector<const Marble::GeoDataFeature *> placeMarks, QPoint mousePos)
{
    Q_UNUSED(mousePos)
    if (m_infoPopup == 0)
    {
        m_infoPopup = new InfoPopup(m_widget);
        m_infoPopup->setWindowTitle(tr("Info Popup"));
    }
    m_infoPopup->clearText();
    // prepare text formats
    QTextCharFormat formatTitle;
    formatTitle.setFontPointSize(12);
    formatTitle.setFontWeight(75);
    formatTitle.setFontItalic(true);
    QTextCharFormat formatParagraph;
    formatParagraph.setFontPointSize(8);
    QTextCharFormat formatName;
    formatName.setFontPointSize(10);
    formatName.setForeground(QBrush(QColor(Qt::blue)));
    QTextCharFormat formatSubtitle;
    formatSubtitle.setFontPointSize(9);
//    formatSubtitle.setForeground(QBrush(QColor(Qt::blue)));
    formatSubtitle.setFontItalic(true);
//    formatSubtitle.toBlockFormat().setLineHeight(8,QTextBlockFormat::LineDistanceHeight);
    QSqlRecord rec;

    for (int i = 0; i < placeMarks.size(); i++)
    {
        m_infoPopup->setCharFormat(formatTitle);
        m_infoPopup->appendText(QString("%1").arg(placeMarks.at(i)->name()));
        GeoDataCoordinates coord = dynamic_cast<const Marble::GeoDataPlacemark*>(placeMarks.at(i))->coordinate();
        if (placeMarks.at(i)->description() == "A")
        {
            rec = getAirportInfo(placeMarks.at(i)->id());
            m_infoPopup->setCharFormat(formatName);
            m_infoPopup->appendText(rec.value("name").toString());
            m_infoPopup->setCharFormat(formatSubtitle);
            m_infoPopup->appendText(QString("\n%1:").arg(tr("Position")));
            m_infoPopup->setCharFormat(formatParagraph);
            QString pos = QString("Lat: %1\tLon: %2").arg(coord.latitude(GeoDataCoordinates::Degree))
                    .arg(coord.longitude(GeoDataCoordinates::Degree));
            if (coord.altitude() > 0)
            {
                pos.append(QString("\t%1: %2 ft").arg(tr("Elevation")).arg(coord.altitude()));
            }
            m_infoPopup->appendText(pos);

            QList<QSqlRecord> runwayData = loadRunwaysFrom(placeMarks.at(i)->id().toLongLong());
            m_infoPopup->setCharFormat(formatSubtitle);
            m_infoPopup->appendText(QString("\n%1:").arg(tr("Runways")));
            m_infoPopup->setCharFormat(formatParagraph);
            for (int i = 0; i < runwayData.size(); i++)
            {
                int length = runwayData.at(i).value("length_m").toInt();  // length an width are stored with 4 decimals
                int width = runwayData.at(i).value("width_m").toInt();   // which will be removed by converting to int
                QString unit = "m";
                if (m_rwySizeUnit == 1)
                {
                    length *= Marble::M2FT;
                    width *= Marble::M2FT;
                    unit = "ft";
                }

                QString t = QString("%1\t%2x%3%4\t%5\tStatus: %6").arg(runwayData.at(i).value("name").toString())
                                                             .arg(length)
                                                             .arg(width)
                                                             .arg(unit)
                                                             .arg(runwayData.at(i).value("sfc").toString())
                                                             .arg(runwayData.at(i).value("status").toString());
                m_infoPopup->appendText(t);
            }
            m_infoPopup->appendText("\n");
            QString temp = rec.value("home_url").toString();
            if (!temp.isEmpty()) {
                m_infoPopup->appendText(QString("Web: %1").arg(temp)); // Home URL
            }
            temp = rec.value("wikipedia_url").toString();
            if (!temp.isEmpty()) {
                    m_infoPopup->appendText(QString("Wikipedia: %1").arg(temp)); // Wiki URL
            }

        }
        else if (placeMarks.at(i)->description() == "N") // navaid
        {
            rec = getNavaidInfo(placeMarks.at(i)->id());
            m_infoPopup->setCharFormat(formatName);
            m_infoPopup->appendText(rec.value("name").toString());
            m_infoPopup->setCharFormat(formatSubtitle);
            m_infoPopup->appendText(QString("\n%1:").arg(tr("Position")));
            m_infoPopup->setCharFormat(formatParagraph);
            QString pos = QString("Lat: %1\tLon: %2").arg(coord.latitude(GeoDataCoordinates::Degree))
                    .arg(coord.longitude(GeoDataCoordinates::Degree));
            if (coord.altitude() > 0)
            {
                pos.append(QString("\t%1: %2 ft").arg(tr("Elevation")).arg(coord.altitude()));
            }
            m_infoPopup->appendText(pos);

            m_infoPopup->appendText(QString("Type: %1\n").arg(rec.value("type").toString()));
            m_infoPopup->appendText(QString("Frequency [MHz]: %1\n").arg(rec.value("freq_mhz").toString()));


        }
        else /*if (placeMarks.at(i)->description() == "N")*/ // 5LNC
        {
            m_infoPopup->setCharFormat(formatSubtitle);
            m_infoPopup->appendText(QString("\n%1:").arg(tr("Position")));
            m_infoPopup->setCharFormat(formatParagraph);
            QString pos = QString("Lat: %1\tLon: %2").arg(coord.latitude(GeoDataCoordinates::Degree))
                    .arg(coord.longitude(GeoDataCoordinates::Degree));
            m_infoPopup->appendText(pos);
        }
        if (!rec.isEmpty())
        {
            m_infoPopup->appendText(tr("Sources:")); // Source
            if (rec.value("source").toInt() & 0x01 ) // Source
            {
                m_infoPopup->appendText("www.openaip.net"); // Source
            }
            if (rec.value("source").toInt() & 0x02 ) // Source
            {
                m_infoPopup->appendText("www.ourairports.net"); // Source
            }
        }
        m_infoPopup->appendText("\n");

    }
    m_infoPopup->show();
}


bool AptNavaidsPlugin::render( GeoPainter *painter,
                          ViewportParams *viewport,
                          const QString& renderPos,
                          GeoSceneLayer * layer )
{
    Q_UNUSED(renderPos)
    Q_UNUSED(layer)
    Q_UNUSED(painter)
    Q_UNUSED(viewport)

    return true;
}

void AptNavaidsPlugin::addContextItems()
{
//    MapWidgetPopupMenu *menu = m_marbleWidget->popupMenu();

    // Connect the inputHandler and the measure tool to the popup menu
    /*
    m_addMeasurePointAction = new QAction( QIcon(":/icons/measure.png"), tr( "Add &Measure Point" ), this );
    m_removeLastMeasurePointAction = new QAction( tr( "Remove &Last Measure Point" ), this );
    m_removeLastMeasurePointAction->setEnabled( false );
    m_removeMeasurePointsAction = new QAction( tr( "&Remove Measure Points" ), this );
    m_removeMeasurePointsAction->setEnabled( false );
    m_separator = new QAction( this );
    m_separator->setSeparator( true );

    if ( ! MarbleGlobal::getInstance()->profiles() & MarbleGlobal::SmallScreen ) {
        menu->addAction( Qt::RightButton, m_addMeasurePointAction );
        menu->addAction( Qt::RightButton, m_removeLastMeasurePointAction );
        menu->addAction( Qt::RightButton, m_removeMeasurePointsAction );
        menu->addAction( Qt::RightButton, m_separator );
    }

    connect( m_addMeasurePointAction, SIGNAL(triggered()), SLOT(addMeasurePointEvent()) );
    connect( m_removeLastMeasurePointAction, SIGNAL(triggered()), SLOT(removeLastMeasurePoint()) );
    connect( m_removeMeasurePointsAction, SIGNAL(triggered()), SLOT(removeMeasurePoints()) );

    connect( this, SIGNAL(numberOfMeasurePointsChanged(int)), SLOT(setNumberOfMeasurePoints(int)) );
    */
}

void AptNavaidsPlugin::removeContextItems()
{
    /*
    delete m_addMeasurePointAction;
    delete m_removeLastMeasurePointAction;
    delete m_removeMeasurePointsAction;
    delete m_separator;
    */
}


bool AptNavaidsPlugin::eventFilter( QObject *object, QEvent *e )
{
     if (e->type() == QEvent::MouseButtonPress) {
         QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(e);
         if (mouseEvent->modifiers() == Qt::ShiftModifier)
         {
             if (mouseEvent->button() == Qt::LeftButton)
             {
                 const QPoint curpos = QPoint(mouseEvent->x(), mouseEvent->y());
                 QVector<const Marble::GeoDataFeature*> placeMarks = (m_map->whichFeatureAt(curpos));
                 if (!placeMarks.empty())
                 {
                     drawInfoPopup(placeMarks, QPoint (mouseEvent->x(), mouseEvent->y()));
                 }
            }
        }
    }



    if ( m_widget && !enabled() ) {
        m_widget = 0;
//        removeContextItems();
    }

    if ( m_widget || !enabled() || !visible() ) {
        return RenderPlugin::eventFilter( object, e );
    }

    QWidget *widget = qobject_cast<QWidget*>( object );
    if ( widget ) {
        m_widget = widget;
//        addContextItems();
    }

    return RenderPlugin::eventFilter( object, e );
}

void AptNavaidsPlugin::setShowNavaids(bool enable)
{
    m_showNavaids = enable;
    if (m_showNavaids)
    {
        if (indexNavaids == 0)
        {
            indexNavaids = m_map->model()->treeModel()->addDocument(m_pNavaids);
        }
    }
    else
    {
        if (indexNavaids != 0)
        {
            m_map->model()->treeModel()->removeDocument(indexNavaids);
            indexNavaids = 0;
        }
    }
}

void AptNavaidsPlugin::setShow5LNC(bool enable)
{
    m_show5LNC = enable;
    if (m_show5LNC)
    {
        if (index5LNC == 0)
        {
            index5LNC = m_map->model()->treeModel()->addDocument(m_p5LNC);
        }
    }
    else
    {
        if (index5LNC != 0)
        {
            m_map->model()->treeModel()->removeDocument(index5LNC);
            index5LNC = 0;
        }
    }
}

void AptNavaidsPlugin::setShowApts(bool enable)
{
    m_showApts = enable;
    if (m_showApts)
    {
        if (indexAirports == 0)
        {
            indexAirports = m_map->model()->treeModel()->addDocument(m_pAirports);
        }
    }
    else
    {
        if (indexAirports != 0)
        {
            m_map->model()->treeModel()->removeDocument(indexAirports);
            indexAirports = 0;
        }
    }
}

void AptNavaidsPlugin::rwySizeUnitClicked()
{
    if (m_uiConfigWidget->rbRwyMeters->isChecked())
    {
        m_rwySizeUnit = 0;
    }
    else
    {
        m_rwySizeUnit = 1;
    }
}





void AptNavaidsPlugin::getAirports(Marble::GeoDataDocument &document)
{
    QString sQuery;
    QString activeContinents = m_pActiveContinents->join("' OR continent = '");
    sQuery = QString("SELECT rowid, icao, type, name, lat_deg, lon_deg, elev_ft, country FROM airports WHERE (continent = '%1') AND NOT type LIKE 'HELI%' AND NOT type='GLIDING' AND NOT type='LIGHT_AIRCRAFT';").arg(activeContinents);
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    // save Marble dataPath for later reset
    QString dataPath = Marble::MarbleDirs::marbleDataPath();
    Marble::MarbleDirs::setMarbleDataPath(QDir::currentPath());

    while (query.next())
    {
        //Create the placemark representing the airport
        QString code;
        if (query.value(1).toString() == "-" || query.value(1).toString().isEmpty())
        {
            code = query.value(3).toString();
        }
        else
        {
            code = query.value(1).toString();
        }
        Marble::GeoDataPlacemark *placemarkTemp = new Marble::GeoDataPlacemark(code);
        float fLatitude = query.value(4).toFloat();
        float fLongitude = query.value(5).toFloat();
        float fAltitude = query.value(6).toFloat();
        placemarkTemp->setCoordinate( fLongitude, fLatitude, fAltitude, Marble::GeoDataCoordinates::Degree );
        placemarkTemp->setCountryCode(query.value(7).toString());

        qlonglong airportId = query.value(0).toLongLong(); // rowid
        placemarkTemp->setId(QString::number(airportId));
        placemarkTemp->setDescription("A");  // set type airport


        //Add styles (icons, colors, etc.) to the placemarks
        QSharedPointer<Marble::GeoDataStyle> styleTemp(new Marble::GeoDataStyle);
        // TODO: change style depending on airport style (query.value(2))
        Marble::GeoDataIconStyle iconStyle;
        iconStyle.setIconPath(Marble::MarbleDirs::path("data/bitmaps/airport-marker.png"));
        styleTemp->setIconStyle(iconStyle);
        placemarkTemp->setStyle(styleTemp);
        document.append(placemarkTemp );
    }
    Marble::MarbleDirs::setMarbleDataPath(dataPath);

}

QSqlRecord AptNavaidsPlugin::getAirportInfo(QString id)
{
    QSqlRecord rec;
    QString sQuery = QString("SELECT icao, type, name, lat_deg, lon_deg, elev_ft, country, continent, municipality, home_url, wikipedia_url, source  FROM airports WHERE rowid = %1;").arg(id);
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    if (query.next()) // should return only one record (rowid's are unique)
    {
        rec = query.record();
    }
    return rec;
}

QSqlRecord AptNavaidsPlugin::getNavaidInfo(QString id)
{
    QSqlRecord rec;
    QString sQuery = QString("SELECT ident, type, name, lat_deg, lon_deg, elev_ft, country, freq_mhz, power, source FROM navaids WHERE rowid = %1;").arg(id);
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    if (query.next()) // should return only one record (rowid's are unique)
    {
        rec = query.record();
    }
    return rec;
}

QList<QSqlRecord> AptNavaidsPlugin::loadRunwaysFrom(qlonglong id)
{
    QList<QSqlRecord> recordList;
    QString sQuery = QString("SELECT icao, name, sfc, status, length_m, width_m, lighted FROM runways WHERE airports_rowid = '%1';").arg(id);
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);
    while (query.next())
    {
        recordList << query.record();
    }
    query.finish();
    return recordList;
}

void AptNavaidsPlugin::get5LNC(GeoDataDocument &document)
{
    QString sQuery;
    QString activeContinents = m_pActiveContinents->join("' OR continent = '");
    sQuery = QString("SELECT rowid, ident, lat_deg, lon_deg, country FROM icao5lnc WHERE continent = '%1';").arg(activeContinents);
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    QString dataPath = Marble::MarbleDirs::marbleDataPath();
    Marble::MarbleDirs::setMarbleDataPath(QDir::currentPath());

    while (query.next())
    {

        //Create the placemark representing the airport
        Marble::GeoDataPlacemark *placemarkTemp = new Marble::GeoDataPlacemark(query.value(1).toString());
        float fLatitude = query.value(2).toFloat();
        float fLongitude = query.value(3).toFloat();
        placemarkTemp->setCoordinate( fLongitude, fLatitude, 0, Marble::GeoDataCoordinates::Degree );
        placemarkTemp->setCountryCode(query.value(4).toString());
        qlonglong navaidId = query.value(0).toLongLong(); // rowid
        placemarkTemp->setId(QString::number(navaidId));
        placemarkTemp->setDescription("F");  // set type 5LNC (five letter name code)

        //Add styles (icons, colors, etc.) to the placemarks
        QSharedPointer<Marble::GeoDataStyle> styleTemp(new Marble::GeoDataStyle);
        Marble::GeoDataIconStyle iconStyle;

        iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/5lnc-marker.png" ));

        styleTemp->setIconStyle(iconStyle);
        placemarkTemp->setStyle(styleTemp);

        document.append(placemarkTemp );
    }
    Marble::MarbleDirs::setMarbleDataPath(dataPath);
}

void AptNavaidsPlugin::getNavaids(Marble::GeoDataDocument &document)
{
    QString sQuery;
    QString activeContinents = m_pActiveContinents->join("' OR continent = '");
    sQuery = QString("SELECT rowid, ident, type, name, lat_deg, lon_deg, elev_ft, country, freq_mhz, power, source FROM navaids WHERE continent = '%1';").arg(activeContinents);
    QSqlQuery query;
    query.setForwardOnly(true);
    query.exec(sQuery);

    QString dataPath = Marble::MarbleDirs::marbleDataPath();
    Marble::MarbleDirs::setMarbleDataPath(QDir::currentPath());

    while (query.next())
    {

        //Create the placemark representing the airport
        Marble::GeoDataPlacemark *placemarkTemp = new Marble::GeoDataPlacemark(query.value(1).toString());
        float fLatitude = query.value(4).toFloat();
        float fLongitude = query.value(5).toFloat();
        float fElevation = query.value(6).toFloat();
        placemarkTemp->setCoordinate( fLongitude, fLatitude, fElevation, Marble::GeoDataCoordinates::Degree );
        placemarkTemp->setCountryCode(query.value(7).toString());
        QString type = query.value(2).toString();
        qlonglong navaidId = query.value(0).toLongLong(); // rowid
        placemarkTemp->setId(QString::number(navaidId));
        placemarkTemp->setDescription("N");  // set type navaid

        //Add styles (icons, colors, etc.) to the placemarks
        QSharedPointer<Marble::GeoDataStyle> styleTemp(new Marble::GeoDataStyle);
        Marble::GeoDataIconStyle iconStyle;

        if (type == "VOR" || type == "DVOR")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/vor-marker.png" ));
        }
        else if (type == "VOR-DME" || type == "DVOR-DME")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/vor-dme-marker.png" ));
        }
        else if (type == "VORTAC" || type == "DVORTAC")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/vortac-marker.png" ));
        }
        else if (type == "DME")
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/dme-marker.png" ));
        }
        else if (type.compare("NDB") >= 0 )
        {
            iconStyle.setIconPath(Marble::MarbleDirs::path( "data/bitmaps/ndb-marker.png" ));
        }
        styleTemp->setIconStyle(iconStyle);
        placemarkTemp->setStyle(styleTemp);

        document.append(placemarkTemp );
    }
    Marble::MarbleDirs::setMarbleDataPath(dataPath);
}


}

//Q_EXPORT_PLUGIN2( AptNavaidsPlugin, Marble::AptNavaidsPlugin )



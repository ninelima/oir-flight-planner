//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//


#ifndef MARBLE_APTNAVAIDSPLUGIN_H
#define MARBLE_APTNAVAIDSPLUGIN_H

#include <DialogConfigurationInterface.h>
#include <RenderPlugin.h>
#include "../../AptNavaidsPluginInterface.h"

#include <QObject>
#include <QFont>
#include <QPen>
#include <QAction>
#include <QSqlRecord>

namespace Ui {
    class AptNavaidsConfigWidget;
}

class QPlainTextEdit;
class InfoPopup;

namespace Marble
{
class MarbleMap;
class GeoDataDocument;

class AptNavaidsPlugin : public RenderPlugin, public DialogConfigurationInterface, public AptNavaidsPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA( IID "org.kde.edu.marble.AirspacesPlugin" )
    Q_INTERFACES( Marble::RenderPluginInterface )
    Q_INTERFACES( Marble::DialogConfigurationInterface )
    Q_INTERFACES(AptNavaidsPluginInterface)
    MARBLE_PLUGIN( AptNavaidsPlugin )

 public:
    explicit AptNavaidsPlugin( const MarbleModel *marbleModel = 0 );

    QStringList backendTypes() const;
    QString renderPolicy() const;
    QStringList renderPosition() const;
    QString name() const;
    QString guiString() const;
    QString nameId() const;

    QString version() const;

    QString description() const;

    QString copyrightYears() const;

    QVector<PluginAuthor> pluginAuthors() const;

    QIcon icon () const;

    void initialize ();

    bool isInitialized () const;


    bool render( GeoPainter *painter, ViewportParams *viewport, const QString& renderPos, GeoSceneLayer * layer = 0 );

    QDialog *configDialog();
    QHash<QString,QVariant> settings() const;
    void setSettings( const QHash<QString,QVariant> &settings );

    // from AptNavaidsPluginInterface
    void setMap(MarbleMap *map, QStringList *activeContinents);

    AptNavaidsPluginWorker *newModule();

 Q_SIGNALS:

 public Q_SLOTS:
    bool  eventFilter( QObject *object, QEvent *event );


 private:
    void addContextItems();
    void removeContextItems();
    void getAirports(Marble::GeoDataDocument &document);
    QSqlRecord getAirportInfo(QString id);
    QSqlRecord getNavaidInfo(QString id);
    void getNavaids(Marble::GeoDataDocument &document);
    QList<QSqlRecord> loadRunwaysFrom(qlonglong id);
    void get5LNC(Marble::GeoDataDocument &document);

 private Q_SLOTS:

    void writeSettings();
    void setShowNavaids(bool enable);
    void setShow5LNC(bool enable);
    void setShowApts(bool enable);
    void rwySizeUnitClicked();
    void drawInfoPopup(QVector<const GeoDataFeature *> placeMarks, QPoint mousePos);

 private:
    Q_DISABLE_COPY( AptNavaidsPlugin )


    int     m_showApts;
    int     m_showNavaids;
    int     m_show5LNC;
    int     m_rwySizeUnit;
    bool    m_initialized;
    QFont   m_font_regular;
    int     m_fontascent;
    bool    m_showPopup;

    QWidget *m_widget;
    MarbleMap *m_map;
    QDialog * m_configDialog;
    Ui::AptNavaidsConfigWidget * m_uiConfigWidget;

    InfoPopup *m_infoPopup;
    GeoDataDocument *m_pAirports;
    GeoDataDocument *m_pNavaids;
    GeoDataDocument *m_p5LNC;
    int indexAirports;
    int indexNavaids;
    int index5LNC;
    QStringList *m_pActiveContinents;


};

}

#endif // MARBLE_APTNAVAIDSPLUGIN_H

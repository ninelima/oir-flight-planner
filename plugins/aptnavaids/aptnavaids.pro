CONFIG -= debug_and_release
CONFIG( debug, debug|release )  {
  CONFIG -= release
}
else {
  CONFIG -= debug
  CONFIG += release
}

QT += core widgets sql
TEMPLATE = lib
CONFIG += plugin
TARGET = aptnavaids

include(../../common.pri)

win32 {
#Windows
        DLLDESTDIR = $$DIST_DIR/plugins
} else {
# Linux
        DESTDIR = $$DIST_DIR/plugins
}

INCLUDEPATH +=  $$MARBLE_INC

# Input
HEADERS += AptNavaidsPlugin.h \
    ../../AptNavaidsPluginInterface.h \
    infopopup.h
FORMS += AptNavaidsConfigWidget.ui \
    infopopup.ui
SOURCES += AptNavaidsPlugin.cpp \
    infopopup.cpp

RESOURCES += \
    ../../data/resources.qrc


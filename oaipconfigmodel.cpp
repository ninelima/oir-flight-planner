//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include "oaipconfigmodel.h"

#include "sql_queries.h"

#define START_INDEX_EDITABLE 3 // column index of the first editable item


OaipConfigModel::OaipConfigModel(QObject *parent) :
    QAbstractTableModel(parent),
    m_transactionActive(false)
{
}

int OaipConfigModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return datalist.size();
}

int OaipConfigModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 6;
}

QVariant OaipConfigModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        switch(index.column())
        {
        case 0:
            return datalist.at(index.row())->country.toUpper();
            break;
        case 1:
            return datalist.at(index.row())->name_en;
            break;
        case 2:
            return datalist.at(index.row())->continent.toUpper();
            break;
        default:
            return QVariant();
        }
    }
    else if ( role == Qt::CheckStateRole)
    {
        switch(index.column())
        {
        case START_INDEX_EDITABLE:
            return getCheckstate(datalist.at(index.row())->wpt);
            break;
        case START_INDEX_EDITABLE+1:
            return getCheckstate(datalist.at(index.row())->nav);
            break;
        case START_INDEX_EDITABLE+2:
            return getCheckstate(datalist.at(index.row())->asp);
            break;
        default:
            return QVariant();
        }
    }
    return QVariant();
}

QVariant OaipConfigModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal)
        {
            switch (section)
            {
            case 0:
                return tr("ISO");
                break;
            case 1:
                return tr("Country");
                break;
            case 2:
                return tr("Continent");
                break;
            case START_INDEX_EDITABLE:
                return tr("Wpt");
                break;
            case START_INDEX_EDITABLE+1:
                return tr("Nav");
                break;
            case START_INDEX_EDITABLE+2:
                return tr("Asp");
                break;
            }
        }
    }

    return QVariant();
}

Qt::ItemFlags OaipConfigModel::flags(const QModelIndex &index) const
{
    if (index.column()>= START_INDEX_EDITABLE)
    {
        return Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    }
    else
    {
        return Qt::NoItemFlags;
    }
}

bool OaipConfigModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(role)
    QString country = datalist[index.row()]->country;
    QString sQuery = QString("UPDATE oaipfiles SET installed='%1' WHERE country='%2' AND type='%3'");
    int8_t state;
    if (value == Qt::Checked)
    {
        state = 1;
    }
    else if (value == Qt::Unchecked)
    {
        state = 0;
    }


    switch (index.column())
    {
    case START_INDEX_EDITABLE:
        datalist[index.row()]->wpt = state;
        sQuery = sQuery.arg(state).arg(country).arg("wpt");
        break;
    case START_INDEX_EDITABLE+1:
        datalist[index.row()]->nav = state;
        sQuery = sQuery.arg(state).arg(country).arg("nav");
        break;
    case START_INDEX_EDITABLE+2:
        datalist[index.row()]->asp = state;
        sQuery = sQuery.arg(state).arg(country).arg("asp");
        break;
    default:
        return false;

    }
    query.exec(sQuery);

    emit dataChanged(index, index);
    return true;
}

bool OaipConfigModel::setCheckState(const int col, const int row, const Qt::CheckState checkState)
{
    QString country = datalist[row]->country;
    QString sQuery = QString("UPDATE oaipfiles SET installed='%1' WHERE country='%2' AND type='%3'");

    int state;
    if (checkState == Qt::Checked)
    {
        state = 1;
    }
    else if (checkState == Qt::Unchecked)
    {
        state = 0;
    }

    switch (col)
    {
    case START_INDEX_EDITABLE:
        if (datalist.at(row)->wpt != 2)
        {
            datalist.at(row)->wpt = state;
            sQuery = sQuery.arg(state).arg(country).arg("wpt");
        }
        break;
    case START_INDEX_EDITABLE+1:
        if (datalist.at(row)->nav != 2)
        {
            datalist.at(row)->nav = state;
            sQuery = sQuery.arg(state).arg(country).arg("nav");
        }
        break;
    case START_INDEX_EDITABLE+2:
        if (datalist.at(row)->asp != 2)
        {
            datalist.at(row)->asp = state;
            sQuery = sQuery.arg(state).arg(country).arg("asp");
        }
        break;
    default:
        break;
    }
    bool ret = query.exec(sQuery);

    emit dataChanged(this->index(row, col), this->index(row, col));
    return ret;
}

void OaipConfigModel::toggleTransaction()
{
    if (m_transactionActive)
    {
        query.exec("END TRANSACTION");
        m_transactionActive = false;
    }
    else
    {
        query.exec("BEGIN TRANSACTION");
        m_transactionActive = true;
    }
}

bool OaipConfigModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(row)
    Q_UNUSED(count)
    Q_UNUSED(parent)
    return true;

}

bool OaipConfigModel::setupData()
{
    QString sQuery;
    sQuery = SEL_CTRY_CONFIG;

    query.setForwardOnly(true);
    query.exec(sQuery);
    while (query.next())
    {
        struct dataSet *temp = new struct dataSet;
        temp->country = query.value(0).toString(); // country
        temp->name_en = query.value(1).toString(); // country_name
        temp->continent = query.value(2).toString(); // continent
        if (query.value(START_INDEX_EDITABLE).toString() == "")
        {
            temp->wpt = 2;
        }
        else
        {
            temp->wpt = query.value(START_INDEX_EDITABLE).toInt();
        }

        if (query.value(START_INDEX_EDITABLE+1).toString() == "")
        {
            temp->nav = 2;
        }
        else
        {
            temp->nav = query.value(START_INDEX_EDITABLE+1).toInt();
        }

        if (query.value(START_INDEX_EDITABLE+2).toString() == "")
        {
            temp->asp = 2;
        }
        else
        {
            temp->asp = query.value(START_INDEX_EDITABLE+2).toInt();
        }
        datalist.append(temp);
    }

    return true;
}

QVariant OaipConfigModel::getCheckstate(const int8_t state) const
{
    switch (state)
    {
    case 0:
        return Qt::Unchecked;
    case 1:
        return Qt::Checked;
    case 2:
        return QVariant();
    }
    return QVariant();
}

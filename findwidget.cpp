//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//  $Rev: 46 $:
//  $Author: brougham73 $:
//  $Date: 2018-11-06 22:14:19 +0000 (Tue, 06 Nov 2018) $:



#include "findwidget.h"
#include "ui_findwidget.h"

#include <QDebug>

#include "databasehandler.h"

FindWidget::FindWidget(DatabaseHandler *databaseHandler, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FindWidget),
    m_pDatabase(databaseHandler)
{
    ui->setupUi(this);
    ui->foundItems->setColumnCount(7);
    QStringList headerTexts;
    headerTexts << tr("Type") << tr("Code") << tr("Name") << tr("Subtype") << tr("Country") << tr("Lon") << tr("Lat");
    ui->foundItems->hideColumn(0);
    ui->foundItems->setHorizontalHeaderLabels(headerTexts);
    ui->foundItems->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->foundItems->setSelectionMode(QAbstractItemView::SingleSelection);
    connect(ui->leFind, SIGNAL(textEdited(QString)), this, SLOT(find(QString)));
    connect(ui->cbICAOonly, SIGNAL(stateChanged(int)), this, SLOT(icaoOnlyChanged(int)));
    connect(ui->pbClose, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->pbAddToRoute, SIGNAL(clicked()), this, SLOT(addWaypoint()));
    connect(ui->pbGoto, SIGNAL(clicked()), this, SLOT(gotoWaypoint()));
}

FindWidget::~FindWidget()
{
    delete ui;
}


void FindWidget::find(QString wpt)
{
    if (wpt.size() < 3) return;

    for (int i = ui->foundItems->rowCount()-1; i >= 0; i--)
    {
        ui->foundItems->removeRow(i);  // does this delete the items from memory (after 'new')?

    }

    QList<QStringList> list = m_pDatabase->findWaypoint(wpt, ui->cbICAOonly->isChecked());
    for (int i = 0; i < list.size(); i++)
    {
        ui->foundItems->insertRow(i);
        for (int k = 0; k < list.at(i).size(); k++)
        {
            //qDebug() << list.at(i).at(k);
            QTableWidgetItem *newItem = new QTableWidgetItem(list.at(i).at(k));
            ui->foundItems->setItem(i, k, newItem);
        }
    }
    if (list.size() == 1)  // select automatically the record if only one is available
    {
        ui->foundItems->selectRow(0);
    }

}

void FindWidget::gotoWaypoint()
{
//    QModelIndexList indexes = ui->foundItems->selectedIndexes();
    QList<QTableWidgetItem*> indexes = ui->foundItems->selectedItems();
    if (indexes.size() > 0)
    {
        int row = indexes.at(0)->row();
        qreal lon, lat;

        lon = ui->foundItems->item(row, 5)->data(Qt::DisplayRole).toFloat();
        lat = ui->foundItems->item(row, 6)->data(Qt::DisplayRole).toFloat();
        emit gotoClicked(lon, lat);
    }
}

void FindWidget::addWaypoint()
{
    QList<QTableWidgetItem*> indexes = ui->foundItems->selectedItems();
    if (indexes.size() > 0)
    {
        int row = indexes.at(0)->row();
        qreal lon, lat;

        lon = ui->foundItems->item(row, 5)->data(Qt::DisplayRole).toFloat();
        lat = ui->foundItems->item(row, 6)->data(Qt::DisplayRole).toFloat();
        QString code = ui->foundItems->item(row, 1)->data(Qt::DisplayRole).toString();
        QString type = ui->foundItems->item(row, 0)->data(Qt::DisplayRole).toString();  // type is A/N/F
        emit addClicked(lon, lat, code, type);
    }
}

void FindWidget::icaoOnlyChanged(int)
{
    find(ui->leFind->text());
}

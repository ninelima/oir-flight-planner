//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//

#ifndef WEBVIEWWIDGET_H
#define WEBVIEWWIDGET_H

#ifdef WITH_WEBKIT
#include <QWebView>
#else
#include <QWebEngineView>
#endif
#include <QList>

class QAction;

#ifdef WITH_WEBKIT
class WebviewWidget : public QWebView
#else
class WebviewWidget : public QWebEngineView
#endif
{
    Q_OBJECT
public:
    WebviewWidget(QWidget *parent = 0);
    ~WebviewWidget();
    void appendUrlTemplate(QString description, QString url);
    void loadIndex(int i);
    void setUrlParam(QString param);
public slots:
    void showNotam(QStringList icaoCodes);
    void showUrl(QUrl url);

protected:
    void contextMenuEvent(QContextMenuEvent *event);

private slots:
    void changeUrl();
    void showExternal();
    void showExternal(QUrl url);
    void showNotam();
    void processSslErrors(QNetworkReply *networkReply, QList<QSslError> errorList);
private:
    QList<QStringList> m_urllist;
    int m_currentIndex;
    QString m_urlParam;
    QList<QAction*> actionList;
    QAction *m_pActShowExternal;
    QAction *m_pActShowNotam;




};

#endif // WEBVIEWWIGET_H

//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include <QtGlobal>

#include "aboutwindow.h"
#include "ui_aboutwindow.h"

#include "databasehandler.h"

AboutWindow::AboutWindow(DatabaseHandler *db, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutWindow)
{
    ui->setupUi(this);
    ui->lblVersion->setText(QString("OIR Flight Planner: %1").arg(QApplication::applicationVersion()));
    ui->lblQt->setText(QString("Compiled against Qt version: %1").arg(QT_VERSION_STR));
    connect(ui->pbClose, SIGNAL(clicked()), this, SLOT(close()));

    ui->lblAirportCount->setText(QString::number(db->getAirportCount()));
    ui->lblAirspaceCount->setText(QString::number(db->getAirspaceCount()));
    ui->lblRunwayCount->setText(QString::number(db->getRunwayCount()));
    ui->lblNavaidsCount->setText(QString::number(db->getNavaidCount()));

}

AboutWindow::~AboutWindow()
{
    delete ui;
}


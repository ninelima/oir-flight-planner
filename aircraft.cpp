//
// Copyright 2016 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 48 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:37:30 +0000 (Wed, 07 Nov 2018) $
//

#include <QSettings>
#include "aircraft.h"

Aircraft::Aircraft(QObject *parent) : QObject(parent)
{
    // initialize with usable default values
    this->immatriculation = "";
    this->type = "";
    this->cruiseSpeed = 120;
    this->climbSpeed = 100;
    this->climbRate = 500;
    this->descentSpeed = 130;
    this->descentRate = 500;
    this->speedUnit = 0;
    this->rateUnit = 0;
    this->fuelFlow = 35;
    this->fuelUsable = 150;
}


Q_GLOBAL_STATIC(AircraftList, aircraftList)

AircraftList::AircraftList() : QList<Aircraft*>()
{
    QSettings settings;
    settings.beginGroup("Aircraft");
    int size = settings.beginReadArray("Data");
    for (int i = 0; i < size; ++i) {
         settings.setArrayIndex(i);
         Aircraft *newAircraft = new Aircraft;
         newAircraft->setImmatriculation(settings.value("imma", "-").toString());
         newAircraft->setType(settings.value("type", "").toString());
         newAircraft->setCruiseSpeed(settings.value("cruiseSpeed", 120).toInt());
         newAircraft->setClimbSpeed(settings.value("climbSpeed", 100).toInt());
         newAircraft->setClimbRate(settings.value("climbRate", 500).toInt());
         newAircraft->setDescentSpeed(settings.value("descentSpeed", 130).toInt());
         newAircraft->setDescentRate(settings.value("descentRate", 500).toInt());
         newAircraft->setSpeedUnit(settings.value("speedUnit", 0).toInt());
         newAircraft->setRateUnit(settings.value("rateUnit", 0).toInt());
         newAircraft->setFuelFlow(settings.value("fuelflow", 32).toReal());
         newAircraft->setFuelUsable(settings.value("fuelusable", 150).toInt());
         append(newAircraft);

    }
    settings.endArray();
    settings.endGroup();
}


AircraftList *AircraftList::instance()
{
    return aircraftList();
}

//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//

#include "webviewwidget.h"

#include <QContextMenuEvent>
#include <QDebug>
#include <QMenu>
#include <QAction>
#include <QDesktopServices>
#include <QNetworkRequest>

WebviewWidget::WebviewWidget(QWidget *parent) :
#ifdef WITH_WEBKIT
    QWebView(parent)
#else
    QWebEngineView(parent)
#endif
{
    m_pActShowExternal = new QAction(tr("Show in external browser"), this);
    connect(m_pActShowExternal, SIGNAL(triggered()), this, SLOT(showExternal()));
    m_pActShowNotam = new QAction(tr("Notam"), this);
    connect(m_pActShowNotam, SIGNAL(triggered()), this, SLOT(showNotam()));
#ifdef WITH_WEBKIT
    connect(this->page()->networkAccessManager(), SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)), this, SLOT(processSslErrors(QNetworkReply*,QList<QSslError>)));
#endif

}

WebviewWidget::~WebviewWidget()
{

}

void WebviewWidget::appendUrlTemplate(QString description, QString url)
{
    QStringList item;
    item.append(description);
    item.append(url);
    m_urllist.append(item);
    QAction *newAction = new QAction(description, this);
    actionList.append(newAction);
    connect(newAction, SIGNAL(triggered()), this, SLOT(changeUrl()));
}

void WebviewWidget::loadIndex(int i)
{
    m_currentIndex = i;
    QUrl url = QUrl(m_urllist.at(i).at(1).arg(m_urlParam));
#ifdef WITH_WEBKIT
    page()->setLinkDelegationPolicy(QWebPage::DontDelegateLinks);
#endif
    load(url);
}

void WebviewWidget::setUrlParam(QString param)
{
    m_urlParam = param;
}

void WebviewWidget::contextMenuEvent(QContextMenuEvent *event)
{
    if ((event->modifiers() & Qt::ShiftModifier) )
    {
#ifdef WITH_WEBKIT
        QWebView::contextMenuEvent(event);
#else
        QWebEngineView::contextMenuEvent(event);
#endif
    }
    else
    {
        QMenu menu(this);
        menu.addActions(actionList);
        QPoint menuPos = event->globalPos();
        menu.addAction(m_pActShowNotam);
        menu.addSeparator();
        menu.addAction(m_pActShowExternal);
        menuPos.setX(menuPos.x()+5);
        menuPos.setY(menuPos.y()+5);
        menu.exec(menuPos);
    }

}

void WebviewWidget::changeUrl()
{
    QObject *senderObj = this->sender();
    for (int i = 0; i < actionList.size(); i++)
    {
        if (actionList.at(i) == senderObj)
        {
            loadIndex(i);
            break;
        }
    }

}

void WebviewWidget::showExternal()
{
    QDesktopServices::openUrl(this->url());
}

void WebviewWidget::showExternal(QUrl url)
{
    QDesktopServices::openUrl(url);
}

void WebviewWidget::showNotam()
{
    showNotam(QStringList(m_urlParam));
}

void WebviewWidget::processSslErrors(QNetworkReply *networkReply, QList<QSslError> errorList)
{
    qDebug() << errorList;
}

void WebviewWidget::showNotam(QStringList icaoCodes)
{
    QString codes;
    for (int i = 0; i < icaoCodes.size(); i++)
    {
        codes.append(icaoCodes.at(i));
        if (i < icaoCodes.size()-1)
        {
            codes.append("+");
        }
    }

#ifdef WITH_WEBKIT
    QNetworkRequest request(QUrl("https://pilotweb.nas.faa.gov/PilotWeb/notamRetrievalByICAOAction.do?method=displayByICAOs"));
#else
    QWebEngineHttpRequest request(QUrl("https://pilotweb.nas.faa.gov/PilotWeb/notamRetrievalByICAOAction.do?method=displayByICAOs"), QWebEngineHttpRequest::Post);
#endif
    QByteArray body;
    body.append("formatType=ICAO&");
    body.append(QString("retrieveLocId=%1&").arg(codes));
    body.append("reportType=REPORT&");
    body.append("actionType=notamRetrievalByICAOs&");
//    body.append("openItems=icaosHeader,icaos:icaoHead,icao:rightNavSec0,rightNavSecBorder0:");
    body.append("submit=View+NOTAMs");
#ifdef WITH_WEBKIT
    page()->setLinkDelegationPolicy(QWebPage::DontDelegateLinks);
    this->load(request, QNetworkAccessManager::PostOperation, body);
#else
    request.setPostData(body);
    this->load(request);
#endif
}

void WebviewWidget::showUrl(QUrl url)
{
#ifdef WITH_WEBKIT
    page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
#endif
    this->load(url);
    connect(this, SIGNAL(linkClicked(QUrl)), this, SLOT(showExternal(QUrl)));
}


//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 47 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:02:47 +0000 (Wed, 07 Nov 2018) $
//

#include "openaipupdater.h"

#include <QString>

// if using webkit for parsing index.html
#if 0
#include <QWebFrame>
#include <QWebPage>
#include <QWebElement>
#endif

// if using XML DOM parser
//#include <QDomDocument>
// if using libhtml
#include <QSgml.h>
#include <QSgmlTag.h>

#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QSettings>


#include "defs.h"
#include "openaipreader.h"


OpenAipUpdater::OpenAipUpdater(QObject *parent) :
    QObject(parent),
    m_pManager(new QNetworkAccessManager(this)),
    m_isWorking(false)
{
    QSettings settings;
    m_url.setUrl(settings.value("update/openaip_url", "http://www.openaip.net/customer_export_Hdj293dksl1SLd10lkZg326gFdu/").toString());
}

OpenAipUpdater::~OpenAipUpdater()
{
    delete m_pManager;
}

#if 0  // if using QWebPage
void OpenAipUpdater::getIndexFromWeb()
{
    m_url.setUrl("http://mf.local/oaip/");
    page.mainFrame()->load(m_url);

}

void OpenAipUpdater::parseIndex(bool finished)
{
    Q_UNUSED(finished)
    QHash<QString, int> monthNameNbr;
    monthNameNbr["Jan"] = 1;
    monthNameNbr["Feb"] = 2;
    monthNameNbr["Mar"] = 3;
    monthNameNbr["Apr"] = 4;
    monthNameNbr["May"] = 5;
    monthNameNbr["Jun"] = 6;
    monthNameNbr["Jul"] = 7;
    monthNameNbr["Aug"] = 8;
    monthNameNbr["Sep"] = 9;
    monthNameNbr["Oct"] = 10;
    monthNameNbr["Nov"] = 11;
    monthNameNbr["Dec"] = 12;
    QWebElement document = page.mainFrame()->documentElement();

/*
one table row has the following format:

<tr>
    <td valign="top">
        <img alt="[   ]" src="/icons/unknown.gif">
    </td>
    <td>
        <a href="ad_wpt.aip">ad_wpt.aip</a>
    </td>
    <td align="right">18-Oct-2015 04:00  </td>
    <td align="right">  0 </td>
    <td>&nbsp;</td>
</tr>

*/

    QWebElementCollection allTr = document.findAll("tr");
    foreach (QWebElement trElement, allTr) {

        QWebElement col0 = trElement.firstChild();
        QWebElement col1 = col0.nextSibling();
        QWebElement aElement = col1.firstChild();
        QWebElement col2 = col1.nextSibling();
        QString colDate = col2.toPlainText();
        QWebElement col3 = col2.nextSibling();
        QString colSize = col3.toPlainText();

        if (aElement.attribute("href").endsWith("aip"))
        {
            QString sType = aElement.attribute("href").mid(3,3);
            QString sCountry = aElement.attribute("href").left(2);
            if (sType == "hot") continue;  // jump over xx_hot.aip files
            qDebug() << aElement.attribute("href") << colDate << colSize;
            QString statusLine = tr("found %1, %2, %3").arg(aElement.attribute("href")).arg(colDate).arg(colSize);
            emit textlineToAdd(statusLine);
            // colDate has format "20-Oct-2015 08:44"
            const QStringList temp = colDate.split(" ").first().split("-"); // split into day, month, year. Do not use QDate::fromString because it uses localized month names!

            QDate fileDate = QDate(temp.at(2).toInt(), monthNameNbr.value(temp.at(1)), temp.at(0).toInt());
            QString sQuery = "UPDATE oaipfiles SET dateAvail='%1' WHERE country='%2' AND type='%3'";
            sQuery = sQuery.arg(fileDate.toString("yyyy-MM-dd")).arg(sCountry).arg(sType);
            query.exec(sQuery);
            //query.exec(sQuery);
            if (query.numRowsAffected() == 0)  // try to update failed
            {
                sQuery = "INSERT INTO oaipfiles (country, type, version, dateAvail, installed) VALUES ('%1', '%2', '', '%3', '0')";
                sQuery = sQuery.arg(sCountry).arg(sType).arg(fileDate.toString("yyyy-MM-dd"));
                query.exec(sQuery);
            }
        }
    }
}
#endif

void OpenAipUpdater::getIndexFromWeb()
{

    m_isWorking = true;
    QNetworkRequest request;
    request.setUrl(m_url);
    QString ua = QString("%1 %2").arg(appName).arg(appVersion);
    request.setRawHeader("User-Agent", ua.toLocal8Bit());
    QNetworkReply *reply = m_pManager->get(request);
    connect(reply, SIGNAL(finished()), this, SLOT(parseIndex()));
}

void OpenAipUpdater::parseIndex()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(QObject::sender());
    /*
    // not used, only for older implementation with localized dates on index file
    QHash<QString, int> monthNameNbr;
    monthNameNbr["Jan"] = 1;
    monthNameNbr["Feb"] = 2;
    monthNameNbr["Mar"] = 3;
    monthNameNbr["Apr"] = 4;
    monthNameNbr["May"] = 5;
    monthNameNbr["Jun"] = 6;
    monthNameNbr["Jul"] = 7;
    monthNameNbr["Aug"] = 8;
    monthNameNbr["Sep"] = 9;
    monthNameNbr["Oct"] = 10;
    monthNameNbr["Nov"] = 11;
    monthNameNbr["Dec"] = 12;
    */

/*
one table row has the following format (date changed from 10-Oct-2015 to 2016-10-02):

<tr>
    <td valign="top">
        <img alt="[   ]" src="/icons/unknown.gif">
    </td>
    <td>
        <a href="ad_wpt.aip">ad_wpt.aip</a>
    </td>
    <td align="right">2016-10-02 04:00  </td>
    <td align="right">  0 </td>
    <td>&nbsp;</td>
</tr>

*/
    //QNetworkReply *reply = qobject_cast<QNetworkReply *>(QObject::sender());

    QSgml *document = new QSgml(QString(reply->readAll()));

    QList<QSgmlTag*> allTr;
    document->getElementsByName("tr", &allTr);

    m_query.exec("BEGIN TRANSACTION");
    for (int i=0; i < allTr.size(); i++)
    {
        QSgmlTag *trElement = allTr.at(i);
        QList<QSgmlTag*> allTd = trElement->Children;
        if (allTd.size() != 5) continue;

        QSgmlTag *col1 = allTd.at(1);
        QSgmlTag aElement = col1->getNextElement(); // return first child <a href..>
        QSgmlTag *col2 = allTd.at(2);
        QString colDate = col2->getNextElement().Value;  // innerHTML will be extracted as another QSgmlTag with getNextElement()
        QSgmlTag *col3 = allTd.at(3);
        QString colSize = col3->getNextElement().Value;
        if (colSize == "0") continue; // do not proceed with files with size 0


        if (aElement.Attributes.value("href").endsWith("aip"))
        {
            QString sType = aElement.Attributes.value("href").mid(3,3);
            QString sCountry = aElement.Attributes.value("href").left(2);
            if (sType == "hot") continue;  // jump over xx_hot.aip files
            qDebug() << aElement.Attributes.value("href") << colDate << colSize;
            QString statusLine = tr("found %1, %2, %3").arg(aElement.Attributes.value("href")).arg(colDate).arg(colSize);
            emit textlineToAdd(statusLine);
            /* older version for localized date:
            // colDate has format "20-Oct-2015 08:44"
            const QStringList temp = colDate.split(" ").first().split("-"); // split into day, month, year. Do not use QDate::fromString because it uses localized month names!
            QDate fileDate = QDate(temp.at(2).toInt(), monthNameNbr.value(temp.at(1)), temp.at(0).toInt());
            */
            // ISO date format YYYY-MM-DD
            const QString temp = colDate.split(" ").first(); //  Do not use QDate::fromString because it uses localized month names!
            QDate fileDate = QDate::fromString(temp, "yyyy-MM-dd");

            QString sQuery = "UPDATE oaipfiles SET dateAvail='%1' WHERE country='%2' AND type='%3'";
            sQuery = sQuery.arg(fileDate.toString("yyyy-MM-dd")).arg(sCountry).arg(sType);
            m_query.exec(sQuery);
            if (m_query.numRowsAffected() == 0)  // try to update failed
            {
                sQuery = "INSERT INTO oaipfiles (country, type, version, dateAvail, installed) VALUES ('%1', '%2', '', '%3', '0')";
                sQuery = sQuery.arg(sCountry).arg(sType).arg(fileDate.toString("yyyy-MM-dd"));
                m_query.exec(sQuery);
            }
        }

    }
    m_query.exec("END TRANSACTION");
    emit getIndexFinished();
    reply->deleteLater();
    m_isWorking = false;
}


void OpenAipUpdater::getFiles()
{
    m_isWorking = true;
    QString sQuery = "SELECT country, type, dateAvail, dateInst FROM oaipfiles WHERE installed='1'";
    m_query.setForwardOnly(true);
    m_query.exec(sQuery);
    // find the necessary files in the database and append their names and dates to m_replyList
    while (m_query.next())
    {
        QString fileName = "%1_%2.aip";
        fileName = fileName.arg(m_query.value(0).toString()).arg(m_query.value(1).toString());

        if ((m_query.value(2).toDate() > m_query.value(3).toDate()) || m_query.value(2).toString().isEmpty() || m_query.value(3).toString().isEmpty())  // update if one of the date fields is empty
        {
            NetworkReply *reply = new NetworkReply;
            reply->setDate(m_query.value(2).toDate());
            reply->setFilename(fileName);
            m_replyList.append(reply);
        }
        else
        {
            emit textlineToAdd(tr("no new version of %1, skipping...").arg(fileName));
        }
    }
    // begin downloading the files one by one. once a file has finished download, replyFinished gets called and the file parsed
    getNextFile();
}



void OpenAipUpdater::replyFinished()
{
    NetworkReply *reply = qobject_cast<NetworkReply *>(QObject::sender());
    QString fileName = reply->getFilename();
    emit textlineToAdd(tr("Parsing %1").arg(fileName));
    OpenAIPReader reader;
    QString type = fileName.mid(3,3);

    if (type.compare("nav", Qt::CaseInsensitive) == 0)
    {
        reader.readNavaidsData(reply->getNetworkReply(), reply->getDate().toString("yyyy-MM-dd"), fileName.left(2));
    }
    else if (type.compare("wpt", Qt::CaseInsensitive) == 0)
    {
        reader.readAirportData(reply->getNetworkReply(), reply->getDate().toString("yyyy-MM-dd"), fileName.left(2));
    }
    else if (type.compare("asp", Qt::CaseInsensitive) == 0)
    {
        reader.readAirspaceData(reply->getNetworkReply(), reply->getDate().toString("yyyy-MM-dd"), fileName.left(2));
    }
    else {
        reply->getNetworkReply()->abort();
    }

    emit textlineToAdd(tr("%1 finished").arg(reply->getFilename()));
    reply->getNetworkReply()->deleteLater();
    getNextFile();

}

void OpenAipUpdater::getNextFile()
{
    if (!m_replyList.empty())
    {
        NetworkReply *reply = m_replyList.takeFirst();

        QNetworkRequest request;
        emit textlineToAdd(tr("Downloading %1").arg(reply->getFilename()));
        QUrl dlUrl = m_url.toString().append(reply->getFilename());
        request.setUrl(dlUrl);
        QString ua = QString("%1 %2").arg(appName).arg(appVersion);
        request.setRawHeader("User-Agent", ua.toLocal8Bit());
        reply->setReply(m_pManager->get(request));
        connect(reply, SIGNAL(finished()), this, SLOT(replyFinished()));
    }
    else
    {
        emit textlineToAdd(tr("parsing and updating database finished!"));
        emit repliesFinished();
        m_isWorking = false;

    }
}


NetworkReply::NetworkReply(QObject *parent) :
    QObject(parent),
    m_pReply(0)
{
}

void NetworkReply::setReply(QNetworkReply *reply)
{
    m_pReply = reply;
    connect(m_pReply, SIGNAL(readyRead()), this, SIGNAL(readyRead()));
    connect(m_pReply, SIGNAL(finished()), this, SIGNAL(finished()));
}

QNetworkReply *NetworkReply::getNetworkReply()
{
    return m_pReply;
}

void NetworkReply::setDate(QDate date)
{
    m_fileDate = date;
}

QDate NetworkReply::getDate()
{
    return m_fileDate;
}

void NetworkReply::setFilename(QString fileName)
{
    m_fileName = fileName;
}

QString NetworkReply::getFilename()
{
    return m_fileName;
}

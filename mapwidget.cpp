//
// Copyright 2015 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 53 $
//  $Author: brougham73 $
//  $Date: 2019-07-23 20:34:58 +0000 (Tue, 23 Jul 2019) $
//

#include "mapwidget.h"

#include <QPaintEvent>
#include <QDebug>

#include <MarbleMap.h>
#include <MarbleModel.h>
#include <MarbleLocale.h>
#include <GeoDataCoordinates.h>
#include <GeoDataFeature.h>
#include <GeoDataPlacemark.h>
#include <GeoPainter.h>
#include <RenderPlugin.h>
#include <Planet.h>
#include <ViewportParams.h>
#include <mapwidgetpopupmenu.h>
#include <mapwidgetinputhandler.h>

MapWidget::MapWidget(QWidget *parent) :
    QWidget(parent),
    m_popupmenu(0),
    m_inputhandler(0),
    m_viewAngle(110),
    m_logzoom(0)
{
    m_map = new MarbleMap();
    m_map->setMapThemeId("earth/openstreetmap/openstreetmap.dgml");
    m_map->setProjection( Mercator );
    m_map->setShowBorders(true);
    m_map->setShowOverviewMap(false);
    m_map->setShowCrosshairs(false);


    m_popupmenu = new MapWidgetPopupMenu( this, model() );

    setInputHandler();
    connect(this, SIGNAL(updateRequired()), this, SLOT(update()));


    connect( m_inputhandler, SIGNAL(mouseClickScreenPosition(int,int)),
           m_map,       SLOT(notifyMouseClick(int,int)) );
    m_inputhandler->setPositionSignalConnected(true);


    connect( m_inputhandler, SIGNAL(mouseMoveGeoPosition(QString)),
             this,       SIGNAL(mouseMoveGeoPosition(QString)) );

    connect(m_inputhandler, SIGNAL(lmbRequest(int,int)), m_popupmenu, SLOT(showLmbMenu(int,int)));
    connect(m_inputhandler, SIGNAL(rmbRequest(int,int)), m_popupmenu, SLOT(showRmbMenu(int,int)));


    // forward some signals of m_map
    connect(m_map, SIGNAL(visibleLatLonAltBoxChanged(GeoDataLatLonAltBox)), this, SIGNAL(visibleLatLonAltBoxChanged(GeoDataLatLonAltBox)));
    connect(m_map, SIGNAL(projectionChanged(Projection)), this, SIGNAL(projectionChanged(Projection)));
    connect(m_map, SIGNAL(tileLevelChanged(int)), this, SIGNAL(tileLevelChanged(int)));
    connect(m_map, SIGNAL(framesPerSecond(qreal)), this, SIGNAL(framesPerSecond(qreal)));

    connect(m_map, SIGNAL(pluginSettingsChanged()), this, SIGNAL(pluginSettingsChanged()));
    connect(m_map, SIGNAL(renderPluginInitialized(RenderPlugin*)), this, SIGNAL(renderPluginInitialized(RenderPlugin*)));

    connect(m_map, SIGNAL(repaintNeeded(QRegion)), this, SLOT(update()));
/*    connect(m_map,   SIGNAL(visibleLatLonAltBoxChanged(GeoDataLatLonAltBox)),
                       m_widget, SLOT(updateSystemBackgroundAttribute()) );*/
    connect(m_map, SIGNAL(renderStatusChanged(RenderStatus)), this, SIGNAL(renderStatusChanged(RenderStatus)));
    connect(m_map, SIGNAL(renderStateChanged(RenderState)), this, SIGNAL(renderStateChanged(RenderState)));

//    map->setSize(this->width(), this->height());  // nicht nötig, da die Grösse später im resizeEvent gesetzt wird
}

void MapWidget::paintEvent(QPaintEvent *event)
{
    QPaintDevice *paintDevice = this;

    {
        // FIXME: Better way to get the GeoPainter
        // Create a painter that will do the painting.
        GeoPainter geoPainter( paintDevice, m_map->viewport(), m_map->mapQuality() );

        m_map->paint( geoPainter, event->rect() );
    }



}

void MapWidget::resizeEvent ( QResizeEvent * event )
{
    m_map->setSize(event->size());
    QWidget::resizeEvent(event);
}

MarbleModel *MapWidget::model()
{
    return m_map->model();
}

const MarbleModel *MapWidget::model() const
{
    return m_map->model();
}

MarbleMap *MapWidget::map()
{
    return m_map;
}

const MarbleMap *MapWidget::map() const
{
    return m_map;
}

void MapWidget::centerOn( const GeoDataCoordinates &position, bool animated )
{
    Q_UNUSED(animated)
    //m_map->centerOn(position.longitude(), position.latitude());
    GeoDataLookAt target = lookAt();
    target.setCoordinates(position);
    flyTo(target, Instant);
}

void MapWidget::setZoom(int newZoom)
{
    // Check for under and overflow.
    if (newZoom < minimumZoom())
        newZoom = minimumZoom();
    else if (newZoom > maximumZoom())
        newZoom = maximumZoom();

    // Prevent infinite loops.
    if (newZoom == m_logzoom)
        return;

    m_map->setRadius(radius(newZoom));
    m_logzoom = newZoom;
    emit zoomChanged(m_logzoom);
    emit distanceChanged(distanceString());
    emit updateRequired();
}

QList<RenderPlugin *> MapWidget::renderPlugins() const
{
    return m_map->renderPlugins();
}

int MapWidget::tileZoomLevel() const
{
    return m_map->tileZoomLevel();
}

qreal MapWidget::centerLatitude() const
{
    return m_map->centerLatitude();
}

qreal MapWidget::centerLongitude() const
{
    return m_map->centerLongitude();
}

int MapWidget::zoom() const
{
    return m_logzoom;
}

int MapWidget::minimumZoom() const
{
    return m_map->minimumZoom();
}

int MapWidget::maximumZoom() const
{
    return m_map->maximumZoom();
}

void MapWidget::readPluginSettings( QSettings& settings )
{
    foreach( RenderPlugin *plugin, renderPlugins() ) {
        settings.beginGroup( QString( "plugin_" ) + plugin->nameId() );

        QHash<QString,QVariant> hash;

        foreach ( const QString& key, settings.childKeys() ) {
            hash.insert( key, settings.value( key ) );
        }

        plugin->setSettings( hash );

        settings.endGroup();
    }
}

void MapWidget::writePluginSettings( QSettings& settings ) const
{
    foreach( RenderPlugin *plugin, renderPlugins() ) {
        settings.beginGroup( QString( "plugin_" ) + plugin->nameId() );

        QHash<QString,QVariant> hash = plugin->settings();

        QHash<QString,QVariant>::iterator it = hash.begin();
        while( it != hash.end() ) {
            settings.setValue( it.key(), it.value() );
            ++it;
        }

        settings.endGroup();
    }
}

void MapWidget::downloadRegion( QVector<TileCoordsPyramid> const & pyramid )
{
    m_map->downloadRegion( pyramid );
}

MapWidgetPopupMenu *MapWidget::popupMenu()
{
    return m_popupmenu;
}


QVector<const GeoDataPlacemark*> MapWidget::whichFeatureAt( const QPoint &curpos ) const
{
#if 1   // for marble >= 15.08
    QVector<const GeoDataPlacemark*> ret;
    QVector<const GeoDataFeature*> featureAt = m_map->whichFeatureAt(curpos);

    for (int i = 0; i < featureAt.size(); i++) {
        const GeoDataFeature *feat = featureAt.at(i);
        const GeoDataPlacemark *pm;
        if ((pm = dynamic_cast<const GeoDataPlacemark*>(feat)))
        {
            ret.append(pm);
        }
    }
    return ret;
#else // marble < 15.08
    return m_map->whichFeatureAt( curpos );
#endif
}

QList<AbstractDataPluginItem*> MapWidget::whichItemAt( const QPoint &curpos ) const
{
    return m_map->whichItemAt( curpos );
}

bool MapWidget::geoCoordinates( int x, int y,
                                   qreal& lon, qreal& lat,
                                   GeoDataCoordinates::Unit unit ) const
{
    return m_map->geoCoordinates( x, y, lon, lat, unit );
}


bool MapWidget::screenCoordinates( qreal lon, qreal lat,
                                      qreal& x, qreal& y ) const
{
    return m_map->screenCoordinates( lon, lat, x, y );
}


void MapWidget::setInputHandler()
{
    setInputHandler(new MapWidgetInputHandler(this));
}


void MapWidget::setInputHandler(MapWidgetInputHandler *handler )
{
    delete m_inputhandler;
    m_inputhandler = handler;

    if ( m_inputhandler )
    {
        qDebug() << "install main inputhandler";
        this->installEventFilter( m_inputhandler );

        connect( m_inputhandler, SIGNAL(mouseClickScreenPosition(int,int)),
               m_map,       SLOT(notifyMouseClick(int,int)) );

        connect( m_inputhandler, SIGNAL(mouseMoveGeoPosition(QString)),
                 this,       SIGNAL(mouseMoveGeoPosition(QString)) );
        m_inputhandler->installPluginEventFilter();
    }
}


MapWidgetInputHandler *MapWidget::inputHandler()
{
    return m_inputhandler;
}

const MapWidgetInputHandler *MapWidget::inputHandler() const
{
    return m_inputhandler;
}

qreal MapWidget::distanceFromRadius(qreal radius) const
{
    // Due to Marble's orthographic projection ("we have no focus")
    // it's actually not possible to calculate a "real" distance.
    // Additionally the viewing angle of the earth doesn't adjust to
    // the window's size.
    //
    // So the only possible workaround is to come up with a distance
    // definition which gives a reasonable approximation of
    // reality. Therefore we assume that the average window width
    // (about 800 pixels) equals the viewing angle of a human being.

    return (model()->planet()->radius() * 0.4
            / radius / tan(0.5 * m_viewAngle * DEG2RAD));
}

qreal MapWidget::radiusFromDistance(qreal distance) const
{
    return model()->planet()->radius() /
            (distance * tan(0.5 * m_viewAngle * DEG2RAD) / 0.4 );
}

int MapWidget::radius() const
{
    return m_map->radius();
}

qreal MapWidget::distanceFromZoom(qreal zoom) const
{
    return distanceFromRadius(radius(zoom));
}

qreal MapWidget::zoomFromDistance(qreal distance) const
{
    return zoom(radiusFromDistance(distance));
}

qreal MapWidget::radius(qreal zoom) const
{
    return pow(M_E, (zoom / 200.0));
}

qreal MapWidget::zoom(qreal radius) const
{
    return (200.0 * log(radius));
}

void MapWidget::zoomAt(const QPoint &pos, qreal newDistance)
{
    Q_ASSERT(newDistance > 0.0);

    qreal destLat;
    qreal destLon;
    if (!map()->geoCoordinates(pos.x(), pos.y(), destLon, destLat, GeoDataCoordinates::Degree))
    {
        return;
    }

    ViewportParams* now = map()->viewport();
    qreal x(0), y(0);
    if (!now->screenCoordinates(destLon * DEG2RAD, destLat * DEG2RAD, x, y))
    {
        return;
    }

    ViewportParams soon;
    soon.setProjection(now->projection());
    soon.centerOn(now->centerLongitude(), now->centerLatitude());
    soon.setSize(now->size());

    qreal newRadius = radiusFromDistance(newDistance);
    soon.setRadius(newRadius);

    qreal mouseLon, mouseLat;
    if (!soon.geoCoordinates(int(x), int(y), mouseLon, mouseLat, GeoDataCoordinates::Degree ))
    {
        return;
    }

    const qreal lon = destLon - (mouseLon - m_map->centerLongitude());
    const qreal lat = destLat - (mouseLat - m_map->centerLatitude());

    GeoDataLookAt lookAt;
    lookAt.setLongitude(lon, GeoDataCoordinates::Degree);
    lookAt.setLatitude(lat, GeoDataCoordinates::Degree);
    lookAt.setAltitude(0.0);
    lookAt.setRange(newDistance * KM2METER);

    map()->viewport()->setFocusPoint(GeoDataCoordinates(destLon, destLat, 0, GeoDataCoordinates::Degree));
    flyTo(lookAt, Linear);

}




void MapWidget::flyTo(const GeoDataLookAt &newLookAt, FlyToMode mode)
{
    Q_UNUSED(mode)
    const int radius = qRound(radiusFromDistance(newLookAt.range() * METER2KM));
    qreal const zoomVal = zoom(radius);

    // Prevent exceeding zoom range. Note: Bounding to range is not useful here
    if (qRound(zoomVal) >= minimumZoom() && qRound(zoomVal) <= maximumZoom())
    {
        map()->setRadius(radius);
        m_logzoom = qRound(zoom(radius));

        GeoDataCoordinates::Unit deg = GeoDataCoordinates::Degree;
        map()->centerOn(newLookAt.longitude(deg), newLookAt.latitude(deg));

        emit zoomChanged(m_logzoom);
        emit distanceChanged(distanceString());
        emit updateRequired();
    }



}

void MapWidget::clearVolatileTileCache()
{
    map()->clearVolatileTileCache();
}

QString MapWidget::distanceString() const
{
    qreal dist = distance();
    QString distanceUnitString;

    const MarbleLocale::MeasurementSystem measurementSystem =
            MarbleGlobal::getInstance()->locale()->measurementSystem();

    switch (measurementSystem)
    {
    case MarbleLocale::MetricSystem:
        distanceUnitString = tr("km");
        break;
    case MarbleLocale::ImperialSystem:
        dist *= KM2MI;
        distanceUnitString = tr("mi");
        break;
    case MarbleLocale::NauticalSystem:
        dist *= KM2NM;
        distanceUnitString = tr("nm");
        break;
    }

    return QString("%L1 %2").arg(dist, 8, 'f', 1, QChar(' ')).arg(distanceUnitString);
}

qreal MapWidget::distance() const
{
    return distanceFromRadius(m_map->radius());
}

GeoDataLookAt MapWidget::lookAt() const
{
    GeoDataLookAt result;

    result.setLongitude(map()->viewport()->centerLongitude());
    result.setLatitude(map()->viewport()->centerLatitude());
    result.setAltitude(0.0);
    result.setRange(distance() * KM2METER);

    return result;
}

ViewportParams* MapWidget::viewport()
{
    return m_map->viewport();
}

const ViewportParams* MapWidget::viewport() const
{
    return m_map->viewport();
}

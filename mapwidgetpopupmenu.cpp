//
// Copyright 2015-2018 Pascal Tritten     <ptritten@gmail.com>
//
// This file is part of OIR Flight Planner.
//
// OIR Flight Planner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OIR Flight Planner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OIR Flight Planner.  If not, see <http://www.gnu.org/licenses/>.
//
//
//  $Rev: 47 $
//  $Author: brougham73 $
//  $Date: 2018-11-07 21:02:47 +0000 (Wed, 07 Nov 2018) $
//


#include "mapwidgetpopupmenu.h"

#include <QAction>
#include <QMenu>
#include <MarbleModel.h>
#include <GeoDataPlacemark.h>
#include "mapwidget.h"
#include <QDebug>

MapWidgetPopupMenu::MapWidgetPopupMenu(MapWidget *widget,const MarbleModel *model) :
    QObject(widget),
    m_widget(widget),
    m_lmbMenu(new QMenu),
    m_rmbMenu(new QMenu),
    m_rmbActionListGen(QList<QAction *>()),
    m_rmbActionListItems(QList<QAction *>())
{
    Q_UNUSED(model)
}

void MapWidgetPopupMenu::addActionForApt( Qt::MouseButton button, QAction* action )
{
    if ( button == Qt::RightButton ) {
        m_rmbActionListItems.append(action);
    } else {
//        d->m_lmbMenu.addAction( action );
    }
}

QPoint MapWidgetPopupMenu::mousePosition() const
{
    return m_mousePosition;
}

void MapWidgetPopupMenu::showLmbMenu(int x, int y)
{
    qDebug() << "showLmbMenu at " << x << y;
}

void MapWidgetPopupMenu::showRmbMenu(int x, int y)
{
    const QPoint curpos = QPoint(x, y);
    m_mousePosition.setX(x);
    m_mousePosition.setY(y);

    m_rmbMenu->clear(); // clear before adding items depending on position
    m_rmbMenu->addActions(m_rmbActionListGen);

    QVector<const Marble::GeoDataPlacemark *> placeMark = m_widget->whichFeatureAt(curpos);
    if (!placeMark.empty() && placeMark.first()->description() == "A")  // airfield/airport
    {
        m_rmbMenu->addActions(m_rmbActionListItems);
    }
    m_rmbMenu->popup(m_widget->mapToGlobal(curpos));


}

void MapWidgetPopupMenu::addLmbActionsGeneral(QList<QAction *> list)
{
    m_lmbMenu->addActions(list);

}

void MapWidgetPopupMenu::addRmbActionsGeneral(QList<QAction *> list)
{
    m_rmbActionListGen.append(list);
}

